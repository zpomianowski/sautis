<?php
// Kodowanie UTF-8, test: Zażółć gęślą jaźń
// Edycja testu
require_once('tests_main.php');
require_once('tests_auto_mails.php');


// Returns associative array with all columns fetched from mantis_project_table for this project.
// On error, print error and return null.
function fetch_test_params($test_id)
{
	$result = mysql_query(sprintf("select * from tests_tests where id_test=%d", $test_id));
	if (!$result)
	{
		print_error_paragraph(mysql_error());
		return null;
	}
	
	$row = mysql_fetch_assoc($result);
	if (!$row)
	{
		print_error_paragraph('Test o podanym identyfikatorze nie istnieje.');
		return null;
	}
	
	return $row;
}

// $selected_id_project can be null.
// On error, return false.
function print_project_select($selected_id_project)
{
	$result = mysql_query("SELECT id, name FROM mantis_project_table WHERE enabled=1 ORDER BY name");
	if (!$result) return false;
	
	print("<select name=\"project\" id=\"project_select\">\n");
	while ($row = mysql_fetch_row($result))
	{
		if ($row[0] == $selected_id_project)
			printf("<option value=\"%d\" selected=\"selected\">%s</option>\n", $row[0], htmlspecialchars($row[1]) );
		else 
			printf("<option value=\"%d\">%s</option>\n", $row[0], htmlspecialchars($row[1]) );
	}
	print("</select>\n");
	
	return true;
}

// $selected_id_planned_soft can be null.
function print_planned_soft_select($selected_id_planned_soft)
{
	// Filled in JavaScript.
	print("<select name=\"planned_soft\" id=\"planned_soft_select\"><option>(Brak)</option></select>\n");
}

// On error, return false.
function print_test_params_editor()
{
	global $test_id, $new_test, $test_params;
	
	$endedTest = $test_params['stop_date'] ? true : false;
	
	$arr = explode("-", $test_params['start_date']);
	$startDate = $arr[2]."/".$arr[1]."/".$arr[0];					
	$arr = explode("-", $test_params['estimated_stop_date']);
	$estimatedStopDate = $arr[2]."/".$arr[1]."/".$arr[0];					
	$arr = explode("-", $test_params['software_arrive_date']);
	$softArriveDate = $arr[2]."/".$arr[1]."/".$arr[0];					
	
	
	
	echo '<div id="TestDiv"><a onclick="return ExpandCollapse(this);" href="#" title="Expand" class="icon_collapse"></a><h3 style="display:inline;padding-left: 5px;"  id="TestDetailsHeader">Details</h3></div>
	<div id="TestDetailsDiv">';
	
	print("<form method=\"post\" action=\"tests_test_edit.php\"><table class=\"TableData\">\n");

	// Project
	print("<tr><td class=\"category\">Project<span style=\"color: #FF0000;\">*</span>: <td class=\"value\">");
	if ($endedTest)
		echo htmlspecialchars(get_project_name($test_params['id_project']));
	else print_project_select($test_params ? $test_params['id_project'] : null);
	
	// Planned soft
	print("<tr><td class=\"category\">Planned Software: <td class=\"value\">");
	if ($endedTest)
	{
		if ($test_params['id_planned_soft'])
		{
			$v = get_planned_soft_data($test_params['id_planned_soft']);
			echo htmlspecialchars($v['name']);
		}
		else echo '';
	}
	else print_planned_soft_select($test_params ? $test_params['id_planned_soft'] : null);
	
	// software arrive date
	if ($test_params['software_arrive_date'])
	{
		if ($endedTest)
		{
			print("<tr><td class=\"category\"><label>Software Delivery Date (DD/MM/YYYY):</label>\n<td class=\"value\">");
			echo $softArriveDate;				
		}
		else 
		{
			print("<tr><td class=\"category\"><label>Software Delivery Date (DD/MM/YYYY):</label>\n<input type=\"checkbox\" checked=\"checked\" id=\"chbSoftArriveDate\" name=\"chbSoftArriveDate\" />");
			printf("<td class=\"value\"><script type=\"text/javascript\">DateInput(\"test_soft_arrive_date\", true, \"DD/MON/YYYY\", \"%s\")</script>\n", $softArriveDate);
		}
	}
	else
	{
		if ($endedTest)
			print("<tr><td class=\"category\"><label>Software Delivery Date (DD/MM/YYYY):</label>\n<td class=\"value\">");			
		else 
		{
			print("<tr><td class=\"category\"><label>Software Delivery Date (DD/MM/YYYY):</label>\n<input type=\"checkbox\" id=\"chbSoftArriveDate\" name=\"chbSoftArriveDate\"/>");
			printf("<td class=\"value\"><script type=\"text/javascript\">DateInput(\"test_soft_arrive_date\", true, \"DD/MON/YYYY\")</script>\n");
		}
	}		
	
	// name
	if ($endedTest)
	{
		print("<tr><td class=\"category\">Software Version<span style=\"color: #FF0000;\">*</span>:\n<td class=\"value\">");
		echo htmlspecialchars($test_params['name']);
	}
	else
	{
		print("<tr><td class=\"category\"><label for=\"TestName\">Software Version<span style=\"color: #FF0000;\">*</span>:</label>\n");
		printf("<td class=\"value\"><input type=\"text\" id=\"TestName\" name=\"TestName\" class=\"button\" size=\"50\" value=\"%s\">\n",
			htmlspecialchars($test_params['name']));
	}
	
	// description
	if ($endedTest)
	{	
		print("<tr><td class=\"category\">Description<span style=\"color: #FF0000;\">*</span>:\n<td class=\"value\" style=\"white-space: pre-wrap;\">");
		echo make_bug_view_links(htmlspecialchars($test_params['description']));
	}
	else
	{
		print("<tr><td class=\"category\"><label for=\"TestDescription\">Description<span style=\"color: #FF0000;\">*</span>:</label>\n");
		printf("<td class=\"value\"><textarea id=\"TestDescription\" name=\"TestDescription\"  cols=\"49\" rows=\"10\">%s</textarea>\n",
			htmlspecialchars($test_params['description']));		
	}
			
	if (!$new_test)
	{
		// start date
		if ($test_params['start_date'])
		{
			if ($endedTest)
			{
				print("<tr><td class=\"category\"><label>Start Date (DD/MM/YYYY):</label>\n<td class=\"value\">");
				echo $startDate;				
			}
			else 
			{	
				print("<tr><td class=\"category\"><label>Start Date (DD/MM/YYYY):</label>\n<input type=\"checkbox\" checked=\"checked\" id=\"chbStartDate\" />");
				printf("<td class=\"value\"><script type=\"text/javascript\">DateInput(\"test_start_date\", true, \"DD/MON/YYYY\", \"%s\")</script>\n",$startDate);
			}
		}		
		
		// estimated stop date
		if ($test_params['estimated_stop_date'])
		{
			if ($endedTest)
			{
				print("<tr><td class=\"category\"><label>Planned End Date (DD/MM/YYYY):</label>\n<td class=\"value\">");
				echo $estimatedStopDate;				
			}
			else 
			{
				print("<tr><td class=\"category\"><label>Planned End Date (DD/MM/YYYY):</label>\n<input type=\"checkbox\" checked=\"checked\" id=\"chbEstimatedStopDate\" />");
				printf("<td class=\"value\"><script type=\"text/javascript\">DateInput(\"test_estimated_stop_date\", true, \"DD/MON/YYYY\", \"%s\")</script>\n",$estimatedStopDate);
			}
		}
		else
		{
			if ($endedTest)
				echo '<tr><td class="category"><label>Planned End Date (DD/MM/YYYY):</label><td class="value">';
			else 
			{			
				print("<tr><td class=\"category\"><label>Planned End Date (DD/MM/YYYY):</label>\n<input type=\"checkbox\" id=\"chbEstimatedStopDate\" />");
				printf("<td class=\"value\"><script type=\"text/javascript\">DateInput(\"test_estimated_stop_date\", true, \"DD/MON/YYYY\")</script>\n");
			}
		}
								
		// summary
		if ($endedTest)
		{
			print("<tr><td class=\"category\">Summary:\n<td class=\"value\" style=\"white-space: pre-wrap;\">");
			echo make_bug_view_links(htmlspecialchars($test_params['summary']));				
		}
		else 
		{
			print("<tr><td class=\"category\"><label for=\"test_summary\">Summary:</label>\n");
			printf("<td class=\"value\"><textarea name=\"summary\" id=\"test_summary\" cols=\"49\" rows=\"10\">%s</textarea>\n",
				htmlspecialchars($test_params['summary']));	
		}

		// Software status - on test end
		if ($endedTest)
		{
			print("<tr><td class=\"category\">Software Status: <td class=\"value\">");
			echo get_software_status_text($test_params['software_status']);			
		}
		else 
		{
			print("<tr><td class=\"category\">Software Status: <td class=\"value\">");			
			print("<select name=\"soft_status\" id=\"soft_status_select\">\n");
			print("<option value=\"0\">(Brak)</option>\n");
			printf("<option value=\"%d\">%s</option>\n", SOFT_ACCEPTED, get_software_status_text(SOFT_ACCEPTED));
			printf("<option value=\"%d\">%s</option>\n", SOFT_ACCEPTED_COND, get_software_status_text(SOFT_ACCEPTED_COND));
			printf("<option value=\"%d\">%s</option>\n", SOFT_REJECTED, get_software_status_text(SOFT_REJECTED));
			print("</select>\n");			
		}
	}
	
	echo '
	<tr>
		<td class="category" ><span style="color: #FF0000;">*necessary</span></td>
		<td class="value"></td>
	</tr>';

	echo '
	<tr>
		<td class="category" ><span id="save_result"></span></td>
		<td class="value">';
	if ($new_test)
	{
		echo '<input type="hidden" name="action" value="add" />';
		echo '<input type="submit" class="button" onclick="return CheckFields();" value="Add" />';
	}
	else
	{
		if (!$test_params['stop_date'])
			echo '<input id="SaveTest" type="submit" class="button" value="Save" />';
			
		if (!$test_params['start_date'] && !$test_params['stop_date'])
		{
			echo '<input type="hidden" name="action" value="start" />';
			echo '<input id="StartTest" type="submit" class="button" value="Start Test" />';
		}
		else if ($test_params['start_date'] && !$test_params['stop_date'])
		{
			echo '<input type="hidden" name="action" value="stop" />';
			echo '<input id="StopTest" type="submit" class="button" value="Stop Test" />';
		}
	}
	
	echo '
		</td>
	</tr>
	</table>
	<input type="hidden" name="testID" value="'.$test_id.'"/></form></div>';
	
	return true;
}

function print_tasks_manager()
{
	global $test_id;
	
	$funcs = get_func_chap_data($test_id);
	$usersTime = array('time' => 0); 
	$usersTimes = get_users_task_times($test_id);
	
	echo '<br><br>
	<div id="TasksDiv"><a onclick="return ExpandCollapseDivsDown(this);" href="#" title="Expand" class="icon_expand"></a><h3 style="display:inline;padding-left: 5px;"  id="TasksHeader">Tasks Manager</h3></div>
	<div id="TasksManagerDiv">';
	
	echo '<div class="CasesTree">';	
	foreach($funcs as $func)
	{
		$exFFDropClass = ($func['open_cases'] > 0) ? 'UserDrop' : '';
		$exFFLoadClass = ($func['open_cases'] == 0) ? 'BlockedTR' : ((count($func['users'])> 0) ? 'LoadTR' : '');
		$exFUDragClass = ($func['open_cases'] > 0) ? 'UserDrag' : '';

		if ($func['cases_count'] != 0)
			$perc = round((($func['cases_count'] - $func['open_cases']) / $func['cases_count']) * 100, 2);
		else $perc = 0;
				
		echo '
		<div class="FunctionalityDiv">	
		<div class="FunctionalityHeaderDiv">		
		<table class="TableData">
		<col width="20"><col width="*">
		<tr id="TRFunctionality-'.$func['id'].'" class="FuncRow '.$exFFDropClass.'  
			 Params_FuncID-'.$func['id'].'_ChapterCount-'.count($func['chapters']).'_Length-'.$func['length']
			.'_CaseTimeRatio-'.$func['case_time_ratio'].'_OpenCases-'.$func['open_cases'].'">			
		<td class="CursorHand '.$exFFLoadClass.'" style="text-align:center;"  onclick="return ExpandCollapse(this);"><a href="#" title="Expand" class="icon_expand"></a></td>
		<td class="CursorHand" onclick="return ExpandCollapse(this);"><span style="color: #ffff00;font-weight:bold;">'.htmlspecialchars($func['name']).'</span>
		<span style="color:#cccccc;"> [Done: '.$perc.'%] [Duration: '.$func['length'].'h]</span>';
		
		echo '<div id="FunctionalityUserContainer'.$func['id'].'" style="display:inline;">';
		echo '<br>';
		if (count($func['users'])> 0)
		{
			foreach($func['users'] as $key => $value)
				echo '<span	id="F-'.$func['id'].'_U-'.$key.'" 				 
						class="'.$exFUDragClass.' UserInFunctionality Params_UserID-'.$key.'" style="display:inline-block;"> '
						.get_user_name($key).' </span>';
		}
		echo '</div>';
		echo '<br></tr></table></div>';
		
		echo '<div class="FunctionalityContentDiv" style="display:none;">';
		$i = 0;
		foreach($func['chapters'] as $chapter)
		{						
			$i++;
			$us = $func['chapters'][$chapter['id']]['user'];
			
			$exCCDropClass = ($chapter['open_cases'] > 0) ? 'UserDrop' : '';
			$exCCLoadClass = ($chapter['open_cases'] == 0) ? 'BlockedTR' : (($us) ? 'FullLoadTR' : '');		
			$exCUClass = ($chapter['open_cases'] > 0) ? 'UserDrag' : '';
			
			if ($chapter['cases_count'] != 0)
				$perc = round((($chapter['cases_count'] - $chapter['open_cases']) / $chapter['cases_count']) * 100, 2);
			else $perc = 0;
			
			echo '
			<table class="TableData" cellspacing="0">
			<col width="40"><col width="40"><col width="*">';
			
			echo '<tr id="TRChapter-'.$chapter['id'].'" style="vertical-align:middle;" 
							class="ChapterRow TRChapter '.$exCCDropClass.' ChapterID'.$chapter['id'].' FuncID'.$func['id'].' 
							Params_FuncID-'.$func['id'].'_ChapterID-'.$chapter['id'].'_CaseTimeRatio-'.$func['case_time_ratio'].'_OpenCases-'.$chapter['open_cases'].'">';
				
			echo '<td style="background-color: #4D5154;"></td>
			<td class="LoadTD '.$exCCLoadClass.'">'.$i.'</td>
			<td style="background-color: #5d6164;">'.htmlspecialchars($chapter['name']).'<span style="color: red;"> [Done: '.$perc.'%] </span>';
			
			echo '<div id="ChapterUserContainer'.$chapter['id'].'" style="display:inline;">';
			if ($us)
				echo '<span 
						id="F-'.$func['id'].'_C-'.$chapter['id'].'_U-'.$us.'" 
						class="'.$exCUClass.' CursorHand UserInChapter FuncID'.$func['id'].' UserID'.$us.' 
						Params_FuncID-'.$func['id'].'_ChapterID-'.$chapter['id'].'_UserID-'.$us.'"> '
						.get_user_name($us).' </span>';
						
			echo '</div>';
			echo '</td>
			</tr></table>';
		}
		echo '</div>
		</div>';
	}	
	echo '</div>';
	
	echo '<div id="TestersList"><form method="get" action="tests_tests.php">
	<table class="TableData">
	<col width="200"><col width="50"><col width="60"><col width="180"><col width="*">
	<tr class="ChapterRow" style="vertical-align:middle;">
		<th>Tester</th><th>Weight</th><th>Work Time[h]</th><th>Start and Stop Dates</th><th>Task Description</th>
	</tr>';
	
	$users = get_users_list();
	while ($row = mysql_fetch_assoc($users))
	{
		if ($row['access_level'] > TESTER)
			continue;
			
		echo '<tr style="height:30px; background-color: #5d6164;">';
		echo '<td><span id="U-'.$row['id'].'" class="UserInUsersList UserDrag NewUser CursorHand Params_UserID-'.$row['id'].'">'.get_user_name($row['id']).'</span></td>';
		echo '<td id="User'.$row['id'].'Weight" style="text-align:center;">'.$row['weight'].'</td>';
				
		if ($usersTimes[$row['id']]['all_time'])
			echo '<td id="User'.$row['id'].'TaskTime" class="TaskTime" style="text-align: center;">'.round(($row['weight']*$usersTimes[$row['id']]['all_time']), 1).'h ('.round($usersTimes[$row['id']]['all_time'],1).'h)</td>';
		else echo '<td id="User'.$row['id'].'TaskTime" class="TaskTime" style="text-align: center;"></td>';

		
		$taskD = get_task_data2($test_id, $row['id']);		
		$arr = explode("-", $taskD['start_date']);
		$startTaskDate = $arr[2]."/".$arr[1]."/".$arr[0];
		$arr = explode("-", $taskD['stop_date']);
		$stopTaskDate = $arr[2]."/".$arr[1]."/".$arr[0];					

		echo '<td class="value" style="width: 180px;">';
		if ($taskD['start_date'] != null) printf("<script type=\"text/javascript\">DateInput(\"task_start_date".$row['id']."\", true, \"DD/MON/YYYY\", \"%s\")</script>", $startTaskDate);
		else echo '<script type="text/javascript">DateInput("task_start_date'.$row['id'].'", true, "DD/MON/YYYY")</script>';
				
		if ($taskD['stop_date'] != null) printf('<script type="text/javascript">DateInput("task_stop_date'.$row['id'].'", true, "DD/MON/YYYY", "%s")</script>', $stopTaskDate);
		else echo '<script type="text/javascript">DateInput("task_stop_date'.$row['id'].'", true, "DD/MON/YYYY")</script>';
		echo '</td>';
		
		
		echo '<td><textarea id="User'.$row['id'].'TaskDesc" class="Params_UserID-'.$row['id'].' TaskDescription" rows="2" cols="0" style="width: 99%;">';
		
		$query = 'SELECT description FROM tests_tasks WHERE id_test='.$test_id.' AND id_user='.$row['id'].';';
		$result = mysql_query($query);
		$desc = mysql_fetch_assoc($result);
		if ($desc)
			echo htmlspecialchars($desc['description']);	
		echo '</textarea></td>';	
		echo '</tr>';
		print("\n");
	}
	
	echo '<tr style="height:30px; background-color: #5d6164;"><td style="text-align: right;">Summary:</td><td></td><td id="TasksTimeSummary" style="text-align:center;"></td><td></td><td></td></tr>';

	echo '<tr><td colspan="4" style="background-image: none; padding-left:0;">		
		<table><tr>
		<td><img id="TrashIcon" src="images/trash.png" alt="Trash"></td>
		<td><input type="submit" class="button" onclick="return UpdateButton();" value="Update" ></td>
		<td><input type="submit" class="button" onclick="return ClearAllButton();" value="Clear All"></td>
		<td><input type="submit" class="button" value="Back">
		<input type="hidden" id="ITestID" value="'.$test_id.'" ></td>
		</tr></table>
		</td></tr>';
	echo '</table></form></div>';	
	
	echo '</div>';	
}

/* Select planned soft, grouped by projects, that is not assigned to any test or assigned to the current test.
Print it as JavaScript array. The variable will look like this:
	var planned_soft = [
		  12, 'planned soft name 1', 123
		, 12, 'planned soft name 2', 124
		... ];
Fields are: project id, planned soft name, planned soft id
On error, return false.
*/
function print_planned_soft_script()
{
	global $test_id, $test_params;
	
	if ($test_id)
		$query = sprintf('select ps.id_project, ps.name, ps.id_planned_soft
			from tests_planned_soft ps
			left join tests_tests t
			on ps.id_planned_soft=t.id_planned_soft
			where ps.active=1 and (t.id_test is null or t.id_test=%d)
			order by ps.id_project, ps.planned_date DESC', $test_id);
	else
		$query = 'select ps.id_project, ps.name, ps.id_planned_soft
			from tests_planned_soft ps
			left join tests_tests t
			on ps.id_planned_soft=t.id_planned_soft
			where t.id_test is null and ps.active=1 
			order by ps.id_project, ps.planned_date DESC';
	$result = mysql_query($query);
	if (!$result) { print_error_paragraph(mysql_error()); return false; }
	
	print("<script type=\"text/javascript\">\n");
	
	if ($test_params)
		printf("var g_planned_soft_id = %d;\n", $test_params['id_planned_soft']);
	else
		print("var g_planned_soft_id = null;\n");
	
	print("var planned_soft = [\n");
	$first = true;
	while ($row = mysql_fetch_row($result))
	{
		$project_id        = $row[0];
		$planned_soft_name = $row[1];
		$planned_soft_id   = $row[2];

		if ($first)
		{
			print('  ');
			$first = false;
		}
		else
			print(', ');
			
		printf("%d, \"%s\", %d\n", $project_id, javascript_escape_string($planned_soft_name), $planned_soft_id);
	}
	print("];\n");
	
	print("</script>\n");
	
	return true;
}


////////////////////////////////////////////////////////////////////////////////
// Code

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

print_page_begin('Test');
?>
<script type="text/javascript" src="tests_calendar.js"></script>
<?php 

if (!current_user_can_edit_tests())
	print_error_paragraph('Access Denied.');
else
{
	$test_id = $_GET['id'];
	if ($test_id == 'new')
	{
		$test_id = null;
		$new_test = true;
		$ok = true;
	}
	else if (is_numeric($test_id))
		$ok = true;
	else
		print_error_paragraph('Wrong test ID.');		
}
/* Here we have variables:
 * if ($ok) then either $new_test==true and $test_id==null or $test_id contains test ID.
 */

if ($ok && $test_id)
{
	$test_params = fetch_test_params($test_id);
	$ok = $test_params !== null;
}

// Now if $ok and $test_id, we also have $test_params.

if ($ok)
	$ok = print_planned_soft_script();

if ($ok)
	$ok = print_test_params_editor();
	
if (!$new_test && !$test_params['stop_date'])
	print_tasks_manager();
	
echo '<br>';
	
?>
<script type="text/javascript"><?php if (!$new_test) echo 'var testID = '.$test_id.';'; ?></script>
<script type="text/javascript" src="tests_test.js"></script>
<?php	
print_page_end();
?>