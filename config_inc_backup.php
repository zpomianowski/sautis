<?php
	$g_hostname = 'saut';
	$g_db_type = 'mysql';
	$g_database_name = 'saut_bugtracker';
	$g_db_username = 'saut';
	$g_db_password = 'papasmerflubijagody?2011';

	date_default_timezone_set('Europe/Warsaw');
	
	$g_file_upload_method    	= DISK;
	#$g_file_upload_ftp_server	= '193.164.142.251';
	#$g_file_upload_ftp_user		= 'saut';
	#$g_file_upload_ftp_pass		= 'uwep0ew86';
	$g_max_file_size		    = 50000000;
	$g_absolute_path_default_upload_folder = 'files\\';

	$g_show_user_email_threshold = REPORTER;
	$g_send_reset_password = ON;
	$g_enable_email_notification = ON;
	$g_administrator_email  = 'saut.test@gmail.com';
	$g_webmaster_email      = 'saut.test@gmail.com';
	$g_from_email           = 'saut.test@gmail.com';
	$g_return_path_email    = 'saut.test@gmail.com';
	$g_validate_email = OFF;
	$g_email_receive_own    = ON;
	
	$g_phpMailer_method = 2;
	$g_smtp_host = 'smtp.gmail.com';
	$g_smtp_port = 587;
	$g_smtp_username = 'saut.test@gmail.com';
	$g_smtp_password = 'saut.test1';
	$g_smtp_connection_mode = 'tls';
	#$g_log_level = LOG_EMAIL | LOG_EMAIL_RECIPIENT | LOG_FILTERING | LOG_AJAX;
	#$g_log_destination = 'file:c:/mantisbt.log';

	$g_limit_email_domain	= OFF;
	$g_phpMailer_method	= PHPMAILER_METHOD_SMTP;
	$g_use_phpMailer = OFF;
	
	
	$g_view_filters = ADVANCED_ONLY;
	$g_signup_use_captcha = OFF;
	$g_show_detailed_errors	= ON; 

	//enums
	$g_priority_enum_string	            = '10:low,30:high,60:very high,90:critical';
	$g_status_enum_string				= '10:open,90:closed';
	$g_resolution_enum_string			= '10:with vendor,20:with CP,30:fixed by vendor,60:open,70:under testing,80:closed by CP,90:reopen'; 
	$g_saut_issue_type_enum_string      = '10:Firmware,30:Software,60:FW + SW,90:Other';

	//defaults
	$g_default_limit_view = 500;
	$g_default_language		= 'auto';
	$g_window_title			= 'SAUTis';
	$g_default_bug_priority = SAUT_PRIORITY_HIGH;
	$g_default_bug_resolution = SAUT_RESOLUTION_OPEN;
	$g_default_new_account_access_level	= VIEWER;
	$g_default_bug_view_status = VS_PUBLIC;
	$g_default_bug_reproducibility = REPRODUCIBILITY_ALWAYS;
	$g_due_date_update_threshold = UPDATER;
	$g_due_date_view_threshold = UPDATER;
	$g_create_project_threshold = MANAGER;
	$g_delete_project_threshold = MANAGER;
	$g_set_view_status_threshold = DEVELOPER;
	$g_auto_set_status_to_assigned	= OFF;
	$g_show_user_email_threshold = REPORTER;
	$g_show_user_realname_threshold = REPORTER;
	$g_reminder_receive_threshold = UPDATER;
	$g_handle_bug_threshold			= UPDATER;
	$g_private_project_threshold = DEVELOPER;
	$g_set_view_status_threshold = DEVELOPER;
	$g_change_view_status_threshold = DEVELOPER;
	$g_update_readonly_bug_threshold = DEVELOPER;
	$g_manage_global_profile_threshold = DEVELOPER;

	$g_bug_report_page_fields = array(
		'category_id',
		'view_state',
		'priority',
		'reproducibility',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'attachments',
		'saut_issue_type',
	);

	$g_bug_view_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'tags',
		'attachments',
		'due_date',		
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_bug_print_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'tags',
		'attachments',
		'due_date',			
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_bug_update_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'attachments',
		'due_date',			
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_bug_change_status_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'tags',
		'attachments',
		'due_date',			
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_language_choices_arr	= array(
		'english',
		'polish',
	);
?>
