<?php
// Edycja planowanego oprogramowania
// Zażółć gęślą jaźń
require_once('tests_main.php');

$action = ($_POST) ? $_POST['action'] : $_SESSION['FAction'];

connect_to_database();
if (!login())
	die("Access Denied");


$id_planned_soft = $_POST['id_planned_soft'];

switch ($action)
{
	case 'deactive':
	{
		$query = 'UPDATE tests_planned_soft SET active="0" WHERE id_planned_soft='.$id_planned_soft;
		$result = mysql_query( $query );
		redirect("tests_planned_soft.php");
		break;
	}
	case 'delete':
	{
		$query = "SELECT COUNT(id_planned_soft) as num FROM tests_tests WHERE id_planned_soft=".$id_planned_soft;
		$result = mysql_query( $query );
		$row = mysql_fetch_assoc($result);
		if ($row['num'] == 0)
		{
			$query = "DELETE FROM tests_planned_soft WHERE id_planned_soft=".$id_planned_soft;
			mysql_query($query);
			$t_redirect = "tests_planned_soft.php";
			redirect($t_redirect, 0);
		}
		else
		{
			send_html_header();
			print_page_begin('Error');
			print_error_paragraph("Can't delete planned software because it's associated with test!");
			echo '<br/>';
			print_back_link("tests_planned_soft.php");
			echo '<br/>';
			print_page_end();
		}		
		break;
	}
	case 'edit':
	{
		$name = StripInput($_POST['Name']);
		$desc = StripInput($_POST['Description']);
		$planned = $_POST['PlannedDate'];
		$id_planned_soft = $_POST['id_planned_soft'];
		$id_project = $_POST['ProjectID'];		
		
		$arr = explode("/", $planned);
		$arrived = $arr[2]."-".$arr[1]."-".$arr[0];		
		
		if (trim($name)=='' || trim($desc)=='')
		{
			$_SESSION['FAction'] = 'editSoft';
			$_SESSION['FName'] = $name;
			$_SESSION['FDescription'] = $desc;			
			$_SESSION['FPlannedDate'] = $planned;
			$_SESSION['FProjectID'] = $id_project;
			
			send_html_header();
			print_page_begin('Error');
			print_error_paragraph("Some necessary fields are empty!");					
			echo '<br/>';
			print_back_link("tests_planned_soft_edit.php");
			echo '<br/>';
			print_page_end();
		}
		else
		{		
			$query = "UPDATE tests_planned_soft SET name=\"".mysql_real_escape_string($name)."\", description=\"".mysql_real_escape_string($desc)."\",
							 planned_date=\"".$arrived."\", id_project=\"".$id_project."\" ";
			$query .= "WHERE id_planned_soft=\"".$id_planned_soft."\";";
			
			$r = mysql_query($query);
			$last_id = mysql_insert_id();			
			if ($r === false)
			{
				send_html_header();
				print_page_begin('Error');
				print_error_paragraph("Can't apply action: ".$query);
									
				echo '<br/>';
				print_back_link("tests_planned_soft_edit.php");
				echo '<br/>';
				print_page_end();
			}
			else
			{
				$t_redirect = "tests_planned_soft.php";
				redirect($t_redirect,0);
			}
		}
		break;
	}
	case 'add':
	{
		$name = StripInput($_POST['Name']);
		$desc = StripInput($_POST['Description']);
		$planned = $_POST['PlannedDate'];
		$id_project = $_POST['ProjectID'];
		
		$arr = explode("/", $planned);
		$arrived = $arr[2]."-".$arr[1]."-".$arr[0];		
		
		if (trim($name)=='' || trim($desc)=='')
		{
			$_SESSION['FAction'] = 'newSoft';
			$_SESSION['FName'] = $name;
			$_SESSION['FDescription'] = $desc;
			$_SESSION['FPlannedDate'] = $planned;
			$_SESSION['FProjectID'] = $id_project;
			
			send_html_header();
			print_page_begin('Error');
			print_error_paragraph("Some necessary fields are empty!");					
			echo '<br/>';
			print_back_link("tests_planned_soft_edit.php");
			echo '<br/>';
			print_page_end();
		}
		else
		{		
			$query = "INSERT INTO tests_planned_soft (id_project, name, description, planned_date) ";
			$query .= "VALUES(".$id_project.", \"".mysql_real_escape_string($name)."\", \"".mysql_real_escape_string($desc)."\", \"".$arrived."\");";
			
			$r = mysql_query($query);
			$last_id = mysql_insert_id();			
			if ($r === false)
			{
				send_html_header();
				print_page_begin('Error');
				print_error_paragraph("Can't apply action: ".$query);
									
				echo '<br/>';
				print_back_link("tests_planned_soft_edit.php");
				echo '<br/>';
				print_page_end();
			}
			else
			{
				$t_redirect = "tests_planned_soft.php";
				redirect($t_redirect,0);
			}
		}
		break;
	}
	default:
	{
		send_html_header();
		if ($action == "newSoft")
			print_page_begin('Plan Software');
		else print_page_begin('Edit Planned Software');
		
		$name = $_SESSION['FName']; unset($_SESSION['FName']);
		$desc = $_SESSION['FDescription']; unset($_SESSION['FDescription']);
		$plannedDate = $_SESSION['FPlannedDate']; unset($_SESSION['FPlannedDate']);
		$id_project = $_SESSION['FProjectID']; unset($_SESSION['FProjectID']);
				
		if ($id_planned_soft)
		{
			$plannedData = get_planned_soft_data($id_planned_soft);
			$name = $plannedData['name'];
			$desc = $plannedData['description'];
			$id_project = $plannedData['id_project'];
			
			$arr = explode("-", $plannedData['planned_date']);
			$plannedDate = $arr[2]."/".$arr[1]."/".$arr[0];					
		}
		
		?>		

<script type="text/javascript" src="tests_calendar.js"></script>
<script type="text/javascript">
function CheckFields()
{
	var name = $('#Name').val();
	var desc = $('#Description').val();

	if (name == '')
	{
		alert('Field Name must be filled!');
		return false;
	}
	
	if (desc == '')
	{
		alert('Field Description must be filled!');
		return false;
	}
}
</script>	

	<form method="post" action="tests_planned_soft_edit.php">
	<?php
	if ($action == "newSoft")
		echo '<input type="hidden" name="action" value="add"/>';
	else
	{
		echo '<input type="hidden" name="id_planned_soft" value="'.$id_planned_soft.'"/>';
		echo '<input type="hidden" name="action" value="edit"/>';
	}
	?>	
	<table class="TableData"> 
	<tr><th>Planned Software</th></tr>
	<tr><td>
	<table class="TableData">
	<tr>
		<td class="category">Project<span style="color: #FF0000;">*</span>:</td>
		<td class="value">
			<select name="ProjectID">
<?php 	
			$query = "SELECT * FROM mantis_project_table WHERE enabled=1 ORDER BY name;";
			$result = mysql_query($query);
			
			if ($result)
			{
				while($row = mysql_fetch_assoc($result))
				{
					$id = $row['id'];
					print "<option value=\"$id\"";
					if ($id_project == $id) $sel = 'selected="selected"'; else $sel = '';					
					print $sel .">".htmlspecialchars($row['name'])."</option>\n";
				}
			}
?>	
			</select>	
		</td>		
	</tr>
	<tr>
		<td class="category">
		<label for="Name">Name<span style="color: #FF0000;">*</span>:</label>
		</td>
		<td class="value">
		<input type="text" name="Name" id="Name" class="button" size="50" value="<?php if ($name) echo htmlspecialchars($name);?>"/>
		</td>
	</tr>
	<tr>
		<td class="category">
		<label for="Description">Description<span style="color: #FF0000;">*</span>:</label>
		</td>
		<td class="value">
		<textarea name="Description" id="Description" cols="49" rows="10"><?php if ($desc) echo htmlspecialchars($desc);?></textarea>
		</td>
	</tr>
	<tr>
		<td class="category">
		<label>Planned Delivery Date (DD/MM/RRRR)<span style="color: #FF0000;">*</span>:</label>
		</td>
		<td class="value">
		<?php 
		if ($plannedDate) 
			echo '<script type="text/javascript">DateInput("PlannedDate", true, "DD/MON/YYYY", "'.$plannedDate.'")</script>';
		else
			echo '<script type="text/javascript">DateInput("PlannedDate", true, "DD/MON/YYYY")</script>';
		?>
		</td>
	</tr>
	<tr>
		<td class="category" ><span style="color: #FF0000;">*necessary</span></td>
		<td class="value">
		<?php
		if ($action == "newSoft")
			echo '<input type="submit" class="button" onclick="return CheckFields();" value="Add" />';
		else
			echo '<input type="submit" class="button" onclick="return CheckFields();" value="Set" />';
		?>			
		<input type="reset" class="button" value="Reset" />
		</td>
	</tr>
	</table>
	</td></tr>
	</table>
	</form>		
	
<?php
	echo '<br>';
	echo '<div class="ActionButton">';
	echo '<form method="get" action="tests_planned_soft.php">';
	echo '<input type="submit" class="button" value="Back">';
	echo '</form>';
	echo '</div>';
	
 	print_page_end(); 	
	}
}


?>