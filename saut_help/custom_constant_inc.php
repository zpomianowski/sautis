<?php
# MantisBT - a php based bugtracking system

# priority
define('SAUT_STATUS_OPEN', 10);
define('SAUT_STATUS_CLOSED', 90);
define('SAUT_RESOLUTION_VENDOR', 40);
define('SAUT_RESOLUTION_FULLACCESS', 100);

//10:with vendor,20:with CP,30:fixed by vendor,60:open,70:still under testing,80:closed by CP,90:reopen';
define('SAUT_RESOLUTION_WITHVENDOR', 10);
define('SAUT_RESOLUTION_WITHCP', 20);
define('SAUT_RESOLUTION_CANTRESOLVE', 25);
define('SAUT_RESOLUTION_FIXEDBYVENDOR', 30);
define('SAUT_RESOLUTION_OPEN', 60);
define('SAUT_RESOLUTION_TESTING', 70);
define('SAUT_RESOLUTION_CLOSEDBYCP', 80);
define('SAUT_RESOLUTION_REOPEN', 90);

define('SAUT_PRIORITY_LOW', 10);
define('SAUT_PRIORITY_MEDIUM', 30);
define('SAUT_PRIORITY_HIGH', 60);
define('SAUT_PRIORITY_CRITICAL', 90);

// '10:Firmware,30:Software,60:FW + SW,90:Other';
define('SAUT_ISSUE_TYPE_FW', 10);
define('SAUT_ISSUE_TYPE_SW', 30);
define('SAUT_ISSUE_TYPE_FWSW', 60);
define('SAUT_ISSUE_TYPE_OTHER', 90);
?>