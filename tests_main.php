<?php
// System version
$g_ATMS_Version = '1.1.27';

// This values are also used in table tests_test_cases in column result
define("CF_NOT_TESTED", 0);
define("CF_CORRECT", 1);
define("CF_ERROR", 2);
define("CF_NOT_TESTABLE", 3);
define("CF_NOT_AVAILABLE", 4);
define("CF_ALL", 5);

define("ADMIN", 1);
define("TESTER", 2);
define("OBSERVER", 4);

define("SOFT_ACCEPTED", 1);
define("SOFT_ACCEPTED_COND", 2);
define("SOFT_REJECTED", 3);

session_start();

include_once('tests_auto_mails.php');
require_once('tests_config.php');

function get_case_status_text($case_status_id)
{
	if ($case_status_id == CF_NOT_TESTED)
		return "N/R";
	else if ($case_status_id == CF_CORRECT)
		return "Correct";
	else if ($case_status_id == CF_ERROR)
		return "Fail";
	else if ($case_status_id == CF_NOT_AVAILABLE)
		return "N/A";
	else if ($case_status_id == CF_NOT_TESTABLE)
		return "N/T";
	else if ($case_status_id == CF_ALL)
		return "All";
}

function get_software_status_text($software_status_id)
{
	if ($software_status_id == SOFT_ACCEPTED)
		return "Accepted";
	else if ($software_status_id == SOFT_ACCEPTED_COND)
		return "Accepted Conditionally";
	if ($software_status_id == SOFT_REJECTED)
		return "Rejected";
}

function get_software_status_text_PL($software_status_id)
{
	if ($software_status_id == SOFT_ACCEPTED)
		return "Zaakceptowany";
	else if ($software_status_id == SOFT_ACCEPTED_COND)
		return "Zaakceptowany Warunkowo";
	if ($software_status_id == SOFT_REJECTED)
		return "Odrzucony";
}

// Przetwarza łańcuch obcinając \" \' \\ do " ' \ lub nie robiąc nic
// To zależy od bieżących ustawień serwera.
// Używać do łańcuchów z wejścia - $_GET, $_POST, $_COOKIE
function StripInput($s)
{
	if (ini_get('magic_quotes_gpc'))
		return stripslashes($s);
	else
		return $s;
}

function send_html_header()
{
	header("Content-type: text/html; charset=UTF-8");
}

function send_xml_header()
{
	header("Content-type: text/xml; charset=UTF-8");
}

function send_header_no_cache()
{
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Pragma: no-cache');
}


$g_connected_to_database = false;

function connect_to_database()
{
	global $config, $g_connected_to_database;
	
	$db_config = $config['database'];
	
	// Protection against Kuba who likes to call connect_to_database multiple times in a single page.
	if ($g_connected_to_database)
		die("Already connected to the database. Please do not call connect_to_database mutliple times in a single page.");
	
	if (mysql_connect($db_config['hostname'], $db_config['login'], $db_config['password']) === false)
		die(sprintf("Cannot connect to MySQL database %s@%s.", $db_config['login'], $db_config['hostname']));
	
	if (!mysql_select_db($db_config['schema']))
		die(sprintf("Cannot select schema %s.", $db_config['schema']));
		
	if (!mysql_set_charset('utf8'))
		die("Cannot set database connection charset.");
		
	$g_connected_to_database = true;
}

$g_login = null;

/* Check for parameters of user logged in.
 * Connection to database must already be established.
 * Returns true if some user is logged in, false if not.
 * If returnes true, also leaves g_login global variable set to an associative array with fields:
 * $g_login['id'] : integer
 * $g_login['username'] : string
 * $g_login['realname'] : string
 * $g_login['display_name'] : string - wybrany realname lub username
 * $g_login['enabled'] : integer
 * $g_login['mantis_access_level'] : integer
 * $g_login['access_level'] : integer
 * $g_login['weight'] : float
  */
function login()
{
	global $g_login;
	
	$cookie = $_COOKIE['MANTIS_STRING_COOKIE'];
	if (!$cookie) return;
	
	$query = sprintf("select
		mantis_user_table.id id, mantis_user_table.username username, mantis_user_table.realname realname, mantis_user_table.enabled enabled, mantis_user_table.access_level mantis_access_level,
		tests_users.access_level access_level, tests_users.weight weight,
		IF (realname = '', username, realname) display_name 
		from tests_users join mantis_user_table
		on tests_users.id_user = mantis_user_table.id
		where mantis_user_table.cookie_string=\"%s\"",
		mysql_real_escape_string($cookie));
	$result = mysql_query($query);
	if (!$result)
		die(mysql_error());
	$row = mysql_fetch_assoc($result);
	if (!$row) return;
	
	$g_login = $row;
	return true;
}

// If not logged in, returns 0.
function current_user_access_level()
{
	global $g_login;
	if (!$g_login) return 0;
	return $g_login['access_level'];
}

function current_user_can_edit_functionalities() { return current_user_access_level() <= ADMIN; }
function current_user_can_edit_tests()           { return current_user_access_level() <= ADMIN; }

function print_page_begin($title, $h1Title = '', $additional_head_html = '')
{
	global $g_login;
	$html_title = htmlspecialchars($title);
	if ($h1Title == '')
		$h1Title = $html_title; 
	?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?php print($html_title); ?></title>
	<script src="javascript/jquery-1.6.min.js" type="text/javascript"></script>
	<script src="javascript/jquery-ui-1.8.13.custom.min.js" type="text/javascript"></script>
	<link rel="STYLESHEET" type="text/css" href="tests_styles.css">
	<?php print($additional_head_html); ?>
</head>
<body>
<div id="header1" style="height:96px;">
<div id="header" style="background-color:#2D3134; color:white; height:64px" class="header_font">
	<!-- <a href="index.php"><span style="font-size:24px; float-left; margin-right:16px; padding:4px">mantis</span></a> -->
	<a href="index.php"><img border="0" height="64" alt="Mantis Bug Tracker" src="images/sautis-logo.png" /></a>
	<!-- <a href="tests_index.php"><span style="font-size:16px; padding:4px">Awesome Test Management System ATMS</span></a> -->
	<a href="tests_index.php"><img border="0" height="64" alt="Mantis Bug Tracker" src="images/atms-logo.png" /></span></a>
	<!-- <span style="font-size:10px; padding:4px"><?php global $g_ATMS_Version; echo 'v'.$g_ATMS_Version; ?></span> -->
	
	<?php 
	if ($g_login != null)
	{
		echo '
	<ul class="menu">
		<li><a href="tests_tests.php">Tests</a></li>';
		if (current_user_can_edit_functionalities())
			print("<li><a href=\"tests_functionalities.php\">Functionalities</a></li>\n");
		
		echo '
		<li><a href="tests_planned_soft.php">Planned Software</a></li>
		<li><a href="tests_users.php">Users</a></li>
	</ul>';
	}
		echo '<span class="login_info">';
		if ($g_login)
			printf("Hello <a href=\"account_page.php\">%s</a><br><a href=\"logout_page.php\">Logout</a>", $g_login['realname']);
		else
			print("Not logged in.<br><a href=\"login_page.php\">Login</a>");
	echo '</span>';
	?>
</div></div>
<div style="margin:8px;">
<h1><?php print($h1Title); ?></h1>
<div id="browser_warning"></div>
<script type="text/javascript">
$(function() {
	if ($.browser.msie && ($.browser.version == '6.0' || $.browser.version == '7.0' || $.browser.version == '8.0'))
	{
		var p = $('<p class="info" />').text('You use old Internet Explorer browser. It\'s a bad browser in which many things don\'t work or work wrong. You better use Firefox, Chrome, Opera or anything other. It\'s not the matter of taste, it\'s a real technical problem. You have been warned.');
		$('#browser_warning').append(p);
	}
} );
</script>
<?php	
}

function print_page_end()
{
	print("</div></body></html>");
}

function print_error_paragraph($msg)
{
	printf("<p class=\"error\"><b>ERROR:</b> %s</p>\n", htmlspecialchars($msg));
}

$javascript_escape_string_translation_table = array(
	'\\' => '\\\\',
	'"'  => '\\"',
	'\'' => '\\\'',
);

function javascript_escape_string($s)
{
	global $javascript_escape_string_translation_table;
	return strtr($s, $javascript_escape_string_translation_table);
}

/*
// Commented out, because server with Mantis doesn't have iconv installed :(
//function iso_to_utf($s) { return iconv('ISO-8859-2', 'UTF-8//TRANSLIT', $s); }
//function utf_to_iso($s) { return iconv('UTF-8', 'ISO-8859-2//TRANSLIT', $s); }
		
$latin2_to_utf8_table = array(
   "\xB1" => "\xC4\x85", "\xE6" => "\xC4\x87", "\xEA" => "\xC4\x99",
   "\xB3" => "\xC5\x82", "\xF1" => "\xC5\x84", "\xF3" => "\xC3\xB3",
   "\xB6" => "\xC5\x9B", "\xBC" => "\xC5\xBA", "\xA1" => "\xC4\x84",
   "\xC6" => "\xC4\x86", "\xCA" => "\xC4\x98", "\xA3" => "\xC5\x81",
   "\xD1" => "\xC5\x83", "\xD3" => "\xC3\x93", "\xA6" => "\xC5\x9A",
   "\xAF" => "\xC5\xBB", "\xAC" => "\xC5\xB9", "\xBF" => "\xC5\xBC",
);
$utf8_to_latin2_table = array(
   "\xC4\x85" => "\xB1", "\xC4\x87" => "\xE6", "\xC4\x99" => "\xEA",
   "\xC5\x82" => "\xB3", "\xC5\x84" => "\xF1", "\xC3\xB3" => "\xF3",
   "\xC5\x9B" => "\xB6", "\xC5\xBA" => "\xBC", "\xC4\x84" => "\xA1",
   "\xC4\x86" => "\xC6", "\xC4\x98" => "\xCA", "\xC5\x81" => "\xA3",
   "\xC5\x83" => "\xD1", "\xC3\x93" => "\xD3", "\xC5\x9A" => "\xA6",
   "\xC5\xBB" => "\xAF", "\xC5\xB9" => "\xAC", "\xC5\xBC" => "\xBF",
);

function iso_to_utf($s)
{
	global $latin2_to_utf8_table;
	return strtr($s, $latin2_to_utf8_table);
}
function utf_to_iso($s)
{
	global $utf8_to_latin2_table;
	return strtr($s, $utf8_to_latin2_table);
}
*/

function check_is_test_user($id_user)
{
	$query = "SELECT * FROM tests_users where id_user=".$id_user;
	$result = mysql_query( $query );
	$row = mysql_fetch_assoc($result);

	if ($row != null)
		return true;
	return false;		
}	
	
function get_user_access_level($id_user)
{
	$query = "SELECT access_level FROM tests_users WHERE id_user=".$id_user;
	$result = mysql_query( $query );
	if ($result !== null)
	{
		$row = mysql_fetch_assoc($result);
		return $row['access_level'];
	}
	else
		return $result;
}
	
function get_user_name($id_user)
{
	$query = "SELECT if(realname='', username, realname) name FROM mantis_user_table WHERE id=\"".$id_user."\"";
	$result = mysql_query( $query );
	if ($result !== null)
	{
		$row = mysql_fetch_assoc($result);
		return $row['name'];
	}
	else
		return $result;
}

function get_user_weight($id_user)
{
	$query = "SELECT weight FROM tests_users WHERE id_user=".$id_user;
	$result = mysql_query( $query );
	if ($result !== null)
	{
		$row = mysql_fetch_assoc($result);
		return $row['weight'];
	}
	else
		return $result;
}

function get_users_list()
{
	$query = "SELECT u.*, mt.id, realname FROM tests_users u JOIN mantis_user_table mt ON u.id_user=mt.id ORDER BY realname;";
	$result = mysql_query( $query );
	return $result;
}

function get_mantis_users_list()
{
	$query = "SELECT * FROM mantis_user_table WHERE id NOT IN (SELECT id_user FROM tests_users) ORDER BY realname";
	$result = mysql_query( $query );

	return $result;	
}

function redirect($p_url, $p_time = null)
{
	if ( null === $p_time )
		$p_time = 0;

	echo "\t<meta http-equiv=\"Refresh\" content=\"$p_time;URL=$p_url\" />\n";

	return true;
}

function print_back_link($link)
{
	echo '<div class="BackLink"><a href="'.$link.'">Powrót</a></div>';
}

function PrintProjectFilter($backLink)
{
	$currID = $_SESSION['gProjectFilter'];
	
	print ("<form method=\"get\" action=\"".$backLink."\">");
	print("<input type=\"hidden\" value=\"1\" name=\"ProjectFilter\" />");
	print("<table class=\"ProjectFilter\" width=\"100%\" style=\"background-color:#3D4144;\"><tr>
	<td width=\"100\">Project:</td>
	<td width=\"200\">
		<select name=\"tests_project_id\">");
	
	print("<option value=\"-1\" ");
	if ($currID == -1) $sel = 'selected="selected"'; else $sel = '';
	print($sel.">All</option>\n");

	$query = "SELECT * FROM mantis_project_table WHERE enabled=1 ORDER BY name;";
	$result = mysql_query($query);
	
	if ($result)
	{
		while($row = mysql_fetch_assoc($result))
		{
			$id = $row['id'];
			print "<option value=\"$id\"";
			if ($currID == $id) $sel = 'selected="selected"'; else $sel = '';					
			print $sel .">".$row['name']."</option>\n";
		}
	}
	print("</select>	
	</td>
	<td width=\"100\">
	<input type=\"submit\" class=\"button\" value=\"Filter\" />
	</td>
	<td></td>
	</tr></table></form>");
}

function get_project_name($id_project)
{
	$query = "SELECT name, id FROM mantis_project_table WHERE id=".$id_project;
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);
	return $row['name'];
}

function get_planned_soft_data($id_planned_soft)
{
	$query = "SELECT * FROM tests_planned_soft WHERE id_planned_soft=".$id_planned_soft;
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);
	return $row;
}

function get_test_data($testID)
{
	$query = 'SELECT t.*, ps.name planned_name, ps.planned_date, ps.description planned_description 
			FROM tests_tests t LEFT JOIN tests_planned_soft ps on t.id_planned_soft=ps.id_planned_soft  
			WHERE id_test='.$testID;
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);
	return $row;
}

function get_test_testers($testID)
{
	$query = 'SELECT t.*, mut.realname, mut.username 
			FROM tests_tasks t JOIN mantis_user_table mut ON t.id_user=mut.id 
			WHERE id_test='.$testID;
	$result = mysql_query($query);
	return $result;	
}

function get_cases_statistics($id_tests_task)
{
	$query = 'SELECT count(*) count, tests_test_cases.*, id_tests_task, 
			sum(case when (result <> 0) then 1 else 0 end) closed,
			sum(case when (result = 0) then 1 else 0 end) open
			FROM tests_test_cases 
			WHERE id_tests_task='.$id_tests_task.';';
	$result = mysql_query($query);
	
	$row = mysql_fetch_assoc($result);
	$stat['closed'] = $row['closed'];	
	$stat['open'] = $row['open'];
	$stat['all'] = $stat['closed'] + $stat['open'];
	
	$queryDet = 'SELECT count(*) count, ttc.id_tests_cases, sum(case when (ttc.result = 0) then 1 else 0 end) open,
				 ttc.id_tests_task, tc.chapter_id FROM tests_test_cases ttc 
				 JOIN tests_cases tc ON tc.id=ttc.id_tests_cases 
				 WHERE ttc.id_tests_task = '.$id_tests_task.' 
				 GROUP BY chapter_id;';
	$resultDet = mysql_query($queryDet);
	
	$queryFunc = 'SELECT count(*) count, ttc.id_tests_cases, sum(case when (ttc.result = 0) then 1 else 0 end) open,
				ttc.id_tests_task, tc.chapter_id, tch.functionality_id 
        		FROM tests_test_cases ttc 
        		JOIN tests_cases tc ON tc.id=ttc.id_tests_cases 
        		JOIN tests_chapters tch ON tch.id=tc.chapter_id 
        		WHERE ttc.id_tests_task = '.$id_tests_task.' 
        		GROUP BY tch.functionality_id;';
	$resultFunc = mysql_query($queryFunc);
	
	
	$stat['chapters'] = array(); 
	while($rowDet = mysql_fetch_assoc($resultDet))
	{
		$stat['chapters'][$rowDet['chapter_id']]['closed'] = $rowDet['count'] - $rowDet['open'];			 
		$stat['chapters'][$rowDet['chapter_id']]['open'] = $rowDet['open'];
		$stat['chapters'][$rowDet['chapter_id']]['all'] = $rowDet['count'];
	}
	
	$stat['functionalities'] = array(); 
	while($rowFunc = mysql_fetch_assoc($resultFunc))
	{
		$stat['functionalities'][$rowFunc['functionality_id']]['closed'] = $rowFunc['count'] - $rowFunc['open'];	 
		$stat['functionalities'][$rowFunc['functionality_id']]['open'] = $rowFunc['open'];
		$stat['functionalities'][$rowFunc['functionality_id']]['all'] = $rowFunc['count'];
	}
	
	return $stat;
}

function get_task_data($taskID)
{
	$query = 'SELECT * FROM tests_tasks WHERE id_tests_task='.$taskID.';';
	$result = mysql_query($query);
	
	$row = mysql_fetch_assoc($result);
	return $row;
}

function get_func_chap_data($testID)
{
	if (!$testID)
		return null;
	
	$query = 'SELECT tf.id, tf.name, tf.length, count(*) cases_count 
				FROM tests_cases tc JOIN tests_chapters tch on tc.chapter_id=tch.id JOIN tests_functionalities tf ON tch.functionality_id=tf.id 
				WHERE tc.active=1 AND tch.active=1 GROUP BY tf.id ORDER BY tf.name;';
	$func_result = mysql_query($query);	

	if ($func_result === false) { print_error_paragraph(mysql_error()); return; }
	
	$query = 'SELECT tch.id, tch.functionality_id, tch.name, tch.order_number, count(*) cases_count 
				FROM tests_chapters tch 
				JOIN tests_cases tc ON tch.id=tc.chapter_id 
				WHERE tch.active=1 AND tc.active=1 GROUP BY tch.id ORDER BY tch.order_number;';	
	$chapters_result = mysql_query($query);
	
	if ($chapters_result === false) { print_error_paragraph(mysql_error()); return; }

	$funcs = array();

	while ($func_row = mysql_fetch_assoc($func_result))
	{
		$funcs[$func_row['id']] = array(
			'id' => $func_row['id'],
			'name' => $func_row['name'],
			'length' => $func_row['length'],
			'cases_count' => $func_row['cases_count'],
			'case_time_ratio' => 0,
			'open_cases' => $func_row['cases_count'],
			'assigned_chapters' => 0,
			'chapters' => array()
		);						
		if ($func_row['cases_count'] != 0)
			$funcs[$func_row['id']]['case_time_ratio'] = $func_row['length'] / $func_row['cases_count'];
		else $funcs[$func_row['id']]['case_time_ratio'] = 0;
	}

	while($chapter_row = mysql_fetch_assoc($chapters_result))
	{
		$funcs[$chapter_row['functionality_id']]['chapters'][$chapter_row['id']] = array(
			'id' => $chapter_row['id'],
			'name' => $chapter_row['name'],
			'active' => $chapter_row['active'],
			'order_number' => $chapter_row['order_number'],
			'cases_count' => $chapter_row['cases_count'],
			'open_cases' => $chapter_row['cases_count'], 
			'user' => 0,
		);			
	}

	$query = 'SELECT tch.functionality_id, tc.chapter_id, tch.active, tt.id_test, sum(case when (ttc.result <> 0) then 1 else 0 end) closed_cases
				FROM tests_test_cases ttc 
				JOIN tests_tasks tt ON ttc.id_tests_task=tt.id_tests_task 
				JOIN tests_cases tc ON ttc.id_tests_cases=tc.id
				JOIN tests_chapters tch ON tc.chapter_id=tch.id
				WHERE id_test = '.$testID.' and tch.active=1 AND tc.active=1
				GROUP BY chapter_id;';	
	$cases_result = mysql_query($query);
	if ($cases_result === false) { print_error_paragraph(mysql_error()); return; }
	
	while($cases_row = mysql_fetch_assoc($cases_result))
	{	
		$funcs[$cases_row['functionality_id']]['chapters'][$cases_row['chapter_id']]['open_cases'] -= $cases_row['closed_cases'];			
		$funcs[$cases_row['functionality_id']]['open_cases'] -= $cases_row['closed_cases'];
	}
	
	$query = 'select tests_tasks.id_tests_task, sub.* from tests_tasks
				join 
				(
				select tt.id_tests_task, tt.id_user, tch.functionality_id, tc.chapter_id, ttc.admin_change_time from tests_tasks tt 
				join tests_test_cases ttc ON tt.id_tests_task=ttc.id_tests_task
				join tests_cases tc ON ttc.id_tests_cases=tc.id
				join tests_chapters tch ON tc.chapter_id=tch.id
				where id_test='.$testID.' order by ttc.admin_change_time desc
				) sub
				on tests_tasks.id_tests_task=sub.id_tests_task
				group by sub.chapter_id;';
	
	$user_result = mysql_query($query);	
	if ($user_result === false)
	{
		print_error_paragraph(mysql_error());
	}
	
	while($user_row = mysql_fetch_assoc($user_result))
	{
		if ($funcs[$user_row['functionality_id']] == null)
		{
			$fData = get_functionality_data($user_row['functionality_id']);
			$funcs[$user_row['functionality_id']] = array(
				'id' => $user_row['functionality_id'],
				'name' => $fData['name'],
				'length' => $fData['length'],
				'cases_count' => $func_row['cases_count'],
				'case_time_ratio' => 0,
				'open_cases' => $func_row['cases_count'],
				'assigned_chapters' => 0,
				'chapters' => array());
		}
		
		if ($funcs[$user_row['functionality_id']]['chapters'][$user_row['chapter_id']] == null)
		{
			$chData = get_chapter_data($user_row['chapter_id']);
			$funcs[$user_row['functionality_id']]['chapters'][$user_row['chapter_id']] = array(
				'id' => $user_row['chapter_id'],
				'name' => $chData['name'],
				'active' => $chData['active'],
				'order_number' => $chData['order_number'],
				'cases_count' => $chData['cases_count'],
				'open_cases' => $chData['cases_count'], 
				'user' => 0,
			);						
		}
		
		$funcs[$user_row['functionality_id']]['users'][$user_row['id_user']]['count'] += 1;  		
		$funcs[$user_row['functionality_id']]['chapters'][$user_row['chapter_id']]['user'] = $user_row['id_user'];
		$funcs[$user_row['functionality_id']]['assigned_chapters'] += 1;
	}		
	
	return $funcs;
}

function get_users_task_times($testID)
{
	if (!$testID)
		return null;
	
	$query = 'SELECT tf.id, tf.name, tf.length, count(*) cases_count 
				FROM tests_cases tc JOIN tests_chapters tch on tc.chapter_id=tch.id JOIN tests_functionalities tf ON tch.functionality_id=tf.id 
				WHERE tc.active=1 AND tch.active=1 GROUP BY tf.id ORDER BY tf.name;';
	$func_result = mysql_query($query);	

	if ($func_result === false) { print_error_paragraph(mysql_error()); return; }
	
	$query = 'SELECT tch.id, tch.functionality_id, tch.name, tch.order_number, count(*) cases_count 
				FROM tests_chapters tch 
				JOIN tests_cases tc ON tch.id=tc.chapter_id 
				WHERE tch.active=1 AND tc.active=1 GROUP BY tch.id ORDER BY tch.order_number;';	
	$chapters_result = mysql_query($query);
	
	if ($chapters_result === false) { print_error_paragraph(mysql_error()); return; }

	$funcs = array();

	while ($func_row = mysql_fetch_assoc($func_result))
	{
		$funcs[$func_row['id']] = array(
			'id' => $func_row['id'],
			'name' => $func_row['name'],
			'length' => $func_row['length'],
			'cases_count' => $func_row['cases_count'],
			'case_time_ratio' => 0,
			'open_cases' => $func_row['cases_count'],
			'chapters' => array()
		);						
		if ($func_row['cases_count'] != 0)
			$funcs[$func_row['id']]['case_time_ratio'] = $func_row['length'] / $func_row['cases_count'];
		else $funcs[$func_row['id']]['case_time_ratio'] = 0;
	}

	while($chapter_row = mysql_fetch_assoc($chapters_result))
	{
		$funcs[$chapter_row['functionality_id']]['chapters'][$chapter_row['id']] = array(
			'id' => $chapter_row['id'],
			'name' => $chapter_row['name'],
			'active' => $chapter_row['active'],
			'order_number' => $chapter_row['order_number'],
			'cases_count' => $chapter_row['cases_count'],
			'open_cases' => $chapter_row['cases_count'], 
		);			
	}

	$query = 'select tt.id_user, tch.functionality_id, tc.chapter_id, ttc.result
				from tests_test_cases ttc 
				join tests_tasks tt on tt.id_tests_task=ttc.id_tests_task
				join tests_cases tc on ttc.id_tests_cases=tc.id
				join tests_chapters tch on tc.chapter_id=tch.id
				where tt.id_test = '.$testID.';
	';
	$users_result = mysql_query($query);
	
	$users_times = array();
	
	while($user_row = mysql_fetch_assoc($users_result))
	{
		if ($user_row['result'] == 0)
			$users_times[$user_row['id_user']]['open_time'] += $funcs[$user_row['functionality_id']]['case_time_ratio'];
		
		$users_times[$user_row['id_user']]['all_time'] += $funcs[$user_row['functionality_id']]['case_time_ratio'];
	}
	
	return $users_times;
}

function get_users_test_load($test_id)
{
	$query = 'SELECT tch.functionality_id, tc.chapter_id, tt.id_user, sum(ttc.result) sum 
				FROM tests_tasks tt 
				JOIN tests_test_cases ttc ON tt.id_tests_task=ttc.id_tests_task 
				JOIN tests_cases tc ON ttc.id_tests_cases=tc.id
				JOIN tests_chapters tch ON tch.id=tc.chapter_id
				WHERE tt.id_test = '.$test_id.'				
				GROUP BY tc.chapter_id;';
	
	$result = mysql_query($query);	
	if ($result === false)
	{
		print_error_paragraph(mysql_error());
	}
	
	$funcs = array();
	
	while($row = mysql_fetch_assoc($result))
	{
		$funcs[$row['functionality_id']]['users'][$row['id_user']]['open'] += ($row['sum'] == 0) ? 1 : 0;  		
		$funcs[$row['functionality_id']]['chapters'][$row['chapter_id']] = $row['id_user'];
	}
	
	return $funcs;
}

function make_bug_view_links($string)
{
	global $g_server_host;
	$string = preg_replace("/(#)(\d+)/", '<a style="color:#0000ff; text-decoration:underline;" href="'.$g_server_host.'view.php?id=$2">$0</a>', $string);
	return $string;	
}

function get_functionality_data($funcID)
{
	$query = 'SELECT tf.id, tf.name, tf.length, count(*) cases_count 
				FROM tests_cases tc JOIN tests_chapters tch on tc.chapter_id=tch.id JOIN tests_functionalities tf ON tch.functionality_id=tf.id 
				WHERE tc.active=1 AND tch.active=1 AND tf.id='.$funcID.';';	
	$result = mysql_query($query);	
	return mysql_fetch_assoc($result);
}

function get_chapter_data($chapID)
{
	$query = 'SELECT tch.id, tch.functionality_id, tch.name, tch.order_number, count(*) cases_count 
				FROM tests_chapters tch 
				JOIN tests_cases tc ON tch.id=tc.chapter_id 
				WHERE tc.active=1 AND tch.id='.$chapID.';';	
	$result = mysql_query($query);	
	return mysql_fetch_assoc($result);
}

function get_task_data2($testID, $userID)
{
	$query = 'SELECT * FROM tests_tasks JOIN tests_users ON tests_tasks.id_user = tests_users.id_user WHERE id_test='.$testID.' and tests_users.id_user='.$userID.';';
	$result = mysql_query($query);
	
	$row = mysql_fetch_assoc($result);
	return $row;	
}

function get_test_case_data($testCaseID)
{
	$query = 'SELECT ttc.id_tests_task, tt.id_test  
				FROM tests_test_cases ttc JOIN tests_tasks tt ON ttc.id_tests_task = tt.id_tests_task 
				WHERE id_tests_test_cases='.$testCaseID.';';
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);
	return $row;	
}
?>