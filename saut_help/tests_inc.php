<?php

function dod_check_is_test_user($id_user)
{
	$query = "SELECT * FROM dod_test_users where id_user=".$id_user;
	$result = db_query( $query );
	while ($row = db_fetch_array($result))
	{
		if ($row['access_level'] >= 75 && dod_is_test_user_exist($row['id_user'])!== null)
			return true;	
	}
	return false;		
}	
	
function dod_is_test_user_exist($id_user)
{
	$query = "SELECT id FROM mantis_user_table WHERE id=".$id_user;
	$result = db_query( $query );
	if ($result !== null)
	{
		$row = db_fetch_array($result);
		return $row['id'];
	}
	else
		return $result;
}
	
function check_is_admin($id_user)
{	
	$query = "SELECT * FROM dod_test_users where id_user=".$id_user;
	$result = db_query( $query );
	while ($row = db_fetch_array($result))
	{
		if ($row['access_level'] == 100 && dod_is_test_user_exist($row['id_user'])!== null)
			return true;	
	}
	return false;		
}
?>