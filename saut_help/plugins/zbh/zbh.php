<?php

class zbhPlugin extends MantisPlugin {
 
  function register() {
    $this->name        = 'zbh Charts';
    $this->description = lang_get('zbh_plugin_desc');

    $this->version     = '0.1';
    $this->requires    = array(
      'MantisCore'       => '1.2.0',
  );

    $this->author      = 'Zbigniew Pomianowski';
    $this->contact     = 'zpomianowski@cyfrowypolsat.pl';
    $this->url         = '';
  }

  function init() {
    $g_dupa = 'dupa';
    //plugin_event_hook( 'EVENT_PLUGIN_INIT', 'header' );
  }

  function hooks() {
    $hooks = array(
        'EVENT_MENU_SUMMARY' => 'summary_menu',
    );
    return $hooks;
  }

  public function summary_menu() {
    return array( '<a class="ajax-links" href="' . plugin_page( 'zbh_main.php' ) . '">' . lang_get(zbh_plugin_summary_menu_link) . '</a>', );
  }  
}