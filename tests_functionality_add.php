<?php
// Kodowanie UTF-8, test: Zażółć gęślą jaźń
// Jeśli dostanie $_POST['new'], tworzy nową funkcjonalność, dodaje ją do bazy danych, potem przekierowuje na tests_functionality.php?id=$ID

require_once('tests_main.php');

if (!$_POST['new'])
	die("Required POST parameter: new=1.");

send_header_no_cache();
connect_to_database();
if (!login())
	die("Access Denied");

if (!current_user_can_edit_functionalities())
	die("Access denied.");
$name = sprintf('Functionality %s', strftime('%c'));
$query = sprintf("insert into tests_functionalities (name, description) values (\"%s\", \"\")",
	mysql_real_escape_string($name) );
mysql_query($query);
$functionality_id = mysql_insert_id();

$first_chapter_query = sprintf("insert into tests_chapters (functionality_id, name, description, order_number, active)
	values (%d, \"Chapter 1\", \"\", 0, 1)",
	$functionality_id);
mysql_query($first_chapter_query);

header("Location: tests_functionality.php?id=" . $functionality_id);
?>