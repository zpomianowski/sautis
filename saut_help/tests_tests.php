<?php
// Lista testów
// Zażółć gęślą jaźń
require_once('tests_main.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

print_page_begin('Tests');

$can_edit_tests = current_user_can_edit_tests();

echo '<div class="Content">';
$currentProject = ($_SESSION['gProjectFilter']) ? $_SESSION['gProjectFilter'] : -1;

if ($_GET['ProjectFilter'] == 1)
{
	echo 'ojoj';
	$currentProject = $_GET['tests_project_id'];
	$_SESSION['gProjectFilter'] = $currentProject;
}

echo '<br/>';
PrintProjectFilter('tests_tests.php');
echo '<br/>';	

if ($can_edit_tests)
	print("<div><p style=\"text-align: left;\"><button onclick=\"window.location='tests_test.php?id=new';\">New Test</button></p></div>\n");
print('<h3>Tests List</h3>');

print('
<table class="TableData Select">
<!-- Header -->
<tr class="Header">
	<th width="10%">Project</th>
	<th width="150">Software Version</th>
	<th>Description</th>
	<th width="200">Planned Software Name</th>
	<th width="100">Software Delivery</th>
	<th width="110">Start</th>
	<th width="110">End</th>
	<th width="150">Status</th>
	<th width="40">
		<a style="DISPLAY:block;width:100%;height:100%;" href="tests_tests.php?preview=-1"><img src="images/show.png" alt="Expand" border="0" /></a>	
	</th>
</tr>');

if ($currentProject == -1)
{
	$query = "SELECT t.*, p.name planned_name, mpt.name project_name 
			FROM tests_tests t 
			LEFT JOIN tests_planned_soft p ON t.id_planned_soft=p.id_planned_soft 
			JOIN mantis_project_table mpt ON t.id_project=mpt.id 
			ORDER BY software_arrive_date DESC, start_date DESC;";
}
else
{
	$query = "SELECT t.*, p.name planned_name, mpt.name project_name 
			FROM tests_tests t 
			LEFT JOIN tests_planned_soft p ON t.id_planned_soft=p.id_planned_soft 
			JOIN mantis_project_table mpt ON t.id_project=mpt.id  
			WHERE t.id_project=".$currentProject."  
			ORDER BY software_arrive_date DESC, start_date DESC;";
}

$result = mysql_query($query);

if ($result == null)
	print_error_paragraph(mysql_error());

while ($row = mysql_fetch_assoc($result))
{
	$text = htmlspecialchars($row['description']);
	$now = strftime("%Y-%m-%d", strtotime("now"));
	$nowDate = strtotime($now);
	$startDate = strtotime($row['start_date']);
	$id_test = $row['id_test'];
	
	if ($row['stop_date'] != null)
	{
		$class = "Closed";
		$status = 'Closed '.'[ '.get_software_status_text($row['software_status']).' ]';
	}
	else if ($row['start_date'] && ($startDate <= $nowDate))
	{
		$class = "InProgress";		
		$status = 'In Progress';
	}
	else if (($row['start_date'] && ($startDate > $nowDate)) || !$row['start_date'])
	{
		$class = "Waiting";
		$status = 'Waiting';
	}
	else
	{
		$class = "Unknown";
		$status = 'Unknown';
	}							

	$linkStart = '<a style="display: block;width:100%;height:100%;" href="tests_test_details.php?testID='.$row[id_test].'">';
	$linkEnd = '</a>';
	echo '<tr class="Normal">';
	echo '<td class="TestRow">'.$linkStart.$row['project_name'].$linkEnd.'</td>';
	echo '<td class="TestRow">'.$linkStart.htmlspecialchars($row['name']).$linkEnd.'</td>';
	if ($_GET['preview'] == $id_test || $_GET['preview'] == -1 || (strlen($text)<80))
		echo '<td class="TestRow" style="white-space: pre-wrap;">'.$linkStart.$text.$linkEnd.'</td>';
	else
		echo '<td class="TestRow">'.$linkStart.mb_substr($text, 0, 80, "utf-8").$linkEnd.'</td>';
	echo '<td class="TestRow">'.$linkStart.htmlspecialchars($row['planned_name']).$linkEnd.'</td>';
	echo '<td class="TestRow DateCell">'.$linkStart.$row['software_arrive_date'].$linkEnd.'</td>';
	echo '<td class="TestRow DateCell">'.$linkStart.$row['start_date'].$linkEnd.'</td>';
	if ($row['stop_date'])
		echo '<td class="TestRow DateCell">'.$linkStart.$row['stop_date'].$linkEnd.'</td>';
	else if($row['estimated_stop_date'])
		echo '<td class="TestRow DateCell" nowrap>'.$linkStart.$row['estimated_stop_date'].'<br>[ estimated ]'.$linkEnd.'</td>';
	else
		echo '<td class="TestRow DateCell" nowrap>'.$linkStart.$linkEnd.'</td>';
	
	echo '<td class="' . $class .'">'.$linkStart.$status.$linkEnd.'</td>';
	
	echo '<td>';
	$preview_visibility = strlen($text)>80 ? 'visible' : 'hidden';
	echo '<a href="tests_tests.php?preview='.$id_test.'" style="visibility:'.$preview_visibility.'"><img src="images/show.png" alt="Expand" border="0"/></a>';
	if ($can_edit_tests)
		printf(" <a href=\"tests_test.php?id=%d\"><img src=\"images/edit.png\" alt=\"Edit\" title=\"Edit\"></a>", $id_test);
	echo '</td>';
	
	echo '</tr>';
}
echo '</table>';

?>
</div>
<div class="Palette">
<table id="defColors" cellspacing="1">
<tr>
<td class="Waiting">Waiting</td>
<td class="InProgress">In Progress</td>
<td class="Closed">Closed</td>
<td class="Unknown">Unknown</td>
</tr></table>
</div>
 
<?php print_page_end(); ?>