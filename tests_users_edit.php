<?php
// Dodawanie i edycja użytkowników
// Zażółć gęślą jaźń

require_once('tests_main.php');

connect_to_database();
if (!login())
	die("Access Denied");

	
if (current_user_access_level() != ADMIN)
	die("Access Denied");

if ($_POST['action'] == "delete")
{
	$user = $_POST['user'];

	$query = "DELETE FROM tests_users WHERE id_user=".$user;
	$result = mysql_query( $query );
	
	$t_redirect = "tests_users.php";
	redirect($t_redirect, 0);		
}
else if ($_GET['action'] == "edit")
{
	$user = $_GET['User'];
	$access = $_GET['Access'];
	$weight = $_GET['Weight'];
	
	$query = 'UPDATE tests_users SET access_level="'.$access.'", weight="'.$weight.'" WHERE id_user='.$user;
	$result = mysql_query( $query );
	
	$t_redirect = "tests_users.php";
	redirect($t_redirect, 0);		
}
else if ($_GET['action'] == "add")
{		
	$user = $_GET['User'];
	$access =$_GET['Access'];
	$weight = $_GET['Weight'];
	
	$query = "INSERT INTO tests_users (id_user, access_level, weight) ";
	$query .= "VALUES(".$user.", \"".$access."\", \"".$weight."\");";
	$r = mysql_query($query);
	if ($r === false)
	{
		send_html_header();
		print_error_paragraph(mysql_error());			
		echo '<br/>';	
		print_back_link('tests_users.php');
		echo '<br/>';	
		print_page_end();		
	}
	else
	{
		$t_redirect = "tests_users.php";
		redirect($t_redirect, 0);		
	}
}
else
{
$user = $_POST['user'];
send_html_header();

if ($user) print_page_begin('Edit User');
else print_page_begin('Add User');
?>	
<script type="text/javascript">
function CheckFields()
{
	var val = $('#Weight').val();
	if (val == '' || isNaN(val))
	{
		alert('Field Weight is required and must be a number!');
		return false;
	}
}
</script>

<br/>
<form method="get" action="tests_users_edit.php">
<?php if ($user) echo '<input type="hidden" name="action" value="edit"/>'; else echo '<input type="hidden" name="action" value="add"/>';?>
<table class="TableData"> 
<tr><th>User</th></tr>
<tr><td>
<table class="TableData">
<tr>
	<td class="category">
	<label>User<span style="color: #FF0000;">*</span>:</label>
	</td>
	<td class="value">

	<?php
	
	if ($user)
	{
		echo '<input name="User" type="hidden" value="'.$user.'" />';
		echo get_user_name($user);
	}
	else 
	{
		echo '<select name="User" id="User">';
		$r = get_mantis_users_list();
		if ($r != null)
		{
			while ($aRow = mysql_fetch_assoc($r))
			{
				echo '<option value="';
				if ($aRow['realname'] != '')
					$val = $aRow['realname'];
				else $val = $aRow['username'];
				
				echo $aRow['id'];
				echo '">';
				echo $val;
				echo "</option>\n";
			}
		}
		echo '</select>';
	}
	?>
	</td>
</tr>
<tr>
	<td class="category">
	<label for="Access">Permissions<span style="color: #FF0000;">*</span>:</label>
	</td>
	<td class="value">
	<select name="Access" id="Access">
	<?php		
	if ($user)
	{
		if (get_user_access_level($user) == ADMIN) $sel = 'selected="selected"'; else $sel = ''; 
		echo '<option '.$sel.' value="1">Admin</option>';
		if (get_user_access_level($user) == TESTER) $sel = 'selected="selected"'; else $sel = ''; 
		echo '<option '.$sel.' value="2">Tester</option>';
		if (get_user_access_level($user) == OBSERVER) $sel = 'selected="selected"'; else $sel = ''; 
		echo '<option '.$sel.' value="4">Observer</option>';
	}
	else 
	{
		echo '<option value="1">Admin</option>';
		echo '<option value="2">Tester</option>';
		echo '<option value="4">Observer</option>';
	}
	?>
	</select>
	</td>
</tr>
<tr>
	<td class="category">
	<label>Weight<span style="color: #FF0000;">*</span>:</label>
	</td>
	<td class="value">
		<?php 
		if ($user) 
			echo '<input type="text" name="Weight" id="Weight" value="'.get_user_weight($user).'" />';
		else echo '<input type="text" name="Weight" id="Weight" value="1" />'; 
		?>			
	</td>	
</tr>


<tr>
	<td class="category" ><span style="color: #FF0000;">*necessary</span>
	</td>
	<td class="value">
	<?php 
		if ($user) echo '<input type="submit" class="button" onclick="return CheckFields();" value="Set" />';
		else echo '<input type="submit" class="button" onclick="return CheckFields();" value="Add" />';
	?>
	</td>
</tr>
</table>
</td></tr>
</table>
</form>
<br>
<div class="ActionButton">
<form method="get" action="tests_users.php">
<input type="submit" class="button" value="Back">
</form>
</div>
	
<?php
print_page_end();
}	
?>
