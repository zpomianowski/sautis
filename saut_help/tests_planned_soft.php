<?php
// Zarządzanie Planowanym oprogramowaniem
// Zażółć gęślą jaźń
require_once('tests_main.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

print_page_begin('Planned Software');

echo '<div class="Content">';

$currentProject = ($_SESSION['gProjectFilter']) ? $_SESSION['gProjectFilter'] : -1;

if ($_GET['ProjectFilter'] == 1)
{
	$currentProject = $_GET['tests_project_id'];
	$_SESSION['gProjectFilter'] = $currentProject;
}

	
echo '<br/>';
PrintProjectFilter('tests_planned_soft.php');
echo '<br/>';	

if ((current_user_access_level() == ADMIN))
{
	echo '<div>';
	echo '<form method="post" action="tests_planned_soft_edit.php">';
	echo '<input type="hidden" name="action" value="newSoft">';
	echo '<input type="hidden" name="id_project" value="'.$currentProject.'">';
	echo '<input type="submit" class="button" value="Add Software">';
	echo '</form>';
	echo '</div><br/>';
}

print('<h3>Planned Software List</h3>');

print('
<table class="TableData Select">	
<!-- Header -->
<tr class="Header">
	<th width="10%">Project</th>
	<th width="20%">Name</th>
	<th width="120">Planned Delivery</th>
	<th>Description</th>
	<th width="180">Test Status</th>
	<th width="20">
		<a style="DISPLAY:block;width:100%;height:100%;" href="tests_planned_soft.php?preview=-1"><img src="images/show.png" alt="Expand" border="0" /></a>	
	</th>');
	
if (current_user_access_level() == ADMIN)
{ 
	print('<th width="230">Action</th>');
}		
print('</tr>');


if ($currentProject != -1)
	$query = "SELECT ps.active, ps.id_planned_soft, ps.id_project, ps.name, ps.description, DATE(ps.planned_date) as planned_date, 
			t.id_test, t.software_arrive_date, t.start_date, t.stop_date  
			FROM tests_planned_soft ps left join tests_tests t on ps.id_planned_soft=t.id_planned_soft 
			WHERE ps.id_project=".$currentProject." ORDER BY ps.planned_date DESC;";
else
	$query = "SELECT ps.active, ps.id_planned_soft, ps.id_project, ps.name, ps.description, DATE(ps.planned_date) as planned_date,  
			t.id_test, t.software_arrive_date, t.start_date, t.stop_date  
			FROM tests_planned_soft ps left join tests_tests t on ps.id_planned_soft=t.id_planned_soft  
			ORDER BY ps.planned_date DESC;";
	
$result = mysql_query( $query );
while ($row = mysql_fetch_assoc($result)) 
{ 	
	$testId = null;
	$status = "";
	$now = strftime("%Y-%m-%d", strtotime("now"));
	$nowDate = strtotime($now);
	$startDate = strtotime($row['start_date']);
	$datePlanned = strtotime($row['planned_date']);
	
	/*$testId = $row['id_test'];
	if ($testId != null)
	{	
		if ($row['stop_date'] != null)
		{
			echo '<tr class="Closed">';
			$status = 'Closed';
		}
		else if ($row['start_date'] && ($startDate <= $nowDate))
		{
			echo '<tr class="InProgress">';		
			$status = 'In Progress';
		}
		else
		{
			echo '<tr class="Waiting">';
			$status = 'Waiting';
		}							
	}
	else if ($row['active'] == 1)
		echo '<tr class="Waiting">';
	else 
	{
		echo '<tr class="Reject">';
		$status = 'Canceled';
	}	*/
	echo '<tr class="Normal">';
	
	echo '<td class="left">'.htmlspecialchars(get_project_name($row['id_project'])).'</td>';
	echo '<td class="left">'.htmlspecialchars($row['name']).'</td>';

	if ($datePlanned < $nowDate && ($testId == null) && ($row['active'] == 1))
		echo '<td class="left">'.$row['planned_date'].'&nbsp;&nbsp;<img src="images/exclamation.png" alt="Canceled" title="Canceled" border="0"></td>';
	else
		echo '<td class="left">'.$row['planned_date'].'</td>';
	
	$text = htmlspecialchars($row['description']);
  	if ($_GET['preview'] == $row['id_planned_soft'] || $_GET['preview'] == -1 || (strlen($text)<80))
		echo '<td class="left">'.$text.'</td>'; 
	else echo '<td class="left">'. mb_substr($text, 0, 80, "utf-8").' ...</td>';
	
	$testId = $row['id_test'];
	if ($testId != null)
	{	
		if ($row['stop_date'] != null)
		{
			echo '<td class="Closed">';
			$status = 'Closed';
		}
		else if ($row['start_date'] && ($startDate <= $nowDate))
		{
			echo '<td class="InProgress">';		
			$status = 'In Progress';
		}
		else
		{
			echo '<td class="Waiting">';
			$status = 'Waiting';
		}							
	}
	else if ($row['active'] == 1)
		echo '<td class="Waiting">';
	else 
	{
		echo '<td class="Reject">';
		$status = 'Canceled';
	}
	echo $status.'</td>';
	
	echo '<td>';
	if (strlen($text)>=80)
		print("<a  style=\"DISPLAY:block;width:100%;height:100%;\" href=\"tests_planned_soft.php?preview=".$row['id_planned_soft']."\"><img src=\"images/show.png\" alt=\"Expand\" border=\"0\" /></a>");
	echo '</td>';
	
	if (current_user_access_level() == ADMIN)
	{ 
		echo '<td>';
		
		echo '<form style="float: left;" method="post" action="tests_planned_soft_edit.php">';		
		echo '<input type="hidden" name="id_planned_soft" value="'.$row['id_planned_soft'].'" />';
		echo '<input type="hidden" name="id_project" value="'.$row['id_project'].'" />';
		echo '<input name="action" type="hidden" value="editSoft" />';
		echo '<input type="submit" class="button" value="Edit" />';
		echo '</form>';				
		
		echo '<form style="float: left;" method="post" action="tests_planned_soft_edit.php">';		
		echo '<input type="hidden" name="id_planned_soft" value="'.$row['id_planned_soft'].'" />';
		echo '<input type="hidden" name="id_project" value="'.$row['id_project'].'" />';
		echo '<input name="action" type="hidden" value="delete"/>';
		$q = 'Are you sure you want to delete ?'; 
		echo '<input type="submit" class="button" onclick="return confirm(\''.$q.'\')"  value="Delete" />';
		echo '</form>';						

		if ($testId == null)
		{
			echo '<form method="post" action="tests_planned_soft_edit.php">';		
			echo '<input type="hidden" name="id_planned_soft" value="'.$row['id_planned_soft'].'" />';
			echo '<input type="hidden" name="id_project" value="'.$row['id_project'].'" />';
			echo '<input name="action" type="hidden" value="deactive"/>';
			$q = 'Are you sure you want to deactivate ?'; 
			echo '<input type="submit" class="button" onclick="return confirm(\''.$q.'\')"  value="Deactivate" />';
			echo '</form>';
		}						
		echo '</td>';
	}		
	echo "</tr>\n";
}
echo '</table>';

?>
<br/><br/>
</div><div class="Palette">
<table id="defColors" cellspacing="1">
<tr>
<td class="Waiting" width="20%">Waiting</td>
<td class="InProgress" width="20%">In Progress</td>
<td class="Closed" width="20%">Closed</td>
<td class="Reject" width="20%">Canceled</td>
<td class="Unknown" width="20%">Unknown</td>
</tr></table>
</div>
 

<?php print_page_end(); ?>