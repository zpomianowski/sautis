<?php
// Kodowanie UTF-8, test: Zażółć gęślą jaźń
// Operacje na funkcjonalnościach - żądanie AJAX, zwraca XML

require_once('tests_main.php');
header('Content-type: text/xml; charset=utf-8');
send_header_no_cache();
send_xml_header();
connect_to_database();
if (!login())
	die("Access Denied");


function print_error($msg)
{
	printf("<error>%s</error>", htmlspecialchars($msg));
}

function functionality_delete()
{
	$id = $_POST['id'];
	if (is_numeric($id))
	{
		$result = mysql_query('delete from tests_functionalities where id='.$id);
		if (mysql_affected_rows() == 1)
			print("<ok/>");
		else
			print_error('Functionality not found.');
	}
	else
		print_error('Invalid functionality identifier.');
}

function functionality_fetch()
{
	$id = $_POST['id'];
	if (!is_numeric($id)) { print_error('Invalid functionality identifier.'); return; }

	$query = sprintf('select name, description, length from tests_functionalities where id=%d', $id);
	$result = mysql_query($query);
	if ($result === false) { print_error(mysql_error()); return; }
	$params_row = mysql_fetch_assoc($result);
	if (!$params_row) { print_error('Functionality not found.'); return false; }

	$query = sprintf("select id, name, description, order_number, active from tests_chapters where functionality_id=%d order by order_number, id", $id);
	$chapters_result = mysql_query($query);
	if ($chapters_result === false) { print_error(mysql_error()); return; }
	
	$query = sprintf("select
			tests_cases.id id,
			tests_cases.order_number order_number,
			tests_cases.chapter_id chapter_id,
			tests_cases.procedure_text procedure_text,
			tests_cases.expected_result expected_result,
			tests_cases.active active,
			tests_cases.last_update_time last_update_time,
			if (mantis_user_table.realname='', mantis_user_table.username, mantis_user_table.realname) last_update_user
		from tests_cases
		join tests_chapters
			on tests_cases.chapter_id=tests_chapters.id
		left join mantis_user_table
			on tests_cases.last_update_user = mantis_user_table.id
		where tests_chapters.functionality_id=%d
		order by tests_chapters.order_number, tests_chapters.id, tests_cases.order_number, tests_cases.id", $id);
	$cases_result = mysql_query($query);
	if ($cases_result === false) { print_error(mysql_error()); return; }

	$chapters = array();

	$case_row = mysql_fetch_assoc($cases_result);
	$last_chapter_id = 0;

	while ($case_row)
	{
		$chapter_id = $case_row['chapter_id'];

		while ($last_chapter_id != $chapter_id)
		{
			$chapter_row = mysql_fetch_assoc($chapters_result);
			$last_chapter_id = $chapter_row['id'];
			$chapters[] = array(
				'id' => $last_chapter_id,
				'name' => $chapter_row['name'],
				'active' => $chapter_row['active'],
				'description' => $chapter_row['description'],
				'order_number' => $chapter_row['order_number'],
				'cases' => array()
			);
		}

		$chapters[count($chapters)-1]['cases'][] = array(
			'id' => $case_row['id'],
			'order_number' => $case_row['order_number'],
			'active' => $case_row['active'],
			'procedure_text' => $case_row['procedure_text'],
			'expected_result' => $case_row['expected_result'],
			'last_update_time' => $case_row['last_update_time'],
			'last_update_user' => $case_row['last_update_user'],
		);

		$case_row = mysql_fetch_assoc($cases_result);
	}

	// Remaining chapters after all cases.
	while ($chapter_row = mysql_fetch_assoc($chapters_result))
	{
		$chapters[] = array(
			'id' => $chapter_row['id'],
			'name' => $chapter_row['name'],
			'active' => $chapter_row['active'],
			'description' => $chapter_row['description'],
			'order_number' => $chapter_row['order_number'],
			'cases' => array(),
		);
	}
		
	printf("<functionality length=\"%d\">\n", $params_row['length']);
	printf("<name>%s</name>\n", htmlspecialchars($params_row['name']));
	printf("<description>%s</description>\n", htmlspecialchars($params_row['description']));
	print("<chapters>\n");
	foreach ($chapters as $chapter)
	{
		printf("<chapter id=\"%d\" active=\"%d\" order_number=\"%d\">\n", $chapter['id'], $chapter['active'], $chapter['order_number']);
		printf("<name>%s</name>\n", htmlspecialchars($chapter['name']));
		printf("<description>%s</description>\n", htmlspecialchars($chapter['description']));
		foreach ($chapter['cases'] as $case)
		{
			$last_update_time = $case['last_update_time'];
			$last_update_time_xml = $last_update_time ? sprintf(" last_update_time=\"%s\"", htmlspecialchars($last_update_time)) : '';
			
			$last_update_user = $case['last_update_user'];
			$last_update_user_xml = $last_update_user ? sprintf(" last_update_user=\"%s\"", htmlspecialchars($last_update_user)) : '';
			
			printf("<case id=\"%d\" active=\"%d\" order_number=\"%d\"%s%s>\n",
				$case['id'],
				$case['active'],
				$case['order_number'],
				$last_update_time_xml,
				$last_update_user_xml);
			printf("<procedure_text>%s</procedure_text>\n", htmlspecialchars($case['procedure_text']));
			printf("<expected_result>%s</expected_result>\n", htmlspecialchars($case['expected_result']));
			print("</case>\n");
		}
		print("</chapter>\n");
	}
	print("</chapters>\n");
	print("</functionality>");
}

function functionality_set_name()
{
	$id = $_POST['id'];
	$name = StripInput(trim($_POST['name']));
	if (!is_numeric($id))
		print_error('Invalid functionality identifier.');
	else if ($name == '')
		print_error('Name cannot be empty.');
	else
	{
		$query = sprintf("update tests_functionalities set name=\"%s\" where id=%d",
			mysql_real_escape_string($name),
			$id);
		$result = mysql_query($query);
		if ($result)
			print("<ok/>");
		else
			print_error(mysql_error());
	}
}

function chapter_set_name()
{
	$id = $_POST['id'];
	$name = StripInput(trim($_POST['name']));
	if (!is_numeric($id))
		print_error('Invalid chapter identifier.');
	else if ($name == '')
		print_error('Chapter name cannot be empty.');
	else
	{
		$query = sprintf("update tests_chapters set name=\"%s\" where id=%d",
			mysql_real_escape_string($name),
			$id);
		$result = mysql_query($query);
		if ($result)
			print("<ok/>");
		else
			print_error(mysql_error());
	}
}

function chapter_set_description()
{
	$id = $_POST['id'];
	$description = StripInput(trim($_POST['description']));

	if (!is_numeric($id))
		print_error('Invalid chapter identifier.');
	
	$query = sprintf("update tests_chapters set description=\"%s\" where id=%d",
		mysql_real_escape_string($description),
		$id);
	$result = mysql_query($query);
	if ($result)
		print("<ok/>");
	else
		print_error(mysql_error());
}

function functionality_set_description()
{
	$id = $_POST['id'];
	$desc = StripInput(trim($_POST['description']));
	if (!is_numeric($id))
		print_error('Invalid functionality identifier.');
	else
	{
		$query = sprintf("update tests_functionalities set description=\"%s\" where id=%d",
			mysql_real_escape_string($desc),
			$id);
		$result = mysql_query($query);
		if ($result)
			print("<ok/>");
		else
			print_error(mysql_error());
	}
}

function functionality_set_length()
{
	$id = $_POST['id'];
	if (!is_numeric($id)) { print_error('Invalid functionality identifier.'); return; }
	$length = trim($_POST['length']);
	if (!is_numeric($length) || $length < 0) { print_error('Invalid length.'); return; }
	
	$query = sprintf("update tests_functionalities set length=\"%d\" where id=%d", $length, $id);
	$result = mysql_query($query);
	if ($result)
		print("<ok/>");
	else
		print_error(mysql_error());
}

function case_delete()
{
	global $g_login;
	$last_update_user_id = ($g_login && $g_login['id']) ? $g_login['id'] : 'null';
	$last_update_user_name = ($g_login && $g_login['display_name']) ? $g_login['display_name'] : '';
	$last_update_time = date('Y-m-d H:i:s');
	
	$id = $_POST['id'];
	if (!is_numeric($id)) { print_error('Invalid functionality identifier.'); return; }
	$query = sprintf("update tests_cases set active=0, last_update_user=%s, last_update_time=\"%s\" where id=%d",
		$last_update_user_id,
		mysql_real_escape_string($last_update_time),
		$id);
	$result = mysql_query($query);
	if ($result)
	{
		printf("<case last_update_user=\"%s\" last_update_time=\"%s\" />",
			htmlspecialchars($last_update_user_name),
			$last_update_time );
	}
	else
		print_error(mysql_error());
}

function chapter_delete()
{
	$id = $_POST['id'];
	if (!is_numeric($id)) { print_error('Invalid chapter identifier.'); return; }
	
	$case_count_result = mysql_query(sprintf('select count(*) from tests_cases where chapter_id=%d', $id));
	if ($case_count_result === false) { print_error(mysql_error()); return; }
	$case_count_row = mysql_fetch_row($case_count_result);
	$case_count = $case_count_row[0];
	
	if ($case_count)
	{
		$result = mysql_query(sprintf("update tests_chapters set active=0 where id=%d", $id));
		if (!$result) { print_error(mysql_error()); return; }
		print("<deactivated/>");
	}
	else
	{
		$result = mysql_query(sprintf("delete from tests_chapters where id=%d", $id));
		if (!$result) { print_error(mysql_error()); return; }
		print("<deleted/>");
	}
}

function case_edit()
{
	global $g_login;
	$last_update_user_id = ($g_login && $g_login['id']) ? $g_login['id'] : 'null';
	$last_update_user_name = ($g_login && $g_login['display_name']) ? $g_login['display_name'] : '';
	$last_update_time = date('Y-m-d H:i:s');
		
	$id = $_POST['id'];
	if (!is_numeric($id)) { print_error('Invalid functionality identifier.'); return; }
	$procedure_text = StripInput($_POST['procedure_text']);
	$expected_result = StripInput($_POST['expected_result']);
	if (!$procedure_text && !$expected_result) { print_error('Case data cannot be empty.'); return; }
	$query = sprintf("update tests_cases set procedure_text=\"%s\", expected_result=\"%s\", last_update_user=%s, last_update_time=\"%s\" where id=%d",
		mysql_real_escape_string($procedure_text),
		mysql_real_escape_string($expected_result),
		$last_update_user_id,
		mysql_real_escape_string($last_update_time),
		$id);
				
	$result = mysql_query($query);
	if ($result)
	{
		printf("<case last_update_user=\"%s\" last_update_time=\"%s\" />",
			htmlspecialchars($last_update_user_name),
			$last_update_time );
	}
	else
		print_error(mysql_error());
}

function case_add()
{
	global $g_login;
	$last_update_user_id = ($g_login && $g_login['id']) ? $g_login['id'] : 'null';
	$last_update_user_name = ($g_login && $g_login['display_name']) ? $g_login['display_name'] : '';
	$last_update_time = date('Y-m-d H:i:s');
	
	$chapter_id = $_POST['chapter_id'];
	if (!is_numeric($chapter_id)) { print_error('Invalid chapter identifier.'); return; }
	
	$index = $_POST['index'];
	if (!is_numeric($index)) { print_error('Invalid "index" parameter.'); return; }
	$order_number = $index + 1;
	
	$procedure_text = StripInput($_POST['procedure_text']);
	$expected_result = StripInput($_POST['expected_result']);
	if (!$procedure_text && !$expected_result) { print_error('Case data cannot be empty.'); return; }
		
	$query = sprintf("update tests_cases set order_number = order_number + 1 where chapter_id = %d and order_number >= %d",
		$chapter_id,
		$order_number);
	$result = mysql_query($query);
	if (!$result) { print_error(mysql_error()); return; }
	
	$query = sprintf("insert into tests_cases (active, chapter_id, order_number, procedure_text, expected_result, last_update_user, last_update_time)
		values (1, %d, %d, \"%s\", \"%s\", %s, \"%s\")",
		$chapter_id,
		$order_number,
		mysql_real_escape_string($procedure_text),
		mysql_real_escape_string($expected_result),
		$last_update_user_id,
		mysql_real_escape_string($last_update_time) );	
	$result = mysql_query($query);
	if (!$result) { print_error(mysql_error()); return; }
	$case_id = mysql_insert_id();
	
	printf("<case id=\"%d\" last_update_user=\"%s\" last_update_time=\"%s\" />",
		$case_id,
		htmlspecialchars($last_update_user_name),
		$last_update_time );
}

function chapter_add()
{
	$functionality_id = $_POST['functionality_id'];
	$name = StripInput($_POST['name']);
	$description = StripInput($_POST['description']);
	
	if (!is_numeric($functionality_id)) { print_error('Invalid functionality identifier.'); return; }
	if ($name == '') { print_error('Chapter name cannot be empty.'); return; }
	
	$new_order_number_query = sprintf("select if(max(order_number) is null, 0, max(order_number)+1) from tests_chapters where functionality_id=%d", $functionality_id);
	$new_order_number_result = mysql_query($new_order_number_query);
	if ($new_order_number_result === false) { print_error(mysql_error()); return; }
	$new_order_number_row = mysql_fetch_row($new_order_number_result);
	$order_number = $new_order_number_row[0];
	
	$query = sprintf("insert into tests_chapters (active, functionality_id, order_number, name, description)
		values (1, %d, %d, \"%s\", \"%s\")",
		$functionality_id,
		$order_number,
		mysql_real_escape_string($name),
		mysql_real_escape_string($description));
		
	$result = mysql_query($query);
	if (!$result) { print_error(mysql_error()); return; }
	
	printf("<id>%d</id>", mysql_insert_id());
}

function case_order()
{
	$chapter_id = $_POST['chapter_id'];
	if (!is_numeric($chapter_id)) { print_error('Invalid chapter identifier.'); return; }
	
	$case_ids = explode(',', $_POST['case_ids']);
	$case_count = count($case_ids);
	if ($case_count)
	{
		/* Here I build a smart query that assigns all cases specified in $case_ids to the $chapter_id
		 * and orders them, in a single update. The query looks like this:
		 * 
		 * update tests_cases
		 * set chapter_id=4,
		 * order_number=case id when 3 then 1 when 4 then 2 else 0 end
		 * where id in (3, 4)
		 */
		
		$query = sprintf('update tests_cases set chapter_id=%d, order_number=case id', $chapter_id);
		
		$order_number = 1;
		foreach ($case_ids as $case_id)
			$query .= sprintf(" when %d then %d", $case_id, $order_number++);
		
		$query .= " else 0 end where id in (";
		
		$case_id = $case_ids[0];
		if (!is_numeric($case_id)) { print_error('Invalid case identifier (0).'); return; }
		$query .= $case_id;
		for ($i = 1; $i < $case_count; ++$i)
		{
			$case_id = $case_ids[$i];
			if (!is_numeric($case_id)) { print_error('Invalid case identifier.'); return; }
			$query .= ',';
			$query .= $case_id;
		}
		
		$query .= ')';
		
		$result = mysql_query($query);
		if (!$result) { print_error(mysql_error()); return; }
	}
	
	print("<ok/>");
}

function chapter_order()
{
	$functionality_id = $_POST['functionality_id'];
	if (!is_numeric($functionality_id)) { print_error('Invalid functionality identifier.'); return; }
	
	$chapter_ids = explode(',', $_POST['chapter_ids']);
	$chapter_count = count($chapter_ids);
	if ($chapter_count > 1)
	{
		/* Here I build a smart query that orders all chapters inside functionality, in a single update.
		 * The query looks like this:
		 * 
		 * update tests_chapters
		 * set order_number=case id when 3 then 0 when 4 then 1 else 0 end
		 * where functionality_id=14
		 */
		
		$query = 'update tests_chapters set order_number=case id';
		
		$order_number = 1;
		foreach ($chapter_ids as $chapter_id)
		{
			if (!is_numeric($chapter_id)) { print_error('Invalid chapter identifier.'); return; }
			$query .= sprintf(" when %d then %d", $chapter_id, $order_number++);
		}
		
		$query .= sprintf(" else 0 end where functionality_id=%d", $functionality_id);
		
		$result = mysql_query($query);
		if (!$result) { print_error(mysql_error()); return; }
	}
	
	print("<ok/>");
}


////////////////////////////////////////////////////////////////////////////////
// Code

if (current_user_can_edit_functionalities())
{
	switch ($_POST['operation'])
	{
		case 'functionality_delete':
			functionality_delete();
			break;
		case 'functionality_fetch':
			functionality_fetch();
			break;
		case 'functionality_set_name':
			functionality_set_name();
			break;
		case 'functionality_set_description':
			functionality_set_description();
			break;
		case 'functionality_set_length':
			functionality_set_length();
			break;
		case 'case_delete':
			case_delete();
			break;
		case 'case_edit':
			case_edit();
			break;
		case 'case_add':
			case_add();
			break;
		case 'case_order':
			case_order();
			break;
		case 'chapter_set_name':
			chapter_set_name();
			break;
		case 'chapter_set_description':
			chapter_set_description();
			break;
		case 'chapter_add':
			chapter_add();
			break;
		case 'chapter_delete':
			chapter_delete();
			break;
		case 'chapter_order':
			chapter_order();
			break;
		default:
			print_error('Invalid operation.');
	}
}
else
	print_error('Access denied.');

?>