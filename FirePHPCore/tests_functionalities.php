<?php
// Kodowanie UTF-8, test: Zażółć gęślą jaźń
// Lista funkcjonalności

require_once('tests_main.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

print_page_begin('Functionalities');

echo '<form action="tests_functionality_add.php" method="POST">
	<input type="hidden" name="new" value="1">
	<input type="submit" value="New functionality">
</form>';

if (!current_user_can_edit_functionalities())
	print_error_paragraph("Access denied.");
else
{

	$query_result = mysql_query('select tests_functionalities.id id, tests_functionalities.name name, t1.case_count case_count
		from tests_functionalities
		left join (
		    select tests_chapters.functionality_id functionality_id, count(*) case_count
		    from tests_chapters
		    join tests_cases on tests_cases.chapter_id=tests_chapters.id
		    where tests_cases.active=1 and tests_chapters.active=1
		    group by tests_chapters.functionality_id
		) t1
		on tests_functionalities.id=t1.functionality_id
		order by tests_functionalities.name');
	if (mysql_num_rows($query_result) == 0)
		print("<p class=\"info\">No functionalities.</p>\n");
	else
	{
		print("<table id=\"functionalities\">\n");
		print("<tr><th>Functionality<th class=\"r\" style=\"width:64px\">Cases<th style=\"width:24px\">&nbsp;\n");
		while ($row = mysql_fetch_assoc($query_result))
		{
			$id = $row['id'];
	
			$case_count = $row['case_count'];
			if ($case_count === null)
				$case_count = 0;
	
			$edit_href = 'tests_functionality.php?id=' . $id;
	
			printf("<tr id=\"functionality_%s\"><td><a href=\"%s\">%s</a>
				<td class=\"r\"><a href=\"%s\">%s</a>
				<td class=\"delete\"><a href=\"#\" title=\"Delete\"><img src=\"images/delete.png\" alt=\"Delete\"></a>\n",
				$id,
				$edit_href,
				htmlspecialchars($row['name']),
				$edit_href,
				$case_count );
		}
		print("</table>\n");
	}
?>


<script type="text/javascript">

function delete_functionality_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		var id = this.id;
		$('#functionalities #functionality_' + id).remove();
	}
}

function delete_functionality_click()
{
	if (confirm('Do you really want to delete this functionality?'))
	{
		var deleted_functionality_id = $(this).parents('tr').attr('id').substr(14);
		var query_data = { operation:'functionality_delete', id:deleted_functionality_id };
		$.ajax( {
			type: 'POST',
			url: 'tests_functionality_edit.php',
			data: query_data,
			success: delete_functionality_response,
			dataType: 'xml',
			context: query_data } );
	}
	return false;
}

function init()
{
	$(document).ajaxError(function(event, request, settings, error) {
		alert('AJAX error: ' + error);
	} );

	$('#functionalities .delete a').click(delete_functionality_click);
}

$(init);

</script>

<?php
} // if (!current_user_can_edit_functionalities())

	print_page_end();
?>