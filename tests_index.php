<?php
// Encoding UTF-8, test: Zażółć gęślą jaźń
// Main page of Tests.

require_once('tests_main.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");


print_page_begin('Tests', '
<style type="text/css">
div#user_gallery img { float:left; margin-right:8px; height:128px; }
</style>');

if ($g_login != null)
{
	echo '<ul>
	<li><a href="tests_tests.php">Tests</a></li>';
	if (current_user_can_edit_functionalities())
		print("<li><a href=\"tests_functionalities.php\">Functionalities</a></li>\n");
	echo '<li><a href="tests_planned_soft.php">Planned Software</a></li>
	<li><a href="tests_users.php">Users</a></li>
	</ul>';
}

/*
// User gallery
$query = sprintf("select mantis_user_table.username username, mantis_user_table.realname realname, tests_users.avatar_url avatar_url
	from mantis_user_table join tests_users
	on mantis_user_table.id=tests_users.id_user
	where avatar_url is not null
	order by realname, username");
$result = mysql_query($query);
if (mysql_num_rows($result))
{
	print("<h1>User gallery :)</h1>\n");
	print("<div id=\"user_gallery\">\n");
	while ($row = mysql_fetch_assoc($result))
	{
		$name = $row['realname'];
		if (!$name)
			$name = $row['username'];
			
		printf("<img src=\"%s\" alt=\"%s\" title=\"%s\">\n",
			htmlspecialchars($row['avatar_url']),
			htmlspecialchars($name),
			htmlspecialchars($name) );
	}
	print("<div style=\"clear:both\"></div>\n");
	print("</div>\n");
}
*/
print_page_end();
?>