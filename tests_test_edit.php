<?php
// Dodawanie i edycja testu
// Zażółć gęślą jaźń

require_once('tests_main.php');

connect_to_database();
if (!login())
	die("Access Denied");

	
if (current_user_access_level() != ADMIN)
	die("Access Denied");

if ($_POST['action'] == "add")
{		
	$projectID = $_POST['project'];
	$plannedSoftID = $_POST['planned_soft'];
	$testName = StripInput($_POST['TestName']);
	$testDescription = StripInput($_POST['TestDescription']);

	$chbSoftArrive = $_POST['chbSoftArriveDate'];
	$soft_arrive_date = $_POST['test_soft_arrive_date'];
	$arr = explode("/", $soft_arrive_date);
	$arriveDate = $arr[2]."-".$arr[1]."-".$arr[0];		
	
	if ($projectID == "")
	{
		send_html_header();
		
		print_error_paragraph("Select project first!");			
		echo '<br/>';	
		print_back_link('tests_test.php');
		echo '<br/>';	
		print_page_end();
		die;		
	}
	
	$fields = 'INSERT INTO tests_tests (id_project, name, description ';
	$values .= 'VALUES('.$projectID.', "'.mysql_real_escape_string($testName).'","'.mysql_real_escape_string($testDescription).'" ';
	
	if ($plannedSoftID != "")
	{
		$fields .= ', id_planned_soft';
		$values .= ', '.$plannedSoftID;
	}
	if ($chbSoftArrive == "on")
	{
		$fields .= ', software_arrive_date';
		$values .= ', "'.$arriveDate.'"';
	}
	$fields .= ') ';
	$values .= ');';
	
	$query = $fields.$values;
	
	$r = mysql_query($query);
	if ($r === false)
	{
		send_html_header();
		print_error_paragraph(mysql_error());			
		echo '<br/>';	
		print_back_link('tests_tests.php');
		echo '<br/>';	
		print_page_end();		
	}
	else
	{
		$t_redirect = "tests_tests.php";
		redirect($t_redirect, 0);		
	}
}
