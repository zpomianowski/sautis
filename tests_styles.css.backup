body {
	margin:0;
	padding:0;
}
/* Main font */
body, p, div, th, td, button, input, textarea {
	font-size: 10pt;
	font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
	color: color: #333;
}
td, th {
	text-align: left;
	padding: 2px;
}
h1, h2, h3, h4, h5, h6, .header_font {
	font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
	color: #333;
	font-weight: bold;
}
h1, h2 {
	font-size:16px;
}

.headText
{
	margin-left: 20px;
	margin-bottom: 20px;
	font-weight: bold;
	font-size:16px;
}
	
td.r, th.r {
	text-align:right;
}
a {
	color: #3B5998;
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}

p.info {
	background-color: #ffffe1;
	border: 1px solid black;
	padding: 4px;
}
p.error {
	background-color: #f00;
	border: 1px solid black;
	padding: 4px;
}
a img {
	border-width:0; /* For Internet Explorer */
}

.icon_expand, .icon_collapse {
	display: inline-block;
	width: 16px;
	height: 16px;
	background-image: url(images/expand_collapse.png);
}
.icon_expand {
	background-position: 0px 0px;
}
.icon_collapse {
	background-position: 0px -16px;
}

/* =============================================================================
Header */

div#header > ul.menu
{
	list-style-type: none;
	margin: 0;
	padding: 0;
	float: right;
	background-color: #627aad;
	height:36px;
}
div#header > ul.menu > li
{
	float: left;
	font-weight: bold;
}
div#header > ul.menu > li:hover
{
	background-color: #6c85b6;
}
div#header a {
	color: white;
}
div#header > ul.menu > li a
{
	display: block;
	padding: 8px 10px 8px 10px;
}
div#header > ul.menu > li a:hover
{
	text-decoration:none;
}
div#header span.login_info {
	float:right;
	font-weight: normal;
	margin-left: 16px;
	margin-right: 16px;
	margin-top: 4px;
	font-size: 11px;
}
div#header span.login_info a {
	font-weight: bold;
}

/* =============================================================================
Specific to tests_functionalities.php */

#functionalities {
	width:100%;
	border-bottom: 1px solid #d9d9d9;
}
#functionalities a {
	display:block;
}
#functionalities tr td.delete a img,
div#functionality_description div.header a img {
	visibility:hidden;
}
#functionalities tr:hover td.delete a img,
div#functionality_description:hover div.header a img {
	visibility:visible;
}
#functionalities tr:hover td {
	background-color:#edeff4;
}

/* =============================================================================
Specific to tests_functionality.php */

div#functionality_name a.edit {
	visibility:hidden;
}
div#functionality_name:hover a.edit {
	visibility:visible;
}

/* Margin under description */
div#functionality_description,
div#functionality_chapters > div > div.description
{
	margin-bottom: 12px;
}
div#functionality_description div.header {
	font-weight:bold;
}
div#functionality_description div.text {
	white-space:pre-wrap;
}

/* Name input */
div#functionality_name input[type="text"],
div#functionality_chapters > div > h2 > span.name > form > input[type="text"]
{
	width:50%;
	font-size:16px;
}
/* Description textarea */
div#functionality_description textarea,
div#functionality_chapters > div > div.description > div.text textarea
{
	width: 50%;
	height: 8em;
}
/* Case textareas */
div#functionality_chapters > div > div.cases > table td.procedure_text > textarea,
div#functionality_chapters > div > div.cases > table td.expected_result > textarea
{
	width: 99%;
	height: 8em;
}

/* Indentation for case list inside chapter. */
div#functionality_chapters div.cases,
div#functionality_chapters div.add_case {
	padding-left: 16px;
}
div#functionality_chapters div.cases table {
	width:100%;
}
div#functionality_chapters div.cases table tr:hover td {
	background-color:#edeff4;
}
div#functionality_chapters div.cases table tr td.functions {
	width: 66px;
}
div#functionality_chapters div.cases table tr td.functions a.add,
div#functionality_chapters div.cases table tr td.functions a.edit,
div#functionality_chapters div.cases table tr td.functions a.delete,
div#functionality_chapters div.cases tr td.drag_handle img
{
	visibility:hidden;
}
div#functionality_chapters div.cases table tr:hover td.functions a.add,
div#functionality_chapters div.cases table tr:hover td.functions a.edit,
div#functionality_chapters div.cases table tr:hover td.functions a.delete,
div#functionality_chapters div.cases tr:hover td.drag_handle img
{
	visibility:visible;
}
div#functionality_chapters div.cases table tr td.functions a.add,
div#functionality_chapters div.cases table tr td.functions a.edit,
div#functionality_chapters div.cases table tr td.functions a.delete,
div#functionality_chapters div.cases table tr td.functions a.accept,
div#functionality_chapters div.cases table tr td.functions a.cancel
{
	margin-left:4px;
}
div#functionality_chapters div.cases table td.drag_handle {
	text-align:center;
}
div#functionality_chapters div.cases table td,
div#functionality_chapters div.cases table th {
	vertical-align:top;
}
div#functionality_chapters div.cases table.inactive td {
	color:#006ffe;
}
div#functionality_chapters div.cases > table {
	border-bottom: 1px solid silver;
}
div#functionality_chapters div.cases td.procedure_text,
div#functionality_chapters div.cases td.expected_result {
	white-space:pre-wrap;
}
div#functionality_chapters div.cases td.procedure_text span.last_update {
	display: block;
	color: #aaa;
}
div#functionality_chapters div.cases td.drag_handle img {
	cursor: s-resize;
}
form#functionality_length {
	display:block;
	width: 210px;
	float:right;
}
form#functionality_length input#functionality_length_input {
	width: 32px;
	text-align:right;
}
form#functionality_length label {
	font-weight:bold;
}

div#functionality_chapters h2 a.edit,
div#functionality_chapters h2 a.delete {
	visibility:hidden;
	margin-left:4px;
}
div#functionality_chapters h2:hover a.edit,
div#functionality_chapters h2:hover a.delete {
	visibility:visible;
}
div#functionality_chapters h2 span.name form {
	display:inline;
}
div#functionality_chapters div.description div.header {
	font-weight: bold;
}
div#functionality_chapters div.description div.header a img {
	visibility:hidden;
}
div#functionality_chapters div.description:hover div.header a img {
	visibility:visible;
}
div#functionality_chapters div.description div.text {
	white-space:pre-wrap;
}
div#add_chapter

{
	margin-top: 12px;
}
div#add_chapter button {
	font-weight:bold;
}
/* Gray out inactive chapter name and description. */
div#functionality_chapters > div.inactive > h2 > span.lp,
div#functionality_chapters > div.inactive > h2 > span.name,
div#functionality_chapters > div.inactive > div.description > div.header,
div#functionality_chapters > div.inactive > div.description > div.text {
	color:gray;
}
/* Chapter drag handle */
div#functionality_chapters > div > h2 > img.chapter_drag_handle {
	visibility:hidden;
	cursor: s-resize;
	margin-left:4px;
}
div#functionality_chapters > div > h2:hover > img.chapter_drag_handle {
	visibility:visible;
}

/* =============================================================================
Dopisane przez Kube */

#defColors
{
	width: 100%;
	border: 1px solid black;
}

tr.Waiting, td.Waiting
{ BACKGROUND-COLOR: #FFCD85; }

tr.InProgress, td.InProgress
{ BACKGROUND-COLOR: #d2f5b0; }

tr.Closed, td.Closed
{ BACKGROUND-COLOR: #cccccc; }

tr.BadThing, td.BadThing
{ BACKGROUND-COLOR: #FFA0A0; }

tr.NotAvailable, td.NotAvailable
{ BACKGROUND-COLOR: #9966FF; COLOR: #ffffff;}

tr.NotTestable, td.NotTestable
{ BACKGROUND-COLOR: #aaaaaa; COLOR: #000000;}

tr.Unknown, td.Unknown
{ BACKGROUND-COLOR: #ffffff; }

tr.Reject, td.Reject
{ BACKGROUND-COLOR: #c9ccc4; }

tr.Undone, td.Undone
{ BACKGROUND-COLOR: #FFFFFF; COLOR: #000000; }

tr.Correct, td.Correct
{ BACKGROUND-COLOR: #d2f5b0; }

.ProjectFilter
{}

td.category
{}

td.value
{}

div.BackLink
{}
tr.BlackRow
{background-color: #000000; color: #ffffff;}

.TableData
{
	width: 100%;
}
	
.TableData tr
{
	height: 30px;
}

Table.Select tr:hover td
{
	background-color: #F3FF35;
}

.TableData a, a:hover, a:visited, a:active
{
	text-decoration: none;
	color: #000000;
}
	
.TableData tr.Header
{
	background-color: #3B5998;
	color: #ffffff;
	width: 100%;
	border: 1px solid black;
	height: 50px;
}

.TableData td.category
{
	background-color: #3B5998;
	color: #ffffff;
	width: 25%;
	text-align: right;
}

.TableData td.value
{
	background-color: #DDDDDD;
	color: #000000;
	width: 75%;	
}

div .ActionButton
{
	text-align: center;
}


div .Content
{
	height: 100%;
	margin-bottom: 80px;
}
	
div .Palette
{
	width: 99%;
	position: fixed;
	bottom: 0px;
	margin-bottom: 0px;
}

a.MarkedLink
{ color: #0000FF; }

a.MarkedLink:hover
{ color: #000000; }

.RedText
{ color: #ff0000; }

.GreenText
{ color: #00ff00; }

.TableData .FuncRow
{
	background-color: #3B5998;
	color: white;
	border: 0px;
	height:60px;
	vertical-align:top;
}

.TableData .FuncRow td
{
 	padding: 10px;
}

.TableData .ChapterRow
{
	background-color: #6C85B6;
	color: black;
	height:40px;	
	vertical-align:top;
}

.TableData .ChapterRow td
{
 	padding: 10px;
}

.UpdateTimeSpan
{
	font-size:12px;
	font-weight: bold;	
}

.CursorHand
{
	cursor: pointer;
	height: 100%;
}

.HowMuchDone
{
	font-weight: bold;
	color: #ffff00;
}

.TesterRow
{
	height: 100%;
	padding: 5px;
	vertical-align: top;
}

.TestRow
{
	height: 100%;
	padding: 5px;
	vertical-align: top;
}

.DateCell
{
	text-align: center;
}

div .CasesTree
{
	background-color: #dddddd;
	width: 49%;
}

div #TestersList
{
	width: 49%;
	height: 200px;
	
	position: absolute;

	top: 825px;
	bottom: 100px;
	left: 50%;
}

span.UserInUsersList
{
	background-image: url(images/user.png);
	background-repeat: no-repeat;
	padding-left: 20px;
}

/*

 Drag and drop

*/

/* Dragged element. */
.UserDrag
{
	margin-left: 10px;
}

/* Determinate place from dragged  */
span.UserInFunctionality
{
	font-weight: bold;
	background-image: url(images/user.png);
	background-repeat: no-repeat;
	padding-left: 20px;
	color: #00FF40;
	margin-left: 10px;
}

/* Determinate place from dragged  */
span.UserInChapter
{
	font-weight: bold;
	background-image: url(images/user.png);
	background-repeat: no-repeat;
	padding-left: 15px;
	color: #2058FF;
	margin-left: 10px;
}

table.UserDragEntity
{
	height: 20px;
	width: 200px;
	background-color: #cccccc;
	border: 1px solid black;
	position: absolute;
}

table.UserDragEntity td:first-child
{
	width: 20px;	
	background-image: url(images/user.png);
	background-repeat: no-repeat;
	background-position: 3px 3px;
}

#TrashIcon:hover
{
	background-color: red;
}

td.BlockedTR
{
	background-color: #aaaaaa;
}

td.LoadTR
{
	background-color: #FF803E;
}

td.FullLoadTR
{
	background-color: #00ff00;
}

.CategoryName
{
	font-weight: bold;
	Color: #ffff00;
}
