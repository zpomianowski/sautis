<?php
// Zarządzanie użytkownikami

require_once('tests_main.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

print_page_begin('Users');

echo '<div class="Content">';
echo '<h3>Users List</h3>
<table class="TableData">
<!-- Header -->
<tr class="Header">
	<th width="300">User</th>
	<th width="150">Permissions</th>
	<th width="50">Weight</th>';
	
if (current_user_access_level() == ADMIN)
	echo '<th width="150">Action</th>';
	
echo '</tr>';

$result = get_users_list();

if ($result == null)
	echo 'Error in mysql test querry!!';
else
{
	while ($row = mysql_fetch_assoc($result))
	{
		echo '<tr class="Normal">';		
		echo '<td>'.get_user_name($row['id_user']).'</td>';


		$acc = get_user_access_level($row['id_user']);
		
		if ($acc == ADMIN)
		{
			echo '<td class="Waiting">';			
			$priv = 'Admin';		
		}
		else if ($acc == TESTER)
		{
			echo '<td class="InProgress">';			
			$priv = 'Tester';		
		}
		else if ($acc == OBSERVER)
		{
			echo '<td class="Reject">';			
			$priv = 'Observer';		
		}
		else
		{
			echo '<td class="Unknown">';			
			$priv = '';		
		}

		echo $priv.'</td>';		
		echo '<td>';
		echo $row['weight'];
		echo '</td>';
		
		if (current_user_access_level() == ADMIN)
		{		
			echo '<td>';
			echo '<form style="float: left;" method="post" action="tests_users_edit.php">';		
			echo '<input name="user" type="hidden" value="'.$row['id_user'].'" />';
			echo '<input name="action" type="hidden" value="editUser" />';
			echo '<input type="submit" class="button" value="Edit" />';
			echo '</form>';				
			echo '<form method="post" action="tests_users_edit.php">';		
			echo '<input name="user" type="hidden" value="'.$row['id_user'].'" />';
			echo '<input name="action" type="hidden" value="delete"/>';
			$q = 'Are you sure you want to delete this user: '.get_user_name($row['id_user']).'?'; 
			echo '<input type="submit" class="button" onclick="return confirm(\''.$q.'\')"  value="Delete" />';
			echo '</form>';				
			echo '</td>';
		}					
		echo '</tr>';
	}	
}
echo '</table>';

if (current_user_access_level() == ADMIN)
{
	echo '<br/>';
	echo '<div class="ActionButton">';
	echo '<form method="post" action="tests_users_edit.php">';		
	echo '<input name="action" type="hidden" value="newUser" />';
	echo '<input type="submit" class="button" value="Add User" />';
	echo '</form>';
	echo '</div>';
}
?>
</div>
<div class="Palette">
<table id="defColors" cellspacing="1">
<tr>
<td class="Waiting" width="25%">Admin</td>
<td class="InProgress" width="25%">Tester</td>
<td class="Reject" width="25%">Observer</td>
<td class="Unknown" width="25%">Unknown</td>
</tr></table>
</div>
<?php print_page_end(); ?>