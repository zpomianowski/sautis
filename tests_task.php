<?php
// Lista casów do wykonania przez testera
// Zażółć gęślą jaźń
require_once('tests_main.php');
require_once('tests_funcs.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

$case = $_GET['redirectToCase'];
if ($case !== null)
{
	print_page_begin('');
	echo '<div class="Content">';
	
	$row = get_test_case_data($case);
	if ($row['id_test'] == null)
	{
		print_error_paragraph("There is no case with id=".$case);
	}
	else 
	{
		$str = $g_server_host.'tests_task.php?testID='.$row['id_test'].'&taskID='.$row['id_tests_task'].'#Case_'.$case;
		redirect($str, 0);
	}
	print_page_end();
	return;
}

$testID = $_GET['testID'];
$taskID = $_GET['taskID'];
$testerTaskData = get_task_data($taskID);
$stat = get_cases_statistics($taskID);

$testData = get_test_data($testID);
$startedTest = ($testData['start_date'] != '') ? true : false;
$stoppedTest = ($testData['stop_date'] != '') ? true : false;

$casesDonePerc = ($stat['closed'] / $stat['all']) * 100;
$fs = sprintf("<span id=\"h1Title\" style=\"color: red;\"> [Done <span id=\"GeneralDoneValue\">%d</span> of %d (<span id=\"GeneralPercValue\">%.2f%%</span>)]</span>", $stat['closed'], $stat['all'], $casesDonePerc);
print_page_begin('Tasks for '.get_user_name($testerTaskData['id_user']), 
				'Tasks for '.get_user_name($testerTaskData['id_user']).$fs);


if ($testerTaskData['id_user'] == $g_login['id']) $taskOwner = 1;
else $taskOwner = 0;
	
echo '<div class="Content">';
echo '<h4><a class="MarkedLink" href="tests_test_details.php?testID='.$testID.'">[ '
	.get_project_name($testData['id_project']).' ## '.htmlspecialchars($testData['name']).' ]</a></h4>';
	
	
function task_fetch()
{
	global $taskID;
	
	$query = 'SELECT tests_functionalities.* FROM tests_test_cases 
			JOIN tests_cases ON tests_test_cases.id_tests_cases = tests_cases.id 
			JOIN tests_chapters ON tests_chapters.id = tests_cases.chapter_id 
			JOIN tests_functionalities ON tests_functionalities.id = tests_chapters.functionality_id 
			WHERE id_tests_task='.$taskID.' GROUP BY tests_functionalities.id ORDER BY tests_functionalities.name;';
	$func_result = mysql_query($query);	

	if ($func_result === false) { print_error_paragraph(mysql_error()); return; }
	
	$query = 'SELECT tests_chapters.* FROM tests_test_cases 
			JOIN tests_cases ON tests_test_cases.id_tests_cases = tests_cases.id 
			JOIN tests_chapters ON tests_chapters.id = tests_cases.chapter_id 
			WHERE id_tests_task='.$taskID.' GROUP BY tests_chapters.id 
			ORDER BY tests_chapters.functionality_id, tests_chapters.order_number, tests_chapters.id;';	
	$chapters_result = mysql_query($query);
	
	if ($chapters_result === false) { print_error_paragraph(mysql_error()); return; }
	
	$query = 'SELECT tests_cases.*, tests_chapters.functionality_id, 
			tests_test_cases.result,tests_test_cases.notes, 
			tests_test_cases.id_tests_test_cases id_case, tests_test_cases.update_time 
			FROM tests_test_cases 
			JOIN tests_cases ON tests_test_cases.id_tests_cases = tests_cases.id 
			JOIN tests_chapters ON tests_chapters.id = tests_cases.chapter_id 
			WHERE id_tests_task='.$taskID.' GROUP BY tests_cases.id 
			ORDER BY tests_chapters.functionality_id, tests_chapters.order_number, 
			tests_chapters.id, tests_cases.order_number, tests_cases.id;';
	$cases_result = mysql_query($query);
	if ($cases_result === false) { print_error_paragraph(mysql_error()); return; }

	$funcs = array();

	while ($func_row = mysql_fetch_assoc($func_result))
	{
		$funcs[$func_row['id']] = array(
			'id' => $func_row['id'],
			'name' => $func_row['name'],
			'description' => $func_row['description'],
			'length' => $func_row['length'],
			'chapters' => array()
		);						
	}

	while($chapter_row = mysql_fetch_assoc($chapters_result))
	{
		$funcs[$chapter_row['functionality_id']]['chapters'][$chapter_row['id']] = array(
			'id' => $chapter_row['id'],
			'name' => $chapter_row['name'],
			'active' => $chapter_row['active'],
			'description' => $chapter_row['description'],
			'order_number' => $chapter_row['order_number'],
			'cases' => array()
		);			
	}

	while($case_row = mysql_fetch_assoc($cases_result))
	{
		$funcs[$case_row['functionality_id']]['chapters'][$case_row['chapter_id']]['cases'][$case_row['id']] = array(
			'id' => $case_row['id'],
			'id_case' => $case_row['id_case'],
			'order_number' => $case_row['order_number'],
			'active' => $case_row['active'],
			'procedure_text' => $case_row['procedure_text'],
			'expected_result' => $case_row['expected_result'],
			'last_update_time' => $case_row['last_update_time'],
			'last_update_user' => $case_row['last_update_user'],
			'result' => $case_row['result'],
			'notes' => $case_row['notes'],
			'update_time' => $case_row['update_time'],
		);
	}
	
	return $funcs;
}

?>

<script type="text/javascript">
var task_id=<?php echo $taskID; ?>;
var tester_id=<?php echo $testerTaskData['id_user']; ?>;

function ExpandCollapse(myTD)
{
	var td = $(myTD);
	var icon = td.parents('table').find('a.Icon');
	var headerDiv = td.parents('div.')[0];
	
	icon.toggleClass('icon_collapse');
	icon.toggleClass('icon_expand');

	var divContent = $(headerDiv).next();
	divContent.toggle('fast');
	
	return false;	
}

function ExpandCollapseDivsDown(myA)
{
	var a = $(myA);
	var div = a.parents('#FunctionalitiesDiv');
	
	var func = $('#FunctionalitiesDiv').find('.FunctionalityContentDiv');
	var chap = $('#FunctionalitiesDiv').find('.ChapterContentDiv');
	var icon = $('.FunctionalityDiv').find('a.Icon');
	var iconFunc = $('.FunctionalityHeaderDiv').find('a.Icon');
		
	if (a.hasClass('icon_collapse'))
	{
		$(func).hide('fast');
		$(chap).hide('fast');
		$(icon).removeClass('icon_collapse');
		$(icon).addClass('icon_expand');
	}
	else
	{
		$(func).show('fast');
		$(chap).show('fast');
		$(icon).removeClass('icon_expand');
		$(icon).addClass('icon_collapse');
	}

	a.toggleClass('icon_collapse');
	a.toggleClass('icon_expand');

	return false;	
}

function SaveSummaryResponse(result)
{
	var error = $('error', result);
	var res = $('#save_result');

	if (error.size())
	{
		res.text(error.text());
		res.removeClass('GreenText');		
		res.addClass('RedText');
	}		
	else
	{
		res.text('saved');
		res.removeClass('RedText');		
		res.addClass('GreenText');		
	}
}

function UpdateCaseResponse(result)
{
<?php
echo 'var CF_NOT_TESTED ='.CF_NOT_TESTED.';';
echo 'var CF_NOT_TESTABLE ='.CF_NOT_TESTABLE.';';
echo 'var CF_ERROR ='.CF_ERROR.';';
echo 'var CF_NOT_AVAILABLE ='.CF_NOT_AVAILABLE.';';
echo 'var CF_CORRECT ='.CF_CORRECT.';';
?>
	var error = $('error', result);

	if (error.size())
	{
		alert(error.text());		
	}		
	else
	{
		var id_case = $('root>id_case', result).text();
		var result_value = $('root>result_value', result).text();
		var update_time = $('root>update_time', result).text();
		var caseRow = $('#CaseRow'+id_case);
		var updateTimeElement = $('#update_time'+id_case);

		// update czasu aktualizacji
		updateTimeElement.text('('+update_time+')');

		// Update statusu case'a
		if (result_value == CF_NOT_TESTED)
		{
			caseRow.removeClass('Undone BadThing Correct NotAvailable NotTestable');
			caseRow.addClass('Undone');		
		}
		else if (result_value == CF_NOT_TESTABLE)
		{
			caseRow.removeClass('Undone BadThing Correct NotAvailable NotTestable');
			caseRow.addClass('NotTestable');		
		}
		else if (result_value == CF_ERROR)
		{
			caseRow.removeClass('Undone BadThing Correct NotAvailable NotTestable');
			caseRow.addClass('BadThing');		
		}
		else if (result_value == CF_NOT_AVAILABLE)
		{
			caseRow.removeClass('Undone BadThing Correct NotAvailable NotTestable');
			caseRow.addClass('NotAvailable');		
		}
		else if (result_value == CF_CORRECT)
		{
			caseRow.removeClass('Undone BadThing Correct NotAvailable NotTestable');
			caseRow.addClass('Correct');		
		}

		// Update wartosci liczy wykonanych zadan
		// Dla chapter
		var chapDiv = $(this).parents('div.ChapterDiv');
		var cUndone = $(chapDiv).find('.Undone').length;
		var cCorrect = $(chapDiv).find('.Correct').length;
		var cBad = $(chapDiv).find('.BadThing').length;
		var cNTestable = $(chapDiv).find('.NotTestable').length;
		var cNAvailable = $(chapDiv).find('.NotAvailable').length;
		var done = cCorrect + cBad + cNTestable + cNAvailable;
		
		var headerDiv = $(chapDiv).find('.ChapterHeaderDiv');
		$(headerDiv).find('.ChapDoneValue').text(done);

		var donePerc = done / (done+cUndone) * 100;
		$(headerDiv).find('.ChapPercValue').text(donePerc.toFixed(2)+'%');
		

		// Dla functionality
		var funcDiv = $(this).parents('div.FunctionalityDiv');
		cUndone = $(funcDiv).find('.Undone').length;
		cCorrect = $(funcDiv).find('.Correct').length;
		cBad = $(funcDiv).find('.BadThing').length;
		cNTestable = $(funcDiv).find('.NotTestable').length;
		cNAvailable = $(funcDiv).find('.NotAvailable').length;
		done = cCorrect + cBad + cNTestable + cNAvailable;
		
		headerDiv = $(funcDiv).find('.FunctionalityHeaderDiv');
		$(headerDiv).find('.FuncDoneValue').text(done);
		
		donePerc = done / (done+cUndone) * 100;
		$(headerDiv).find('.FuncPercValue').text(donePerc.toFixed(2)+'%');

		// Dla calego taska
		var funcsDiv = $(this).parents('div#FunctionalitiesDiv');
		cUndone = $(funcsDiv).find('.Undone').length;
		cCorrect = $(funcsDiv).find('.Correct').length;
		cBad = $(funcsDiv).find('.BadThing').length;
		cNTestable = $(funcsDiv).find('.NotTestable').length;
		cNAvailable = $(funcsDiv).find('.NotAvailable').length;
		done = cCorrect + cBad + cNTestable + cNAvailable;
		
		$('#GeneralDoneValue').text(done);
		donePerc = done / (done+cUndone) * 100;
		$('#GeneralPercValue').text(donePerc.toFixed(2)+'%');

		if (donePerc.toFixed(2) == 100)
		{
			$('#CloseTaskButtonDiv').css('display', 'inline');
		}		
		else if (donePerc.toFixed(2) != 100 && $('#CloseTaskButtonDiv').css('display') == 'inline')
			$('#CloseTaskButtonDiv').toggle();
			
	}
}

function UpdateCaseClick()
{
	var form = $(this).parents('form');
	
	var case_id = form.find('.CaseID').val();
	var result = form.find('.Result').val();
	var notes = form.find('.Notes').val();

	var params = {operation: 'update_case', tester_id: tester_id, case_id: case_id, result: result, notes: notes};
	$.ajax({type:'POST', url:'tests_task_edit_js.php', context:this, data: params, success: UpdateCaseResponse, dataType:'xml'});

	return false;	
}

function SaveSummaryClick()
{
	var summary = $('#TaskSummary').val();
	var params = {operation: 'save_summary', task_id: task_id, tester_id: tester_id, summary: summary};
	$.ajax({type:'POST', url:'tests_task_edit_js.php', data: params, success: SaveSummaryResponse, dataType:'xml'});

	return false;	
}

function FocusTaskSummary()
{
	$('#save_result').text('');
}

function CloseTaskConfirm()
{
	if (confirm("Are you sure you want to close task?") == 0)
		return false;
	return true;
}

function init()
{
	$('.UpdateCase').click(UpdateCaseClick);		
	$('#SaveSummary').click(SaveSummaryClick);		
	$('#TaskSummary').focusin(FocusTaskSummary);			

	if (!window.location.hash)
	{
		var a = $('#ExpandAllA');
		ExpandCollapseDivsDown(a);
	}
}

$(init);
</script>

<form method="post" action="tests_task_edit.php">
<input type="hidden" name="taskID" value="<?php echo $taskID; ?>"/>
<input type="hidden" name="testID" value="<?php echo $testID; ?>"/>

<div class="TaskDiv"><a class="Icon" onclick="return ExpandCollapse(this);" href="#" title="Expand" class="icon_collapse"></a><h3 style="display:inline;padding-left: 5px;"  id="TaskDetailsHeader">Details</h3></div>
<div id="TaskDetailsDiv">
<table class="TableData">
<tr>
	<td class="category">
	<label>Summary</label>
	</td>
	<?php
	if ($testerTaskData['closed'] == 1) 
		echo '<td class="value" style="white-space: pre-wrap;">'.string_process_case_link(make_bug_view_links(htmlspecialchars($testerTaskData['summary']))).'</td>';
	else
	{ 
		echo '<td class="value">
		<textarea id="TaskSummary" rows="0" cols="0" style="width: 99%; height: 7em;">'.htmlspecialchars($testerTaskData['summary']).'</textarea>
		</td>';
	}
	?>
</tr>	
<tr>
	<td class="category"><span id="save_result"></span></td>
	<td class="value">
	<?php
	if ($startedTest)
	{
		if (($g_login['access_level'] == ADMIN || $taskOwner) && $testerTaskData['closed'] == 0)
			echo '<input id="SaveSummary" type="submit" value="Save"/>';
			
		if ($g_login['access_level'] == ADMIN && $testerTaskData['closed'] == 1 && !$stoppedTest)
			echo '<input id="OpenTask" name="Action" type="submit" value="Open"/>';
			
		if (($g_login['access_level'] == ADMIN || $taskOwner) && $testerTaskData['closed'] == 0 && $stat['open'] == 0)
		{
			echo '<div id="CloseTaskButtonDiv" style="display:inline;">';
			echo '<input id="CloseTask" name="Action" type="submit" onclick="return CloseTaskConfirm();" value="Close"/>';
			echo '</div>';
		}
		else
		{
			echo '<div id="CloseTaskButtonDiv" style="display:none;">';
			echo '<input id="CloseTask" name ="Action" type="submit"  onclick="return CloseTaskConfirm();" value="Close"/>';
			echo '</div>';
		}
	}
	?>
	</td>
</tr>
<tr>
	<td class="category">
	<label>Description</label>
	</td>
	<td class="value" style="white-space: pre-wrap;"><?php echo make_bug_view_links(htmlspecialchars($testerTaskData['description'])); ?></td>
</tr>	
<tr>
	<td class="category">Start Date</td>
	<td class="value"><?php echo $testerTaskData['start_date']; ?></td>
</tr>
<tr>
	<td class="category">End Date</td>
	<td class="value"><?php echo $testerTaskData['stop_date']; ?></td>
</tr>
</table></div>
</form>

<?php
$funcs = task_fetch();

$filter = ($_GET['filter'] !== null) ? $_GET['filter'] : CF_ALL;

if ($filter == CF_CORRECT) $selC = 'selected="selected"';
if ($filter == CF_ERROR) $selE = 'selected="selected"';
if ($filter == CF_NOT_AVAILABLE) $selNA = 'selected="selected"';
if ($filter == CF_NOT_TESTABLE) $selNT = 'selected="selected"';
if ($filter == CF_NOT_TESTED) $selNotT = 'selected="selected"';
if ($filter == CF_ALL) $selA = 'selected="selected"';

echo '<br><br><div id="FunctionalitiesDiv">
		<a id="ExpandAllA" onclick="return ExpandCollapseDivsDown(this);" href="#" title="Expand" class="Icon icon_collapse"></a>
		<h3 style="display:inline;padding-left: 5px;"  id="TestDetailsHeader">Functionalities / Chapters / Cases</h3>';
echo '
<form method="get" action="tests_task.php" style="display:inline;">
<select name="filter" style="margin-left: 25px;">
	<option '.$selNotT.' value="'.CF_NOT_TESTED.'">'.get_case_status_text(CF_NOT_TESTED).'</option>
	<option '.$selC.' value="'.CF_CORRECT.'">'.get_case_status_text(CF_CORRECT).'</option>
	<option '.$selE.' value="'.CF_ERROR.'">'.get_case_status_text(CF_ERROR).'</option>
	<option '.$selNA.' value="'.CF_NOT_AVAILABLE.'">'.get_case_status_text(CF_NOT_AVAILABLE).'</option>
	<option '.$selNT.' value="'.CF_NOT_TESTABLE.'">'.get_case_status_text(CF_NOT_TESTABLE).'</option>
	<option '.$selA.' value="'.CF_ALL.'">'.get_case_status_text(CF_ALL).'</option>
</select>
<input type="hidden" name="testID" value="'.$testID.'"/>
<input type="hidden" name="taskID" value="'.$taskID.'"/>
<input id="FilterCases" type="submit" value="Filter"/>
</form>';

foreach($funcs as $func)
{
	$funcDonePerc = ($stat['functionalities'][$func['id']]['closed'] / $stat['functionalities'][$func['id']]['all']) * 100;
	
	echo '
	<div class="FunctionalityDiv">	
	<div class="FunctionalityHeaderDiv">
		
	<br><table class="TableData">
	<col width="40"><col width="450"><col width="*"><col width="180">
	<tr class="FuncRow">
	<td  class="CursorHand" style="text-align:center;"  onclick="return ExpandCollapse(this);"><a href="#" title="Expand" class="Icon icon_collapse"></a></td>
	<td  class="CursorHand" style="font-weight: bold; padding-right: 15px;"  onclick="return ExpandCollapse(this);"><span class="CategoryName">Name: <br/></span>'.htmlspecialchars($func['name']).'</td>
	<td  class="CursorHand" style="white-space: pre-wrap;"  onclick="return ExpandCollapse(this);"><span class="CategoryName">Description: <br/></span>'.htmlspecialchars($func['description']).'</td>
	<td class="CursorHand" onclick="return ExpandCollapse(this);"><span class="CategoryName">Duration: <br/></span>'.$func['length'].'h<br>';
	
	printf("<span class=\"CategoryName\">Done: <br/></span><span class=\"FuncDoneValue\">%d</span> of <span class=\"FuncAllValue\">%d</span> (<span class=\"FuncPercValue\">%.2f%%</span>)</td>",
			 $stat['functionalities'][$func['id']]['closed'], $stat['functionalities'][$func['id']]['all'], $funcDonePerc);	

	echo '</tr></table></div>';
	
	echo '<div class="FunctionalityContentDiv" >';
	foreach($func['chapters'] as $chapter)
	{
		$chapDonePerc = ($stat['chapters'][$chapter['id']]['closed'] / $stat['chapters'][$chapter['id']]['all']) * 100;
		
		echo '
		<div class="ChapterDiv">
		<div class="ChapterHeaderDiv">
		
		<table class="TableData" cellspacing="0">
		<col width="44"><col width="40"><col width="400"><col width="*"><col width="180">
		<tr class="ChapterRow" style="color: #4D5154;">
		<td style="background-color: #4D5154;"></td>
		<td class="CursorHand" style="text-align:center;" onclick="return ExpandCollapse(this);" ><a href="#" title="Expand" class="Icon icon_collapse"></a></td>
		<td class="CursorHand" style="padding-right: 15px;" onclick="return ExpandCollapse(this);"><span class="CategoryName">Name: <br/></span>'.htmlspecialchars($chapter['name']).'</td>
		<td class="CursorHand" style="white-space: pre-wrap;" onclick="return ExpandCollapse(this);"><span class="CategoryName">Description: <br/></span>'.htmlspecialchars($chapter['description']).'</td>
		<td class="CursorHand" onclick="return ExpandCollapse(this);">';

		printf("<span class=\"CategoryName\">Done: <br/></span><span class=\"ChapDoneValue\">%d</span> of <span class=\"ChapAllValue\">%d</span> (<span class=\"ChapPercValue\">%.2f%%</span>)</td>", $stat['chapters'][$chapter['id']]['closed'],
			 $stat['chapters'][$chapter['id']]['all'], $chapDonePerc);	
		
		echo '</tr></table></div>';		
				
		echo '<div class="ChapterContentDiv">';	

		foreach($chapter['cases'] as $case)
		{
			$selEmpty = '';
			$selOK = '';
			$selBad = '';
			$selNotAvailable = '';
			$selNotTestable = '';
			$wynik = '';

			if ($case['result'] == CF_NOT_TESTED)
			{
				$class = 'Undone';
				$selEmpty = 'selected="selected"';
				$wynik = ' ';
				
				if ($filter != CF_NOT_TESTED && $filter != CF_ALL)
					continue;
			}
			else if ($case['result'] == CF_ERROR)
			{
				$class = 'BadThing';
				$selBad = 'selected="selected"';
				$wynik = get_case_status_text(CF_ERROR);
				
				if ($filter != CF_ERROR && $filter != CF_ALL)
					continue;
			}
			else if ($case['result'] == CF_CORRECT)
			{
				$class = 'Correct';
				$selOK = 'selected="selected"';
				$wynik = get_case_status_text(CF_CORRECT);
				if ($filter != CF_CORRECT && $filter != CF_ALL)
					continue;
			}				
			else if ($case['result'] == CF_NOT_AVAILABLE)
			{
				$class = 'NotAvailable';
				$selNotAvailable = 'selected="selected"';
				$wynik = get_case_status_text(CF_NOT_AVAILABLE);
				if ($filter != CF_NOT_AVAILABLE && $filter != CF_ALL)
					continue;
			}				
			else if ($case['result'] == CF_NOT_TESTABLE)
			{
				$class = 'NotTestable';
				$selNotTestable = 'selected="selected"';
				$wynik = get_case_status_text(CF_NOT_TESTABLE);
				if ($filter != CF_NOT_TESTABLE && $filter != CF_ALL)
					continue;
			}				
			
			echo '<form method="post" action="#">
			<input type="hidden" name="taskID" value="'.$taskID.'"/>
			<input type="hidden" name="testID" value="'.$testID.'"/>
			<input type="hidden" class="CaseID" value="'.$case['id_case'].'" />
			
			<div style="margin-left: 45px;">
			<table class="TableData" cellspacing="0" style="border: 1px solid black; margin-top:5px;">
			<col width="5%"><col width="40%"><col width="*"><col width="300">
			<tr id ="CaseRow'.$case['id_case'].'" class="'.$class.'" style="vertical-align:top;">
			<td><a name="Case_'.$case['id_case'].'" id="Case_'.$case['id_case'].'"></a><b>'.$case['id_case'].'</b></td><td style="width:40%; white-space: pre-wrap; padding-right: 15px;"><b>Procedure:</b><br>'
			.htmlspecialchars($case['procedure_text']).'</td>
			<td style="white-space: pre-wrap; padding-right: 15px;"><b>Expected Result:</b><br>'
			.htmlspecialchars($case['expected_result']).'</td>';
			
			if (($g_login['access_level'] != ADMIN && !$taskOwner)  || $testerTaskData['closed'] == 1 || !$startedTest)
			{
				if ($case['notes'] != '') $note = htmlspecialchars($case['notes']).'<br>';
				else $note = htmlspecialchars($case['notes']);
				
				echo '<td style="white-space: pre-wrap;"><b>Result:</b> '.$wynik.'<br><b>Notes:</b><br>'.make_bug_view_links($note).'<b>Update:</b> '.$case['update_time'].'</td>';				
			}
			else 
			{
				echo '<td><b>Result: </b><select class="Result">			
				<option '.$selEmpty.' value="'.CF_NOT_TESTED.'">&nbsp;</option>
				<option '.$selOK.' value="'.CF_CORRECT.'">'.get_case_status_text(CF_CORRECT).'</option>
				<option '.$selBad.' value="'.CF_ERROR.'">'.get_case_status_text(CF_ERROR).'</option>
				<option '.$selNotAvailable.' value="'.CF_NOT_AVAILABLE.'">'.get_case_status_text(CF_NOT_AVAILABLE).'</option>
				<option '.$selNotTestable.' value="'.CF_NOT_TESTABLE.'">'.get_case_status_text(CF_NOT_TESTABLE).'</option>
				</select><br><b>Notes:</b><textarea class="Notes" rows="0" cols="0" style="width:98%; height: 7em;">';
			
				if ($case['notes'] != "")
					echo $case['notes'];
				
				echo '</textarea>
				<div style="text-align: right;">';
				if ($case['update_time'])
					echo '<span class="UpdateTimeSpan" id="update_time'.$case['id_case'].'">('.$case['update_time'].')</span>';
				else
					echo '<span class="UpdateTimeSpan" id="update_time'.$case['id_case'].'"></span>';
				echo '&nbsp;&nbsp;<input class="UpdateCase" type="submit" value="Update"/></div></td>';
			}
			echo '</tr></table></div></form>';		
		}
		echo '</div></div>';
		echo '<br>';
	}
	echo '</div></div>';
}
echo '</div>';
?>

</div>
<div class="Palette">
<table id="defColors" cellspacing="1">
<tr>
<td class="Undone" width="16%">Not Tested</td>
<td class="Correct" width="16%">Correct</td>
<td class="BadThing" width="16%">Error</td>
<td class="NotAvailable" width="16%">Not Available</td>
<td class="NotTestable" width="16%">Not Testable</td>
<td class="Unknown" width="20%">Unknown</td>
</tr></table>
</div>
 
<?php print_page_end(); ?>