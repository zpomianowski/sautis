<?php
	require_once( 'core.php' );

	html_robots_noindex();

	html_page_top1();
	html_page_top2a();
	echo "\t", '<br />', "\n";
	echo "\t", '<hr size="1" />', "\n";
	?>
	<table border="0" width="100%">
	<tr><th align="center">
	<table border="0" width=800>
	<tr><th align="left">
	<?php
	echo '<a class="help", name="help_title">'.	lang_get('help_title') .'</a>';
	echo "\t", '<br />', "\n";
	echo "\t", '<hr size="1" />', "\n";
	echo '<a href="#what_is_sautis">'. 				lang_get('help_what_is_sautis') .'</a><br/>';
	echo '<a href="#what_is_sautis_for">'. 			lang_get('help_what_is_sautis_for') . '</a><br/><br/>';
	echo '<a class="l1", href="#normal_usage">'. 	lang_get('help_normal_usage') . '</a><br/>';
	echo '<a href="#vpn_access">'.		 			lang_get('help_vpn_access') . '</a><br/>';
	echo '<a href="#sign_up">'.			 			lang_get('help_sign_up') . '</a><br/>';
	echo '<a href="#reporting_issues">'. 			lang_get('help_reporting_issues') . '</a><br/>';
	echo '<a href="#editing_issues">'.	 			lang_get('help_editing_issues') . '</a><br/>';
	echo '<a href="#statuses">'.		 			lang_get('help_statuses') . '</a><br/><br/>';
	echo '<a class="l1", href="#management">'.	 	lang_get('help_management') . '</a><br/>';
	echo '<a href="#global_profiles">'.	 			lang_get('help_global_profiles') . '</a><br/>';
	echo '<a href="#tags">'.	 					lang_get('help_tags') . '</a><br/>';
	echo '<a href="#user_management">'.	 			lang_get('help_user_management') . '</a><br/>';
	echo '<a href="#project_management">'. 			lang_get('help_project_management') . '</a><br/>';
	echo "\t", '<br />', "\n";
	echo "\t", '<hr size="1" />', "\n";

	//Text
	echo '<a class="help3", name="what_is_sautis">'. lang_get('help_what_is_sautis') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_01') .'</p>';
	echo '<br/><br/><br/><br/>';

	echo '<a class="help3", name="what_is_sautis_for">'. lang_get('help_what_is_sautis_for') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_02') .'</p>';
	echo '<br/><br/><br/><br/>';
	echo '<br/><br/><br/><br/>';

	echo '<a class="help2", name="normal_usage">'. lang_get('help_normal_usage') . '</a><br/>';
	echo '<a class="help3", name="vpn_access">'. lang_get('help_vpn_access') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_03') .'</p>';
	echo '<br/><br/><br/><br/>';

	echo '<a class="help3", name="sign_up">'. lang_get('help_sign_up') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_04') .'</p>';
	?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_01_path') ?> width=800 alt="login.png" /></th>
	</tr>
	</table>
<?php 
	echo '<p align="justify">'. lang_get('help_05') .'</p>';
	echo '<p align="justify">'. lang_get('help_06') .'</p>';
	echo '<p align="justify">'. lang_get('help_07') .'</p>';
?>
	<table border="0" width="100%">
		<tr>
			<th align="center">
				<table width="100%">
				<tr class="row-category">
				<td> <?php echo lang_get('help_user_type') ?> </td>
				<td> <?php echo lang_get('help_user_features') ?> </td>
				<td> <?php echo lang_get('help_user_scope') ?> </td>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_viewer') ?> </td>
				<td><?php echo lang_get('help_view') ?></td>
				<td><?php echo lang_get('help_scope01') ?></td>	
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_reporter') ?> </td>
				<td><?php echo lang_get('help_view') ?>, <?php echo lang_get('help_report') ?></td>
				<td><?php echo lang_get('help_scope01') ?></td>		
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_updater') ?> </td>
				<td><?php echo lang_get('help_view') ?>, <?php echo lang_get('help_report') ?>, <?php echo lang_get('help_edit') ?>, (<?php echo lang_get('help_limited') ?>)</td>
				<td><?php echo lang_get('help_scope01') ?></td>		
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_developer') ?> </td>
				<td><?php echo lang_get('help_view') ?>, <?php echo lang_get('help_report') ?>, <?php echo lang_get('help_edit') ?></td>
				<td><?php echo lang_get('help_scope02') ?></td>	
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_manager') ?> </td>
				<td><?php echo lang_get('help_view') ?>, <?php echo lang_get('help_report') ?>, <?php echo lang_get('help_edit') ?>, <?php echo lang_get('help_manage') ?></td>
				<td><?php echo lang_get('help_scope02') ?></td>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_admin') ?> </td>
				<td><?php echo lang_get('help_all') ?></td>
				<td><?php echo lang_get('help_scope02') ?></td>
				</tr>
				</table>
	 		</th>
		</tr>
	</table>
<?php
	echo '<br/><br/><br/><br/>';
	echo '<a class="help3", name="reporting_issues">'. lang_get('help_reporting_issues') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_02_path')?> width=800 alt="report.png" /></th>
	</tr>
	</table>
<?php
	echo '<p align="justify">'. lang_get('help_08') .'</p>';
	echo '<p align="justify">'. lang_get('help_09') .'</p>';
	echo '<p align="justify">'. lang_get('help_10') .'</p>';
	echo '<p align="justify">'. lang_get('help_11') .'</p>';

	echo '<br/><br/><br/><br/>';
	echo '<a class="help3", name="editing_issues">'. lang_get('help_editing_issues') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_12') .'</p>';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_03_path')?> width=800 alt="report.png" /></th>
	</tr>
	</table>
<?php
	echo '<p align="justify">'. lang_get('help_13') .'</p>';
	echo '<p align="justify">'. lang_get('help_14') .'</p>';
	?>
	<table border="0" width="100%">
		<tr>
			<th align="center">
				<table width="100%">
				<tr class="row-category">
				<td> <?php echo lang_get('help_15_1') ?> </td>
				<td> <?php echo lang_get('help_15_2') ?> </td>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_15_10') ?> </td>
				<td width="80%"><?php echo lang_get('help_15_01') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_15_20') ?> </td>
				<td width="80%"><?php echo lang_get('help_15_02') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_15_30') ?> </td>
				<td width="80%"><?php echo lang_get('help_15_03') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_15_40') ?> </td>
				<td width="80%"><?php echo lang_get('help_15_04') ?></td>
				</tr>
				</table>
	 		</th>
		</tr>
	</table>
<?php
	echo '<p align="justify">'. lang_get('help_16') .'</p>';
	echo '<br/><br/><br/><br/>';
	echo '<a class="help3", name="statuses">'. lang_get('help_statuses') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_04_path')?> width=800 alt="report.png" /></th>
	</tr>
	</table>
<?php
	echo '<p align="justify">'. lang_get('help_17') .'</p>';
	echo '<p align="justify">'. lang_get('help_18') .'</p>';
?>
<table border="0" width="100%">
		<tr>
			<th align="center">
				<table width="100%">
				<tr class="row-category">
				<td> <?php echo lang_get('help_15_1') ?> </td>
				<td> <?php echo lang_get('help_15_2') ?> </td>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_19_10') ?> </td>
				<td width="80%"><?php echo lang_get('help_19_01') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_19_20') ?> </td>
				<td width="80%"><?php echo lang_get('help_19_02') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_19_30') ?> </td>
				<td width="80%"><?php echo lang_get('help_19_03') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_19_40') ?> </td>
				<td width="80%"><?php echo lang_get('help_19_04') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_19_50') ?> </td>
				<td width="80%"><?php echo lang_get('help_19_05') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_19_60') ?> </td>
				<td width="80%"><?php echo lang_get('help_19_06') ?></td>
				</tr>
				</tr>
				<tr bgcolor="#5d6164" border="1"><td> <?php echo lang_get('help_19_70') ?> </td>
				<td width="80%"><?php echo lang_get('help_19_07') ?></td>
				</tr>
				</table>
	 		</th>
		</tr>
	</table>
<?php
	echo '<p align="justify">'. lang_get('help_18_fix01') .'</p>';
	
	echo '<br/><br/><br/><br/>';
	echo '<br/><br/><br/><br/>';
	echo '<a class="help2", name="management">'. lang_get('help_management') . '</a><br/>';
	echo '<p align="justify">'. lang_get('help_20') .'</p>';
	echo '<br/><br/><br/><br/>';

	echo '<a class="help3", name="global_profiles">'. lang_get('help_global_profiles') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_21') .'</p>';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_05_path')?> width=800 alt="global_profiles.png" /></th>
	</tr>
	</table>
<?php
	echo '<br/><br/><br/><br/>';

	echo '<a class="help3", name="tags">'. lang_get('help_tags') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_22') .'</p>';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_06_path')?> width=800 alt="project_4.png" /></th>
	</tr>
	</table
<?php
	echo '<br/><br/><br/><br/>';

	echo '<a class="help3", name="user_management">'. lang_get('help_user_management') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
	echo '<p align="justify">'. lang_get('help_23') .'</p>';

	echo '<br/><br/><br/><br/>';

	echo '<a class="help3", name="project_management">'. lang_get('help_project_management') . '</a> [<a href="#help_title">'. lang_get('help_menu') .'</a>]';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_07_path')?> width=800 alt="project_1.png" /></th>
	</tr>
	</table
<?php	
	echo '<p align="justify">'. lang_get('help_24') .'</p>';
	echo '<p align="justify">'. lang_get('help_24') .'</p>';
	echo '<p align="justify">'. lang_get('help_26') .'</p>';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_08_path')?> width=800 alt="project_2.png" /></th>
	</tr>
	</table
<?php
	echo '<p align="justify">'. lang_get('help_27') .'</p>';
?>
	<table border="0" width="100%">
	<tr>
		<th align="center"><img src=<?php echo lang_get('help_09_path')?> width=800 alt="project_3.png" /></th>
	</tr>
	</table
<?php
	echo '<p align="justify">'. lang_get('help_28') .'</p>';
	echo '<br/><br/><br/><br/>';
	echo '<br/><br/><br/><br/>';
?>
</th></tr></table>
</th></tr></table>
<?php html_page_bottom1a( __FILE__ ); ?>
