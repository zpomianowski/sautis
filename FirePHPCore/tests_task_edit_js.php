<?php
// Zażółć gęślą jaźń
// Operacje na zadaniach - żądanie AJAX, zwraca XML

require_once('tests_main.php');
header('Content-type: text/xml; charset=utf-8');
send_header_no_cache();
send_xml_header();
connect_to_database();
login();

function SaveSummary()
{
	$task_id = $_POST['task_id'];
	$summary = StripInput($_POST['summary']);
	if (!is_numeric($task_id)) { print_error('Wrong task ID.'); return; }
	$query = 'UPDATE tests_tasks SET summary="'.mysql_real_escape_string($summary).'" WHERE id_tests_task='.$task_id;
	$result = mysql_query($query);
	if ($result)
		print("<ok/>");
	else
		print_error(mysql_error());
}

function UpdateCase()
{
	$case_id = $_POST['case_id'];
	$resultValue = $_POST['result'];
	$notes = StripInput($_POST['notes']);
	
	if (!is_numeric($case_id)) { print_error('Wrong task ID.'); return; }
	
	$query = 'UPDATE tests_test_cases SET result="'.$resultValue.'", notes="'.mysql_real_escape_string($notes).'", update_time=NOW() 
			 WHERE id_tests_test_cases='.$case_id.' LIMIT 1;';
	
	$result = mysql_query($query);
	
	$query = 'SELECT * FROM tests_test_cases WHERE id_tests_test_cases='.$case_id;
	$result2 = mysql_query($query);
	$row = mysql_fetch_assoc($result2);
	
	if ($result)
		printf("<root><id_case>%d</id_case><result_value>%d</result_value><update_time>%s</update_time></root>",
				 $case_id, $resultValue, $row['update_time']);
	else
		print_error(mysql_error());
}

function print_error($msg)
{
	printf("<error>%s</error>", htmlspecialchars($msg));
}

switch ($_POST['operation'])
{
	case 'save_summary':
		if ($g_login['access_level'] == ADMIN || $g_login['id'] == $_POST['tester_id'])
			SaveSummary();
		else 
			print_error('Access Denied');		
		break;
		
	case 'update_case':
		if ($g_login['access_level'] == ADMIN || $g_login['id'] == $_POST['tester_id'])
			UpdateCase();
		else 
			print_error('Access Denied');		
		break;
		
	default:
		print_error('Wrong Operation.');
}
?>