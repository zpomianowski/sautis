$(function(){  
    //References
    var loading = $("#loading");  
    var container = $("#container");  
    var link;
    var scriptUrl = "plugins/zbh/lib/zbh_chart01.js"; 
    location.hash = 'foo';

      
    //Manage click events  
    $("a.ajax-links").click(function(e){  
    //prevent default action  
    e.preventDefault();
    
    //show the loading bar  
    showLoading();  
    container.slideUp(500);  
      
    //define the target and get content then load it to container  
    link = $(this).attr("href") + " #container"; 

    container.load(link, hideLoading); 
    
    container.slideDown(500, function() { 
        $.getScript(scriptUrl); 
    });

    });

    //show loading bar  
    function showLoading(){  
    loading  
    .css({visibility:"visible"})  
    .css({opacity:"1"})  
    .css({display:"block"})  
    ;  
    }  

    //hide loading bar  
    function hideLoading(){  
    loading.fadeTo(500, 0);
    };  
});