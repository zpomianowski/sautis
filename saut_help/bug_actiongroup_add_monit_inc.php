<?php
# MantisBT - a php based bugtracking system

# MantisBT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# MantisBT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MantisBT.  If not, see <http://www.gnu.org/licenses/>.

	/**
	 * @package MantisBT
	 * @copyright Copyright (C) 2000 - 2002  Kenzaburo Ito - kenito@300baud.org
	 * @copyright Copyright (C) 2002 - 2011  MantisBT Team - mantisbt-dev@lists.sourceforge.net
	 * @link http://www.mantisbt.org
	 */

	/**
	 * Prints the title for the custom action page.
	 */
	function action_add_monit_print_title() {
        echo '<tr class="form-title">';
        echo '<td colspan="2">';
        echo lang_get( 'add_bugmonit_title' );
        echo '</td></tr>';
	}

	/**
	 * Prints the field within the custom action form.  This has an entry for
	 * every field the user need to supply + the submit button.  The fields are
	 * added as rows in a table that is already created by the calling code.
	 * A row has two columns.
	 */
	function action_add_monit_print_fields() {
		echo '<tr class="row-1" valign="top"><td class="category">', lang_get( 'add_bugmonit_user' ), '</td><td>'; ?>
		<select name="user">
				<?php
				if (access_has_project_level( DEVELOPER )) print_assign_to_option_list( null, null );
				else {
					$t_user_id = auth_get_current_user_id();
					echo '<option value=' , $t_user_id, '>', user_get_name( $t_user_id ),'</option>';
				}
				?>
		</select>
		<?php
		echo '</td></tr>';
		echo '<tr><td colspan="2"><center><input type="submit" class="button" value="' . lang_get( 'add_bugmonit_button' ) . ' " /></center></td></tr>';
	}

	/**
	 * Validates the action on the specified bug id.
	 *
	 * @returns true|array Action can be applied., ( bug_id => reason for failure )
	 */
	function action_add_monit_validate( $p_bug_id ) {
		$f_user = gpc_get_int( 'user' );

		$t_failed_validation_ids = array();
		$t_bug_id = $p_bug_id;

		if ( bug_is_readonly( $t_bug_id ) ) {
			$t_failed_validation_ids[$t_bug_id] = lang_get( 'actiongroup_error_issue_is_readonly' );
			return $t_failed_validation_ids;
		}

		if ( !access_has_bug_level( MANAGER, $t_bug_id ) ) {
		 	$t_failed_validation_ids[$t_bug_id] = lang_get( 'access_denied' );
		 	return $t_failed_validation_ids;
		}

		if ( !access_has_bug_level( VIEWER, $t_bug_id, $f_user ) ) {
		 	$t_failed_validation_ids[$t_bug_id] = lang_get( 'access_user_denied' );
		 	return $t_failed_validation_ids;
		}

		return true;
	}

	/**
	 * Executes the custom action on the specified bug id.
	 *
	 * @param $p_bug_id  The bug id to execute the custom action on.
	 * @returns true|array Action executed successfully., ( bug_id => reason for failure )
	 */
	function action_add_monit_process( $p_bug_id ) {
		$f_user = gpc_get_int( 'user' );
		bug_monitor( $p_bug_id, $f_user );
        return true;
    }
