<?php
// Funkcje rozsyłające maile 
	

require_once('tests_main.php');
require_once("tests_config.php");
include_once('library/phpmailer/class.phpmailer.php');

class DODMailer extends PHPMailer
{
    var $priority = 3;
    var $to_name;
    var $to_email;
    var $From = null;
    var $FromName = null;
    var $Sender = null;

    function DODMailer()
    {
        global $g_mailer;
       
        // Comes from config.php $g_mailer array

        if($g_mailer['smtp_mode'] == 'enabled')
        {
            $this->Host = $g_mailer['smtp_host'];
            $this->Port = $g_mailer['smtp_port'];
            if($g_mailer['smtp_username'] != '')
            {
                $this->SMTPAuth = true;
                $this->Username = $g_mailer['smtp_username'];
                $this->Password = $g_mailer['smtp_password'];
            }
            $this->Mailer = "smtp";
        }
        if(!$this->From)
        {
            $this->From = $g_mailer['from_email'];
        }
        if(!$this->FromName)
        {
            $this-> FromName = $g_mailer['from_name'];
        }
        if(!$this->Sender)
        {
            $this->Sender = $g_mailer['from_email'];
        }
        $this->Priority = $this->priority;
        
        $this->CharSet = 'utf-8';
        
        $this->ClearReplyTos();
        $this->AddReplyTo($g_mailer['reciever_email'], $g_mailer['reciever_email']);
    }
    
    function SendMail()
    {
        global $g_mailer;
    	
    	if ($g_mailer['auto_mails_enabled'] == true)
    		return $this->Send();    	
    }
}

function get_user_email($id_user)
{
	$query = "SELECT email FROM mantis_user_table WHERE id=".$id_user;
	$result = mysql_query( $query );
	if ($result !== null)
	{
		$row = mysql_fetch_assoc($result);
		return $row['email'];
	}
	return null;
}

function get_emails($priv)
{
	$emails = array();
	
	$query = "SELECT mut.email, mut.id, tu.access_level FROM mantis_user_table mut JOIN tests_users tu ON mut.id=tu.id_user WHERE tu.access_level=".$priv;
	$result = mysql_query( $query );
	if ($result !== null)
	{
		$i = 0;
		while($row = mysql_fetch_assoc($result))
		{
			$emails[$i] = get_user_email($row['id']);
			$i++;
		}		
	}
	else
		return null;

	return $emails;
}

function get_footer_email()
{
	$footer = "\r\n";
	$footer .= "Pozdrawiamy \r\n";
	$footer .= "Dział Oprogramowania Dekoderów\r\n\r\n";	
	$footer .= "P.S. Nie odpowiadaj na tą wiadomość, jest generowana automatycznie!";
	
	return $footer;
}

function ReportStartTest($testID)
{
	global $g_server_host;
	
	$testData = get_test_data($testID);
	
	$mailer = new DODMailer();			 	
	$mailer->Subject = "[ATMS] Rozpoczęcie testu";
	
	$content = "Witaj!\r\n\r\n";
	$content .= "Właśnie został rozpoczęty nowy test.\r\n\r\n";
	$content .= "Projekt: 		".get_project_name($testData['id_project'])."\r\n";
	$content .= "Test:			".$testData['name']."\r\n";
	$content .= "Wersja: 		".$testData['planned_name']."\r\n";
	$content .= "Szczegóły: 	".$g_server_host."tests_test_details.php?testID=".$testID."\r\n";
	$content .= get_footer_email();
	
	$mailer->Body = $content;
	
	$admins = get_emails(ADMIN);
	$observers = get_emails(OBSERVER);
	$testers = get_emails(TESTER);
	
	$emails = array_merge($admins, $observers, $testers);
	
	foreach ($emails as $email)
		$mailer->AddAddress($email, $email);

	if(!$mailer->SendMail())
	    return $mailer->ErrorInfo;	  
}

function ReportEndTest($testID)
{
	global $g_server_host;
	
	$testData = get_test_data($testID);
	
	$mailer = new DODMailer();			 
	$mailer->Subject = "[ATMS] Zakończenie testu";
	
	$content = "Witaj!\r\n\r\n";
	$content .= "Właśnie jeden z testów został zakończony.\r\n\r\n";
	$content .= "Projekt: 		".get_project_name($testData['id_project'])."\r\n";
	$content .= "Test:			".$testData['name']."\r\n";
	$content .= "Wersja: 		".$testData['planned_name']."\r\n";
	$content .= "Podsumowanie: 	".$testData['summary']."\r\n";
	$content .= "Status Oprogramowania: 	".get_software_status_text_PL($testData['software_status'])."\r\n";
	$content .= "Szczegóły: 	".$g_server_host."tests_test_details.php?testID=".$testID."\r\n";
	$content .= get_footer_email();
	
	$mailer->Body = $content;
	
	$admins = get_emails(ADMIN);
	$observers = get_emails(OBSERVER);
	$testers = get_emails(TESTER);
	
	$emails = array_merge($admins, $observers, $testers);
	
	foreach ($emails as $email)
		$mailer->AddAddress($email, $email);

	if(!$mailer->SendMail())
	    return $mailer->ErrorInfo;
}

function ReportCloseTask($taskID)
{
	global $g_server_host;
	
	$taskData = get_task_data($taskID);
	$testID = $taskData['id_test'];
	$testData = get_test_data($testID);
	$func_chap_data = get_func_chap_data($testID);
	$mailer = new DODMailer();			 	
	$mailer->Subject = "[ATMS] Zakończenie zadania";
	
	$content = "Witaj!\r\n\r\n";
	$content .= "Właśnie jedno z zadań zostało zakończone.\r\n\r\n";
	$content .= "Projekt: 		".get_project_name($testData['id_project'])."\r\n";
	$content .= "Test:			".$testData['name']."\r\n";
	$content .= "Wersja: 		".$testData['planned_name']."\r\n";
	$content .= "Tester: 		".get_user_name($taskData['id_user'])."\r\n";
	$content .= "Zadanie: 		\r\n";

	foreach ($func_chap_data as $func)
	{
		if ($func['users'][$taskData['id_user']])
		{
			$content .= "	".$func['name']."\r\n";
			foreach ($func['chapters'] as $chap)
			{
				if ($chap['user'] == $taskData['id_user'])
				{
					$content .= "		".$chap['name']."\r\n";					
				}
			}
		}
	}
	
	$content .= "\r\nPodsumowanie: 	".$taskData['summary']."\r\n";
	$content .= "Szczegóły: 	".$g_server_host."tests_task.php?testID=".$testID."&taskID=".$taskID."\r\n";
	
	$content .= get_footer_email();
	
	$mailer->Body = $content;
	
	$admins = get_emails(ADMIN);
	
	foreach ($admins as $email)
		$mailer->AddAddress($email, $email);

	if(!$mailer->SendMail())
	    return $mailer->ErrorInfo;
}

function ReportOpenTask($idTask)
{
	global $g_server_host;
	
	$taskData = get_task_data($taskID);
	$testID = $taskData['id_test'];
	
	$mailer = new DODMailer();			 
	
	$mailer->Subject = "[ATMS] Wznowienie zadania";
	
	$content = "Witaj!\r\n\r\n";
	$content .= "Jedno z twoich zadań zostało ponownie otwarte.\r\n";
	$content .= $g_server_host."tests_task.php?testID=".$testID."&taskID=".$idTask;
	$content .= "\r\n".get_footer_email();
	
	$mailer->Body = $content;
	
	$query = 'SELECT id_tests_task, id_user FROM tests_tasks WHERE id_tests_task='.$idTask;
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);
	$email = get_user_email($row['id_user']);
	
	$mailer->AddAddress($email, $email);

	if(!$mailer->SendMail())
	    return $mailer->ErrorInfo;
}

?>