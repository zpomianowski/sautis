<?php
// Szczegóły testu
// Zażółć gęślą jaźń
require_once('tests_main.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

print_page_begin('Test Information');

$testID = $_GET['testID'];
$testData = get_test_data($testID);

$usersTimes = get_users_task_times($testID);

echo '<div class="Content">';
?>
<script type="text/javascript">

function ExpandCollapse(myElement)
{
	var elem = $(myElement);
	var icon = elem.parents('.TestDiv').find('a');
	var div = elem.parents('.TestDiv').next('div');
	
	icon.toggleClass('icon_collapse');
	icon.toggleClass('icon_expand');

	div.toggle('fast');
	
	return false;	
}
</script>

<div class="TestDiv"><a onclick="return ExpandCollapse(this);" href="#" title="Expand" class="icon_collapse"></a><h3 style="display:inline;padding-left: 5px;"  id="TestDetailsHeader">Details</h3></div>
<div id="TestDetailsDiv">
<table class="TableData"> 
<tr><td>
	<table class="TableData">
	<?php 
	if ($testData['summary'] != null) 
		echo '
	<tr>
		<td class="category">
		<label>Summary</label>
		</td>
		<td class="value" style="white-space: pre-wrap;">'.make_bug_view_links(htmlspecialchars($testData['summary'])).'
		</td>
	</tr>';
	?>
	<tr>
		<td class="category">
		<label>Description</label>
		</td>
		<td class="value" style="white-space: pre-wrap;"><?php echo make_bug_view_links(htmlspecialchars($testData['description'])); ?>
		</td>
	</tr>

<?php 
if ($testData['id_planned_soft'] != null)
	echo '
	<tr>
		<td class="category">
		<label>Software</label>
		</td>
		<td class="value">
		'.htmlspecialchars($testData['planned_name']).'
		</td>
	</tr>
	<tr>
		<td class="category">
		<label>Software Delivery</label>
		</td>
		<td class="value">
		'.$testData['software_arrive_date'].'
		</td>
	</tr>
	<tr>
		<td class="category">
		<label>Software Description</label>
		</td>
		<td class="value">
		'.htmlspecialchars($testData['planned_description']).'
		</td>
	</tr>';

	echo '
	</table>
</td></tr>
</table>
</div>';
	
$testersData = get_test_testers($testID);
	
echo '<br>';

print('<div class="TestDiv"><a onclick="return ExpandCollapse(this);" href="#" title="Expand" class="icon_collapse"></a><h3 style="display:inline;padding-left: 5px;">Testers</h3></div>
<div id="TestTestersDiv">
<table class="TableData Select">	
<!-- Header -->
<tr class="Header">
	<th width="150">Tester</th>
	<th width="250">Progress</th>
	<th>Description</th>
	<th width="110">Planned Start</th>
	<th width="110">Planned End</th>
	<th width="150">Status</th>
	<th width="20">
		<a style="DISPLAY:block;width:100%;height:100%;" href="tests_test_details.php?testID='.$testID.'&amp;preview=-1"><img src="images/show.png" alt="Expand" border="0" /></a>	
	</th>
</tr>');

while($row = mysql_fetch_assoc($testersData))
{
	$statData = get_cases_statistics($row['id_tests_task']);
	
	if (!$statData['all'])
		$casesDonePerc = 0;
	else $casesDonePerc = ($statData['closed'] / $statData['all']) * 100;
	
	$now = strftime("%Y-%m-%d", strtotime("now"));
	$nowDate = strtotime($now);
	$startDate = strtotime($row['start_date']);
	
	$status = '';
	if ($row['closed'] == 1)
	{
		$status = 'Closed';
		$class = 'Closed';	
	}
	else if ($startDate > $nowDate)
	{
		$status = 'Waiting';
		$class = 'Waiting';			
	}
	else if ($startDate <= $nowDate)
	{
		$status = 'In Progress';
		$class = 'InProgress';			
	}
	else 
	{
		$status = 'Unknown';
		$class = 'Unknown';			
	}
		
	if ($row['realname'] == '')
		$user = $row['username'];
	else
		$user = $row['realname'];
	
	$linkStart = '<a style="display: block; height:100%; width:100%;" href="tests_task.php?testID='
				.$testID.'&amp;taskID='.$row['id_tests_task'].'">';
	$linkEnd = '</a>';
		
	echo '<tr class="'.$class.'">';
	echo '<td class="TesterRow">'.$linkStart.$user.$linkEnd.'</td>';
	echo '<td class="TesterRow">';
	$weight = get_user_weight($row['id_user']);
	$UserTime = round($usersTimes[$row['id_user']]['open_time'] * $weight,1);
	printf('%s Done %d of %d (%.2f%%) (%.1fh left) %s', $linkStart, $statData['closed'], $statData['all'], $casesDonePerc, $UserTime, $linkEnd);
	echo '</td>';
	
	$text = htmlspecialchars($row['description']);
	if ($_GET['preview'] == $row['id_tests_task'] || $_GET['preview'] == -1 || (strlen($text)<80))
		echo '<td class="TesterRow" style="white-space: pre-wrap;">'.$linkStart.$text.$linkEnd.'</td>';
	else
		echo '<td class="TesterRow">'.$linkStart.mb_substr($text, 0, 80, "utf-8").$linkEnd.'</td>';
	
	echo '<td class="TesterRow DateCell">'.$linkStart.$row['start_date'].$linkEnd.'</td>';
	echo '<td class="TesterRow DateCell">'.$linkStart.$row['stop_date'].$linkEnd.'</td>';
	echo '<td class="TesterRow">'.$linkStart.$status.$linkEnd.'</td>';
	if (strlen($text)>80)
		echo '<td class="TesterRow"><a style="DISPLAY:block;width:100%;height:100%;" href="tests_test_details.php?testID='.$testID.'&amp;preview='.$row['id_tests_task'].'"><img src="images/show.png" alt="Expand" border="0"/></a></td>';
	else echo '<td class="TesterRow"></td>';	
	echo '</tr>';
}

print('</table></div>');
?>

</div><div class="Palette">
<table id="defColors" cellspacing="1">
<tr>
<td class="Waiting" width="25%">Waiting</td>
<td class="InProgress" width="25%">In Progress</td>
<td class="Closed" width="25%">Closed</td>
<td class="Unknown" width="25%">Unknown</td>
</tr></table>
</div>
 

<?php print_page_end(); ?>