<?php
# Note that we don't have to remove the Updater entry from the localisation file
# bug in Mantis when using 'auto' language assignment - I copied all below strings to string_<lang>.txt files, and it works fine now 
if ( true ) return;
if ( lang_get_current() === 'polish' ) {
	$s_saut_issue_type = 'Typ problemu';
	$s_saut_closed_by_id = 'Zamknięty przez';
	$s_priority_enum_string             = '10:niski,30:średni,60:wysoki,90:krytyczny';
	$s_severity_enum_string				= '10:niski,30:średni,60:wysoki,90:krytyczny';
	$s_status_enum_string				= '10:otwarty,90:zamknięty';
	$s_resolution_enum_string			= '10:po stronie dostawcy,20:po stronie CP,25:niemożliwe,30:naprawiony przez dostawcę,60:świeżo otwarty,70:ciągle w trakcie testów,80:zamknięty przez CP,90:otwarty ponownie';
	$s_saut_issue_type_enum_string      = '10:Firmware,30:Software,60:FW + SW,90:Inny';
	$s_orct								= '(Otwarte/Zamknięte/Wszystkich)';
	$s_by_saut_issue_type				= 'wg typu problemu';
	$s_closed							= 'Zamknięte';

	$s_help_menu						= 'menu główne';
	$s_help_title						= 'SAUTis - krótki poradnik';
	$s_help_what_is_sautis				= 'Czym jest SAUTis?';
	$s_help_what_is_sautis_for	  		= 'Jaka jest rola SAUTisa?';
	$s_help_normal_usage     	  		= 'Jak używać';
	$s_help_vpn_access     	  			= 'Dostęp do VPN';
	$s_help_sign_up     	  			= 'Rejestracja';
	$s_help_reporting_issues			= 'Raportowanie błędów';
	$s_help_editing_issues				= 'Aktualizacja błędów';
	$s_help_statuses					= 'Omówienie statusów';
	$s_help_management					= 'Zarządzanie';
	$s_help_global_profiles				= 'Globalne profile';	
	$s_help_tags						= 'Znaczniki';
	$s_help_user_management				= 'Zarządzanie użytkownikami';
	$s_help_project_management			= 'Zarządzanie projektami';

	$s_help_img_path = 'images/saut_help_pl.png';
	$s_help_01_path = 'saut_help/login_pl.png';
	$s_help_02_path = 'saut_help/report_pl.png';
	$s_help_03_path = 'saut_help/edit_pl.png';
	$s_help_04_path = 'saut_help/statuses_pl.png';
	$s_help_05_path = 'saut_help/project_1_pl.png';
	$s_help_06_path = 'saut_help/project_2_pl.png';
	$s_help_07_path = 'saut_help/project_3_pl.png';
	$s_help_08_path = 'saut_help/project_4_pl.png';
	$s_help_09_path = 'saut_help/project_5_pl.png';

	$s_help_01 = 'SAUTis jest prostym systemem stworzonym na potrzeby śledzenia błędów. Serwis jest dedykowany na potrzeby SAUT (Sekcja Analiz Urządzeń Telekomunikacyjnych) w Cyfrowym Polsacie S.A.';

	$s_help_02 = 'Wspomniany serwis webowy z założenia ma dostarczać rozwiązania pozwalające na efektywną wymianę danych i wspomagać koordynację wspólnych projektów. Intecją było stworzenie praktycznego narzędzia pozwalającego na śledzenie oraz synchronizację błędów, uwag, doświadczeń wśród pracowników CP jak i dostawców, z którymi CP wpspółpracuje.';

	$s_help_03 = 'SAUTis jest wewnętrznym narzędziem dostępnym dla sekcji i departamentów Cyfrowego Polsatu S.A. Dostęp jest gwarantowany w ramach wewnętrznej sieci korporacyjnej. Użytkownicy oraz współpracownicy, którzy nie są podpięci bezpośrednio do wspomnianej sieci potrzebują specjalnego dostępu VPN (Virtual Private Network). Dopóki użytkownik nie uzyska odpowiedniego konta VPN i certyfikatów dla uwierzytelnienia jego osoby, użytkowanie systemu SAUTis jest niemożliwe. Aby uzyskać dostęp do sieci VPN, użytkownik potrzebuje zgłosić się do kierownika SAUT:' .
	"<br/><br/>
	1) Kierownik SAUT dostaje zgłoszenie o potrzebie wystawienia zleceń ( max 1 dzień ):<br/>
	&#160;&#160;&#160;&#160;&#160;&#160;&#160;a) założenie kont osobowych w systemie IT<br/>
	&#160;&#160;&#160;&#160;&#160;&#160;&#160;b) założenie kont VPN osobom posiadającym konto osobowe w systemie IT<br/>
	&#160;&#160;&#160;&#160;&#160;&#160;&#160;c) Należy dostarczyć Imię/Nazwisko/e-mail/nr Tel GSM<br/><br/>
	2) IT zakłada konto dla nowej osoby w systemie ( 1 dzień )<br/><br/>
	3) IT kreuje konto VPN wysyła SMSem login i hasło ( 1 dzień )";


	$s_help_04 = 'Dopiero w momencie zestawienia połączenia VPN, użytkowanie SAITisa jest możliwe. Użytkownik potrzebuje jedynie wpisać "saut" w pasek adresu swojej przeglądarki internetowej. Wejście na "saut" prowadzi do strony logowania, gdzie użytkownik musi się zarejestrować.';

	$s_help_05 = 'Rejestracja odbywa się poprzez odzielną stronę (patrz na rys wyżej). Należy dostarczyć unikalną nazwę dla użytkownika (jeśli serwis informuje, że nazwa, którą wpisał użytkownik już istnieje, należy wybrać i wpisać inne). Ponadto należy wpisać swój adres email. Jest bardzo ważnym by adres email był prawdziwy i był dobrze wpisany. Bez konta email użytkownik nie będzie w stanie dostać linku aktywacyjnego, a co za tym idzie, nie będzie w stanie określić hasła dla swojego konta. Dostęp do konta wówczas będzie niemożliwy.';

	$s_help_06 = 'Domyślnym kontem dla nowego użytkownika jest "obserwator". Ten typ konta pozwala jedynie na przeglądanie projektów i dodanych do nich błędów. Jeśli użytkownik potrzebue większej interakcji w systemie, potrzebuje skontaktować się z administratorem lub webmasterem SAUTisa w celu uzyskania wyższych poziomów dostępu.';

	$s_help_07 = 'Możliwe typy kont:';

	$s_help_08 = 'Oprócz pokazanych wyżej pól, są jeszcze pola: "Temat", "Opis", "Kroki, by powtórzyć" and "Dodatkowe informacje". Te pola raczej nie wymagają dodatkowego komentarza.';

	$s_help_09 = 'Ponadto należy zwrócić uwagę na zestaw pól: "Platforma", "OS" oraz "wersja OS". Tutaj jest możliwość stworzenia predefiniowanych profili, które reprezentowałyby najczęstsze zestawy tych pól. Ogólnie rzecz biorąc w polu "Platforma" użytkownik powinien opisać fizyczne urządzenie, na którym testy były przeprowadzane (np. komputer, laptop, model lub wyszczególnione urządzenie), "OS": np. Windows XP Pro, Windows 7 Home, Linux Ubuntu itd., "OS Version": np. 5.1.2600 Service Pack 3.';

	$s_help_10 = 'Zazwyczaj "Przypisz do" jest pozostawiany jako pole puste. Jeżeli użytkownik uważa, że ktoś wśród dostawców lub ekipy CP jest specjalistą, osobą kompetentną lub zwyczajnie jest odpowiedzialny za danego typu problem, podczas procesu zgłaszania można przypisać konkretnego użytkownika do konkretnego problemu. Problem opisany w ten sposób ma większą szansę na rozwiązanie i zamknięcie jeśli jak najszybciej trafi do odpowiedniej osoby. Interfejs umożliwa wybór z listy spośród zarejestrowanych użytkowników';

	$s_help_11 = 'Ostatnim, lecz nie najmniej ważnym jest pole załącznika. W tym miejscy użytkownik może dodawać pliki, które potwierdzają problem lub mogą pomóc w ustaleniu źródła problemu. Maksymalna wielkość takiego pliku to 50MB. W przypadku, gdy taka wielkość kontentu nie wystarcza należy spróbować plik spakować. W przypadku większej ilości plików można dodawać je oddzielnie. W innych przypadkach związanymi z oggraniczeniami w użytkowaniu SAUTisa należy kontaktować się z administratorem serwisu.';

	$s_help_12 = 'Edycja błędów dotyczy głównie tych pól, które wymagają weryfikacji. Pola, które z definicji nie powinny się zmieniać są dla większości typów kont tylko do odczytu.';

	$s_help_13 = 'W trakcie rozwiązywania problemów jedynie część faktów może uledz zmianie.';

	$s_help_14 = 'Użytkownicy w szczególności powinni czuć się w obowiązku by śledzić poniższe pola:';

	$s_help_15_1  = 'Pole';
	$s_help_15_2  = 'Opis';
	$s_help_15_10 = 'Przypisz do';
	$s_help_15_01 = 'Jeśli konkretny użytkownik zna temat (miał z nim wcześniej do czynienia lub za dany typ zwyczajnie odpowiada) i może tutaj pomóc, użytkownik powinien go przypisać do danego problemu - taka czynność pomoże uzyskać uwagę tego użytkownika dla tego problemu. Przypisywanie błędów powinno odbywać się pomiędzy pracownikami dostawcy.';
	$s_help_15_20 = 'Termin realizacji';
	$s_help_15_02 = 'Reprezentant dostawcy powinien w tym polu wskazać szacowany czas naprawy błędu (lub wpisać estymowany czas, kiedy dana kwestia będzie miała szansę przejść do następnego etapu - w przypadku ciężkich przypadków).';
	$s_help_15_30 = 'Typ problemu';
	$s_help_15_03 = 'Dostawca powinien zweryfikować czy dany problem został poprawnie przypisany (czy dobrze została określona dziedzina problemu).';
	$s_help_15_40 = 'Status prac';
	$s_help_15_04 = 'Prawdopodobnie najważniejsze pole - wskazuje aktualny stan problemu i pozwala synchronizować działania w ramach problemu po stronie CP i dostawcy';

	$s_help_16 = 'Dodatkowo należy wspomnieć, że każdy problem można komentować. Komentarze są gromadzone pod każdym problemem w postaci kolejkowanej konwersacji.';

	$s_help_17 = 'Są dwa rodzaje statusów. Najważniejszy jest "status". Wskazuje on czy dany problem został zamknięty czy nadal jest aktualny.';

	$s_help_18 = 'Drugi status "status prac" wskazuje na obecny stan problemu, określa także kto (CP czy dostawca) jest w danej chwili za dany etap odpowiedzialny:';

	$s_help_18_fix01 = 'Dostawca może posługiwać się tylko trzema statusami określającymi postęp pracy. W momencie, gdy problem ma status "świeżo otwarty", oznacza to, że pojawił się niedawno i nikt się nim nie zajmuje. Dostawca w tym momecnie przeglądając nowe problemy powinien zmienić ten status na "po stronie dostawcy". Przedstawiciel CP wówczas wie, że ktoś danym problemem się zajmuje. W momencie, gdy dostawca potrzebuje odpowiedzi ze strony CP lub sytuacja problemy się zmieniła - dostawca powieniec zmienić status na "po stronie CP". Reakcja CP będzie potwierdzona zmianą statusu na "po stronie dostawcy" w celu uzyskania kolejnej odpowiedzi od strony dostawcy. Dostawca w momecie naprawy problemu zmienia status na "naprawiono przez dostawcę" - osoba z CP wówczas naprawę weryfikuje i w zależności od wyników zmienia status na "zamknięto przez CP" lub "po stronie dostawcy".';

	$s_help_19_1  = 'Stan';
	$s_help_19_2  = 'Opis';
	$s_help_19_10 = 'świeżo otwarty';
	$s_help_19_01 = 'Wskazuje na świeży problem, z którym dostawca nie zdążył się zapoznać.';
	$s_help_19_20 = 'po stronie dostawcy';
	$s_help_19_02 = 'Dostawca zapoznał się z problemem i potwierdza, że nim się zajmuje.';
	$s_help_19_30 = 'po stronie CP';
	$s_help_19_03 = 'Dostawca wskazuje, że CP powinno ustosunkować się do wyników analizy/naprawy.';
	$s_help_19_40 = 'naprawiony przez dostawcę';
	$s_help_19_04 = 'Dostawca wskazuje, że problem został rozwiązany. Potrzebne potwierdzienie CP.';
	$s_help_19_50 = 'ciągle w trakcie testów';
	$s_help_19_05 = 'Kandydat do zamknięcia, ale nadal obserwowany.';
	$s_help_19_60 = 'zamknięty przez CP';
	$s_help_19_06 = 'CP zweryfikowało naprawę/analizę i zamyka problem.';
	$s_help_19_70 = 'ponownie otwarty';
	$s_help_19_07 = 'Problem z przeszłości znów zaczął się pojawiać (np. po wielokrotnej zmianie FW).';

	$s_help_20 	  = 'Zarządzający ma dostęp do konfiguracji: kont użytkowników, projektów, znaczników oraz globalnych profili.';

	$s_help_21	  = 'Profile globalne pełnią funkcję ułatwiającą dodawanie nowych problemów. Taki profil składa się z zestawu trzech pól - opisują one stosunkowo dokładnie urządzenie oraz oprogramowanie komputera, na którym testowano urządzenie telekomunikacyjne. Profile mogą być wyjątkowo użyteczne w przypadku, gdy większosć testów jest przeprowadzanych na tych samych komputerach (np. komputerach testowych w laboratorium).';

	$s_help_22 	  = 'Zarządzający może dodawać znaczniki do każdego zaobserwowanego problemu – są to jedynie wskaźniki, które mogą pomóc podczas wyszukiwania i filtrowania bazy problemów.';

	$s_help_23	  = 'Zarządzanie użytkownikami wiąże się z możliwością edycji poziomu dostępu dla każdego z użytkowników. Ponadto zarządzający może dodawać, usuwać użytkowników oraz resetować ich hasła. W ramach zarządzania użytkownikami wlicza się również dodawanie i usuwanie użytkowników do projektów prywatnych.';

	$s_help_24	  = 'Dla każdego projektu można dodawać podprojekty. Podprojekty mają taką samą strukturę jak zwykły projekt. Różnica polage na tym, że podprojekt jest w strukturze związany z jego nadrzędnym projektem, dodatkowo można ustalić jakie ustawienia i atrybuty podprojekt powienien dziedziczyć po swoim nadrzędnym projekcie.';

	$s_help_25	  = 'Kategorie – jest to pole wymagane podczas każdego dodawania problemów. Typowo używane są dwa rodzaje problemów: funkcjonalne i translacje (błędy w tłumaczeniach). Zarządzający podczas dodawania nowego projektu powinien pamiętać o dodaniu tych dwóch kategorii.';

	$s_help_26 	  = 'Dobrą praktyką jest dodawać każdą nową konfigurację wersji SW, FW i HW – predefiniowane kombinacje wówczas są dostępne z rozwijanej listy podczas dodawania nowych problemów.';

	$s_help_27 	  = 'Bardzo ważne jest by pilnować status widoczności projektu - tylko projekty prywatne umożliwiają kontrolę nad tym, kto może w ramach danego projektu działać i je listować. Projekty publiczne są widoczne dla wszystkich użytkowników.';

	$s_help_28 	  = 'Standardowo jedynie administrator ma widoczność absolutną (widzi projekty prywatne mimo, że nie jest do nich przypisany). Mantis jednak został skonfigurowany w taki sposób, że od poziomu dostępu wykonawcy wzwyż projekty prywatne są domyślnie dostępne i widoczne. W takim przypadku zgłaszający, aktualizujący oraz obserwator nie mają domyślnie dostępu do tych projektów. Zarządzający może manualnie zdecydować, kto może zostać dodany do projektu i jaki poziom dostępu powinien mu być przyznany.';

	$s_help_user_type 		= 'Użytkownik';
	$s_help_user_features 	= 'Możliwe akcje';
	$s_help_user_scope 		= 'Poziom dostępu';
	$s_help_viewer			= 'obserwator';
	$s_help_reporter		= 'zgłaszający';
	$s_help_updater			= 'aktualizujący';
	$s_help_developer		= 'wykonawca';
	$s_help_manager 		= 'zarządzający';
	$s_help_admin			= 'administrator';
	$s_help_view			= 'obserwacja';
	$s_help_edit			= 'edycja';
	$s_help_report			= 'raportowanie';
	$s_help_manage			= 'zarządzanie';	
	$s_help_all				= 'brak ograniczeń';
	$s_help_limited			= 'pewne ograniczenia';
	$s_help_scope01			= 'tylko dedykowane projekty';
	$s_help_scope02			= 'wszystkie projekty (tylko CP)';
}
if ( lang_get_current() === 'english' ) {
	$s_saut_issue_type = 'Issue Type';
	$s_saut_closed_by_id = 'Closed by';
	$s_priority_enum_string             = '10:low,30:medium,60:high,90:critical';
	$s_severity_enum_string				= '10:low,30:medium,60:high,90:critical';
	$s_status_enum_string				= '10:open,90:closed';
	$s_resolution_enum_string			= '10:with vendor,20:with CP,25:can\'t resolve,30:fixed by vendor,60:just opened,70:still under testing,80:closed by CP,90:reopen';
	$s_saut_issue_type_enum_string      = '10:Firmware,30:Software,60:FW + SW,90:Other';
	$s_orct								= '(Opened/Closed/Total)';
	$s_by_saut_issue_type				= 'By Issue Type';
	$s_closed							= 'Closed';

	$s_help_menu						= 'main menu';
	$s_help_title						= 'SAUTis - very short help';
	$s_help_what_is_sautis				= 'What is SAUTis?';
	$s_help_what_is_sautis_for	  		= 'What the SAUTis was created for?';
	$s_help_normal_usage     	  		= 'Normal usage';
	$s_help_vpn_access     	  			= 'VPN access';
	$s_help_sign_up     	  			= 'Signing up';
	$s_help_reporting_issues			= 'Raporting issues';
	$s_help_editing_issues				= 'Editing issues';
	$s_help_statuses					= 'Statuses';
	$s_help_management					= 'Management';
	$s_help_global_profiles				= 'Global profiles';	
	$s_help_tags						= 'Tags';
	$s_help_user_management				= 'User management';
	$s_help_project_management			= 'Project management';

	$s_help_img_path = 'images/saut_help_eng.png';
	$s_help_01_path = 'saut_help/login_eng.png';
	$s_help_02_path = 'saut_help/report_eng.png';
	$s_help_03_path = 'saut_help/edit_eng.png';
	$s_help_04_path = 'saut_help/statuses_eng.png';
	$s_help_05_path = 'saut_help/project_1_eng.png';
	$s_help_06_path = 'saut_help/project_2_eng.png';
	$s_help_07_path = 'saut_help/project_3_eng.png';
	$s_help_08_path = 'saut_help/project_4_eng.png';
	$s_help_09_path = 'saut_help/project_5_eng.png';

	$s_help_01 = 'SAUTis is a simple system made for bug tracking purposes. System is dedicated for SAUT (pl: Sekcja Analiz Urządzeń Telekomunikacyjnych, eng: Section of Telecommunications Equipment Analysis) in Cyfrowy Polsat S.A.';

	$s_help_02 = 'This web service provides solutions for effective data sharing and helps to coordinate common projects. It is meant to be a practical tool among vendors and Cyfrowy Polsat’s employees for tracking, viewing and synchronizing issues which appear during the development and verification processes.';

	$s_help_03 = 'SAUTis is an internal tool which is available for CP sections and departments within CP’s network. Users and cooperators from outside need special VPN access. Until you get such VPN account it is impossible to use SAUTis. To get VPN access you need to contact with SAUT manager:
	<br/><br/>
	1) SAUT manager takes requests about creating VPN accounts to access SAUTis ( max 1 day ):<br/>
	&#160;&#160;&#160;&#160;&#160;&#160;&#160;a) creating new corporate accounts - IT<br/>
	&#160;&#160;&#160;&#160;&#160;&#160;&#160;b) creating VPN accounts for people who have corporate accounts - IT<br/>
	&#160;&#160;&#160;&#160;&#160;&#160;&#160;c) You need to provide your name, surname, e-mail and GSM phone number<br/><br/>
	2) IT creates new accounts in the IT system ( 1 day )<br/><br/>
	3) IT creates new VPN accounts VPN and sends login and password by SMS to particular persons ( 1 day )';

	$s_help_04 = 'First of all – after establishing VPN connection you are able to use SAUTis. You need to enter "saut" into your web browser. After that, you have to sign up before login.';

	$s_help_05 = 'You will be redirected to signup page. You have to enter your user name (it has to be unique, if already someone registered your user name – please, use another one) and valid email address. It is very important – without email account it will be impossible to send you activation link and setup your password. After activation you can use your new password to login.';

	$s_help_06 = 'The default account type is viewer. Viewer account allows you only to view projects and their issues. If you need interaction with SAUT team you need to write to administrator or webmaster that you need higher privileges.';

	$s_help_07 = 'Possible account types:';

	$s_help_08 = 'In addition to this there are four text fields: "Summary", "Description", "Steps to Reproduce" and "Additional Information". These do not need comments.';

	$s_help_09 = 'There is also a box which includes fields like "Platform", "OS" and "OS version". It is possible to make predefinied profiles of these fields. In general in "Platform" we should depict machine that we used in tests (computer, laptop, model, or particular machine), "OS": Windows XP Pro, Windows 7 Home, Linux Ubuntu etc., "OS Version": eg. 5.1.2600 Service Pack 3.';

	$s_help_10 = 'Normally "Assign To" we left blank, but if we think that someone is capable to handle this problem or he is a specialist which should take a look at this issue then choose proper user from the list.';

	$s_help_11 = 'It is possible to add files and attachments to the issue (max 50MB per attachment). In case when this value is insufficient please consider zipping the file/files (7zip is very effective if you have no ideas what kind of software you should use). If still you have problems, be aware that there is possibility to add files as separate attachments. When apart of mentioned methods you still have problems - contact with the administrator of SAUTis.';

	$s_help_12 = 'Editing is possible mainly due to verification, updating statuses that may change.';

	$s_help_13 = 'During development it is possible to change only some of the facts. That is why only few fields can be edited by major accounts.';

	$s_help_14 = 'Editors should feel obligated to trace and verify these fields:';

	$s_help_15_1  = 'Field';
	$s_help_15_2  = 'Description';
	$s_help_15_10 = 'Assigned To';
	$s_help_15_01 = 'If someone is a specialist and can help, by changing this field we should gain his interest. Assignment should be used among the vendor\'s employees.';
	$s_help_15_20 = 'Due Date';
	$s_help_15_02 = 'Cooperator should try to estimate how long can it take to solve the problem (or estimate the time which is needed to skip to the next stage of development).';
	$s_help_15_30 = 'Issue Type';
	$s_help_15_03 = 'Cooperator should verify the problem and indicate/correct the source of the problem.';
	$s_help_15_40 = 'Current Worflow Status';
	$s_help_15_04 = 'Propably the most important field; indicates current work status.';

	$s_help_16 = 'In addition to this there is also a possibility to comment the carry on a conversation about the issue.';

	$s_help_17 = 'There are two kind of statuses. The most important is ‘status’. It indicates if the issue is closed or still under investigation.';

	$s_help_18 = 'Second one helps to indicate the current workflow status:';

	$s_help_18_fix01 = 'Vendor can use only three of statuses which describes work progress. When issue is fresh (has "just opened" status) it means, that it is very new and someone should take care of it. Vendor in this situation, after viewing and forwarding to RND should change this status to "with vendor". CP will know that issue is processed. At the moment, when vendor needs feedback from CP - status change to "with CP" is required. After CP acquaintance, issue will be updated and commented. To get vendor\'s response, CP will change status to "with vendor". When issue is claimed as fixed, vendor changes status to "fixed by vendor". At the time CP team will know that the issue is a candidate to close. After successful verification CP changes work status to "closed by CP", main status will be changed from "open" to "closed". This is a main indicator that issue is officially fixed and working on it is officially finished. Otherwise, in the case of failed verification, CP will comment issue properly and will change status to "with vendor" to get vendor\'s comment and response';

	$s_help_19_1  = 'State';
	$s_help_19_2  = 'Description';
	$s_help_19_10 = 'just opened';
	$s_help_19_01 = 'Indicates new issue which vendor didn’t acquaint with yet.';
	$s_help_19_20 = 'with vendor';
	$s_help_19_02 = 'Vendor has acquainted with the issue and confirms it is under investigation.';
	$s_help_19_30 = 'with CP';
	$s_help_19_03 = 'Vendor indicates that CP should assume an attitude to the issue.';
	$s_help_19_40 = 'fixed by vendor';
	$s_help_19_04 = 'Vendor claims that issue is resolved, CP verification is needed.';
	$s_help_19_50 = 'still under testing';
	$s_help_19_05 = 'Candidate to be closed, but still observed.';
	$s_help_19_60 = 'closed by CP';
	$s_help_19_06 = 'CP verified the fix.';
	$s_help_19_70 = 'reopen';
	$s_help_19_07 = 'Issue appears again (in a new FW for eg.).';
	
	$s_help_20 	  = 'Manager can manage: users, projects, tags and global profiles.';

	$s_help_21	  = 'Global profiles can ease usage of three fields related to test machines. It can improve comfort of adding issues – especially when there are particular machines with fixed OS in the lab.';

	$s_help_22	  = 'Manager can also add tags – these are only indicators which can be pin to the issues.';

	$s_help_23	  = 'Management of users means that manager can edit access level of each user. Moreover he can add, delete users and reset their password. In addition to this, he can pin users to particular projects and change the account settings.';

	$s_help_24	  = 'To each project you can add subproject. Subproject has similar structure as common project. The difference is that it is under main project and you can specify from which project it should inherit main properties and settings.';

	$s_help_25	  = 'Categories – it is obligatory field which has to be specified when you add issue. Common type of issues are "functional" and "translations" so manager should remember about adding this two categories.';

	$s_help_26 	  = 'It is a good practice to add available versions of tested product – versions will be available then for the reporters from a combobox.';

	$s_help_27 	  = 'It was said before that it is very important to be careful and keep an eye on the view status – only for private projects we can restrict access for another users.';

	$s_help_28 	  = 'Normally private projects can see only administrator. Configuration of this Mantis allows developers and above to see private projects. Access is restricted for viewer, reporter and updater. Manager can manually decide who should be involved in particular projects and also can set the access level.';

	$s_help_user_type 		= 'User';
	$s_help_user_features 	= 'Features';
	$s_help_user_scope 		= 'Scope';
	$s_help_viewer			= 'Viewer';
	$s_help_reporter		= 'Reporter';
	$s_help_updater			= 'Updater';
	$s_help_developer		= 'Developer';
	$s_help_manager 		= 'Manager';
	$s_help_admin			= 'Administrator';
	$s_help_view			= 'view';
	$s_help_edit			= 'edit';
	$s_help_report			= 'report';
	$s_help_manage			= 'manage';
	$s_help_all				= 'all privileges';
	$s_help_limited			= 'some limits';
	$s_help_scope01			= 'dedicated projects only';
	$s_help_scope02			= 'all projects (only CP)';
}	
?>