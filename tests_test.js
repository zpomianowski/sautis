// Kodowanie UTF-8, test: Zażółć gęślą jaźń

// Rozwijanie i zwijanie bloków
function ExpandCollapse(myElement)
{
	var elem = $(myElement);

	var div = elem.parents('div')[0];
	var icon = $(div).find('a');
	var block = $(div).next('div');
	
	icon.toggleClass('icon_collapse');
	icon.toggleClass('icon_expand');

	block.toggle('fast');

	ScrollPageEvent();
	
	return false;	
}

// Rozwijanie i zwijanie  wszystkich bloków funkcjonalności
function ExpandCollapseDivsDown(myA)
{
	var a = $(myA);
	var div = a.parents('#TasksDiv');
	
	a.toggleClass('icon_collapse');
	a.toggleClass('icon_expand');

	var func = $('#TasksManagerDiv').find('.FunctionalityContentDiv');
	var icon = $('#TasksManagerDiv').find('a');
	
	if (a.hasClass('icon_collapse'))
	{
		$(func).show('fast');
		$(icon).removeClass('icon_expand');
		$(icon).addClass('icon_collapse');
	}
	else
	{
		$(func).hide('fast');
		$(icon).removeClass('icon_collapse');
		$(icon).addClass('icon_expand');
	}
			
	return false;	
}

// Sprawdzenie czy jakies wymagane pola nie sa puste
function CheckFields()
{
	var name = $('#TestName').val();
	var desc = $('#TestDescription').val();

	if (name == '')
	{
		alert('Field Name must be filled!');
		return false;
	}

	if (desc == '')
	{
		alert('Field Description must be filled!');
		return false;
	}
	return true;
}

// Update select#planned_soft_select based on selected project.
function update_planned_soft_list()
{
	var project_select = $('select#project_select');
	var planned_soft_select = $('select#planned_soft_select');

	var selected_project_id = project_select.val();
	
	planned_soft_select.empty();
	planned_soft_select.append($('<option value="">(Brak)<\/option>'));
	
	var planned_soft_item_count = planned_soft.length;
	for (var i = 0; i < planned_soft_item_count; i += 3)
	{
		var project_id = planned_soft[i];
		if (project_id == selected_project_id)
		{
			var planned_soft_name = planned_soft[i+1];
			var planned_soft_id = planned_soft[i+2];

			var option = $('<option />')
				.attr('value', planned_soft_id)
				.text(planned_soft_name);
			if (planned_soft_id == g_planned_soft_id)
				option.attr('selected', 'selected');
			planned_soft_select.append(option);
		}
	}
}

// kasowanie napisu 'zapisane' jezeli zmienil sie focus
function FocusTestFields()
{
	$('#save_result').text('');
}

// Odpowiedz na zdarzenie zapisu danych testu
function SaveTestResponse(result)
{
	var error = $('error', result);
	var res = $('#save_result');

	if (error.size())
	{
		res.text(error.text());
		res.removeClass('GreenText');		
		res.addClass('RedText');
	}		
	else
	{
		res.text('saved');
		res.removeClass('RedText');		
		res.addClass('GreenText');		
	}
}

// Zdarzenie zapisu ustawien testu
function SaveTestClick()
{
	if (!CheckFields())
		return false;
	
	var projectID = $('#project_select').val();
	var softID = $('#planned_soft_select').val();
	var name = $('#TestName').val();
	var description = $('#TestDescription').val();
	var summary = $('#test_summary').val();

	var chbStart = $('#chbStartDate').prop('checked');
	var start_date = $('#test_start_date').val();

	var chbStop = $('#chbEstimatedStopDate').prop('checked');
	var estimated_stop_date = $('#test_estimated_stop_date').val();

	var chbSoftArrive = $('#chbSoftArriveDate').prop('checked');
	var soft_arrive_date = $('#test_soft_arrive_date').val();
	
	var params = {operation: 'save_test', testID: testID, projectID: projectID, softID: softID, name: name, description: description, summary: summary};

	if (chbStart == true)
		params.startDate = start_date;
	
	if (chbStop == true)
		params.estimatedStopDate = estimated_stop_date;

	if (chbSoftArrive == true)
		params.softArriveDate = soft_arrive_date;

	$.ajax({type:'POST', url:'tests_test_edit_js.php', data: params, success: SaveTestResponse, dataType:'xml'});

	return false;	
}

//Zdarzenie zwrotne po próbie wystartowania testu testu
function StartTestResponse(result)
{
	var error = $('error', result);
	var tasks = $('noTasks', result);
	if (error.size())
	{
		alert(error.text());
	}
	else if (tasks.size())
	{
		alert("There are no Tasks definied. Test cannot start!");
	}
	else
	{
		window.location.reload();
	}	
}

// Zdarzenie zwrotne po próbie zamkniecia testu
function StopTestResponse(result)
{
	var error = $('error', result);
	var res = $('#save_result');

	if (error.size())
	{
		alert(error.text());
	}
	else
	{
		window.location.reload();
	}	
}

// Odpowiedz na zdarzenie sprawdzenia czy sa w tescie jakies otwarte taski
function CheckOpenTasksResponse(result)
{
	var openTasks = $('openTasks', result);
	var soft_status = $('#soft_status_select').val();

	if (openTasks.text() > 0)
	{
		var res = confirm('Warning! Open tasks will be automatically closed.\nAre youy sure?');
		if (!res)
			return false;
	}	

	var params = {operation: 'stop_test', testID: testID, softStatus: soft_status};
	$.ajax({type:'POST', url:'tests_test_edit_js.php', data: params, success: StopTestResponse, dataType:'xml'});
	
	return false;
}

//Zdarzenie wystartowania testu - sprawdza czy jest ustawiony teoretyczny czas zakonczenia testu
function StartTestClick()
{
	var chbStop = $('#chbEstimatedStopDate').prop('checked');

	if (chbStop == false)
	{
		alert('You cannot start Test while Calculated End Test is not set!');
		return false;
	}

	var params = {operation: 'start_test', testID: testID};
	$.ajax({type:'POST', url:'tests_test_edit_js.php', data: params, success: StartTestResponse, dataType:'xml'});
	
	return false;
}

// Zdarzenie zamkniecia testu
function StopTestClick()
{
	var summary = $('#test_summary').val();
	var soft_status = $('#soft_status_select').val();
	if (summary == '')
	{
		alert('You cannot stop Test while Summary field is empty!');
		return false;
	}

	if (soft_status == 0)
	{
		alert('You cannot stop Test without setting Software Status!');
		return false;
	}

	var params = {operation: 'check_open_tasks', testID: testID};
	$.ajax({type:'POST', url:'tests_test_edit_js.php', data: params, success: CheckOpenTasksResponse, dataType:'xml'});

	return false;
}

// Zdarzenie przewijania strony, zmienia sie wtedy pozycja listy testerów
function ScrollPageEvent()
{
	var yOffset = window.pageYOffset;
	var div = $('#TestersList');

	var offset = 0;
	
	var testDiv = $('#TestDiv');
	var icon = $(testDiv).find('a');
	if (icon.hasClass('icon_collapse'))
	{
		offset = 825;
		if ($.browser.msie)
			offset = 780;
	}
	else
	{
		offset = 160;
		if ($.browser.msie)
			offset = 155;
	}

	if (yOffset > offset)
	{
		div.css('position', 'fixed');
		div.css('top', '0');		
	}
	else
	{	
		div.css('position', 'absolute');
		div.css('top', offset+'px');		
	}
}

// Złapanie 'użytkownika' - tworzy sie wtedy element przy kursorze
function DragUserEntity(e)
{
	$('body').disableSelection();
	
	var elemText = $(this).text();
	var elemID = $(this).prop('id');

	var entity = $('<table id="SelectedUser" class="UserDragEntity Params_'+elemID+'"><tr> <td><td class="SelectedUserName">'+ elemText+'<\/table>');
	
	if ($(this).hasClass('NewUser'))
		entity.addClass('NewUser');

	if (e)
	{
		entity.css('left', 2+e.pageX+'px');
		entity.css('top', 2+e.pageY+'px');
	}

	$('body').append(entity);	
}

// Upuszczenie testera na rozdzial lub funkcjonalnosc
function DropUserEntity()
{
	var selected = $('#SelectedUser');
	if (selected.size() == 0)
		return;
		
	var data = GetParamsFromClass(selected);
	var userID = data['U'];
	var funcID = GetParamsFromClass($(this))['FuncID'];
	
	data['Name'] = $('#SelectedUser').find('.SelectedUserName').text();	

	var addToChap = 0;
	
	if ($(this).hasClass('ChapterRow'))
	{
		addToChap = (AddUserToChapter($(this), userID, data['Name'])) ? 1 : 0;
	}	
	else if ($(this).hasClass('FuncRow'))
	{
		var chapters = $('.TRChapter.FuncID'+funcID);		
		for(var i = 0; i < chapters.size(); i++)
			addToChap += (AddUserToChapter($(chapters[i]), userID, data['Name'])) ? 1 : 0;
	}
	
	if (addToChap == 0)
		return;
		
	if (!selected.hasClass('NewUser'))
		DeleteUserEntity();

	var funcDiv = $('#FunctionalityUserContainer'+funcID);
	if (funcDiv.find('#F-'+funcID+'_U-'+userID).size() == 0)
	{
		var newSpan = '<span id="F-'+funcID+'_U-'+userID+'" '; 				 
		newSpan += 'class="UserDrag UserInFunctionality Params_UserID-'+userID+'" style="display:inline-block;"> '+data['Name']+' </span>';		
		var span = $(newSpan);
		span.mousedown(DragUserEntity);
		funcDiv.append(span);
	}
	ValidateFunctionalitiesLoad();
	TimeLeftSum();
	$('body').enableSelection();
}

// Dodaje testera do rozdzialu
function AddUserToChapter(chap, userID, name)
{
	dest = chap.find('span.UserInChapter');

	if (dest.size() > 0)
		return false;
	
	var chapData = GetParamsFromClass(chap);
	
	var chapID = chapData['ChapterID'];
	var funcID = chapData['FuncID'];
		
	var newSpan = '<span id="F-'+funcID+'_C-'+chapID+'_U-'+userID+'" ';
	newSpan += 'class="UserDrag CursorHand UserInChapter ';
	newSpan += 'FuncID'+funcID+' UserID'+userID+' '; 
	newSpan += 'Params_FuncID-'+funcID+'_ChapterID-'+chapID+'_UserID-'+userID+'"> '+name+' </span>';
	var span = $(newSpan);
	span.mousedown(DragUserEntity);
	
	$('#ChapterUserContainer'+chapID).append(span);
	$(chap).find('td.LoadTD').addClass('FullLoadTR');
	
	var timeRatio = GetChapterTimeRatio(funcID, chapID); 
	AddTaskTimeToUser(userID, timeRatio);
	
	return true;
}

// Zwraca tablice z parametrami zapisanymi jako nazwa klasy:
// 		Params_[param_name-param_value]
// 			param_name - nazwa parametru i poźniej indeks w tabeli
// 			param_value - wartość parametru
function GetParamsFromClass(element)
{
	var dataClass = '';
	var classes = element.attr('class');

	if (!classes)
		return null;

	var arr = classes.split(' ');
	for (var i = 0; i < arr.length; i++)
	{
		arr[i] = arr[i].replace(/(\r\n|\n|\r|\t)/gm, '');
		if (arr[i].indexOf('Params_') == 0)
		{
			dataClass =arr[i];
			break;
		}
	}

	if (!dataClass)
		return null;
	
	dataClass = dataClass.replace('Params_', '');	

	var ret = new Array();
	var arr = dataClass.split('_');
	var keyValuePair;
	
	for (var i = 0; i < arr.length; i++)
	{
		keyValuePair = arr[i].split('-');
		ret[keyValuePair[0]] = keyValuePair[1]; 
	}

	return ret;
}

// Zdarzenie upuszczenia testera na śmietnik
function DeleteUserEntity(e)
{
	var ret = false;	
	var arrData = GetParamsFromClass($('#SelectedUser'));
	
	if (!arrData)
		return ret;

	var userID = arrData['U'];
	var funcID = arrData['F'];

	if (arrData['C'])
	{
		var chapID = arrData['C'];
		var timeRatio = GetChapterTimeRatio(funcID, chapID); 
		
		var chapUser = $('#F-'+funcID+'_C-'+chapID+'_U-'+userID);
		var funcUser = $('#F-'+funcID+'_U-'+userID);
		
		$('#TRChapter-'+chapID+' td.LoadTD').removeClass('FullLoadTR');
		chapUser.remove();
		
		if($('.UserID'+userID+'.FuncID'+funcID).size() == 0)
			funcUser.remove();

		AddTaskTimeToUser(userID, -timeRatio);
		ret = true;
	}
	else
	{
		var chapUser = $('.UserDrag.UserID'+userID+'.FuncID'+funcID);
		var allChapUsers = $('.UserID'+userID+'.FuncID'+funcID);

		ret = (chapUser.size() == allChapUsers.size()) ? true : false;
		
		var timeRatio = GetFunctionalityTimeRatio(funcID, userID); 

		var chaps = $(chapUser).parents('.ChapterRow');

		for(var i = 0; i < chaps.size(); i++)
		{
			($(chaps[i]).find('.LoadTD')).removeClass('FullLoadTR');	
		}
		chapUser.remove();
		
		AddTaskTimeToUser(userID, -timeRatio);

		if (ret)
			$('#F-'+funcID+'_U-'+userID).remove();
	}

	ValidateFunctionalitiesLoad();
	TimeLeftSum();
	$('body').enableSelection();
}

// Aktualizuje przydzielony czas zadań na liście testerów - do aktualnego czasu dodaje "time"
function AddTaskTimeToUser(userID, time)
{
	var elemTimeTake = $('#User'+userID+'TaskTime');
	var userWeight = parseFloat($('#User'+userID+'Weight').text());
	var elemTimeTakeVal = elemTimeTake.text();

	if (!elemTimeTakeVal)
		elemTimeTakeVal = '0h (0h)';

	var cTime = elemTimeTakeVal.split(' ')[1];
	cTime = cTime.replace('(', '');
	cTime = cTime.replace('h)', '');
	
	cTime = parseFloat(parseFloat(cTime) + parseFloat(time));
	console.log(parseFloat(cTime));
	
	if (cTime > 0.04 )
	{
		elemTimeTake.text((cTime*userWeight).toFixed(1)+'h ('+ (cTime).toFixed(1) +'h)');
	}
	else elemTimeTake.text('');
}

// Pobiera szacowaną wartośc czasu potrzebnego na wykonanie otwartych casów z rozdziału 
function GetChapterTimeRatio(funcID, chapID)
{
	var chap = $('#TRChapter-'+chapID);
	var params = GetParamsFromClass(chap);
	return (params['CaseTimeRatio'] * params['OpenCases']);
}

// Pobiera szacowaną wartośc czasu potrzebnego na wykonanie otwartych casów z funkcjonalności 
function GetFunctionalityTimeRatio(funcID, userID)
{
	var retTime = 0;
	var chaps = $('.TRChapter.FuncID'+funcID);

	for(var i = 0; i < chaps.size(); i++)
	{
		var userSpan = $(chaps[i]).find('.UserInChapter');
		if (userSpan.size() == 0)
			continue;
		
		var userParams = GetParamsFromClass(userSpan); 
		var params = GetParamsFromClass($(chaps[i]));

		if (userParams['UserID'] == userID)
			retTime += params['CaseTimeRatio'] * params['OpenCases'];
	}
	return retTime;
}

// Zdarzenie ruchu myszką w dokumencie
function MouseMoveEvent(e)
{
	var selected = $('#SelectedUser');

	if (selected)
	{
		selected.css('left', 2+e.pageX+'px');
		selected.css('top', 2+e.pageY+'px');
	}
}

// Zdarzenie puszczenia 'testera' gdzie indziej niz potrzeba - skasowanie elementu przy kursorze myszki
function MouseUpEvent(e)
{
	$('#SelectedUser').remove();
}

// Sprawdza załadowanie wszystkich funkcjonalności
function ValidateFunctionalitiesLoad()
{
	var funcs = $('.FuncRow');

	for(var i = 0; i < funcs.size(); i++)
	{
		ValidateFunctionalityLoad($(funcs[i]));
	}
}

// Zmienia styl funkcjonalności w zależności od liczby przydzielonych do niej rozdziałów
function ValidateFunctionalityLoad(func)
{
	var all = 0;
	var assigned = 0;
	var firstTD = $(func).find('td:first-child');
	var params = GetParamsFromClass(func);
	var funcID = params['FuncID'];
	
	var chaps = $('.TRChapter.FuncID'+funcID);
	all = chaps.size();

	for(var i = 0; i < all; i++)
		assigned += $(chaps[i]).find('.UserInChapter').size();

	// wszystko przypisane
	if (all == assigned)
	{
		$(firstTD).removeClass('BlockedTR');
		$(firstTD).removeClass('LoadTR');
		$(firstTD).addClass('FullLoadTR');
	}
	// nic nie przypisane
	else if (assigned == 0)
	{
		$(firstTD).removeClass('BlockedTR');
		$(firstTD).removeClass('FullLoadTR');
		$(firstTD).removeClass('LoadTR');
	}
	// przypisane tylko troche
	else
	{
		$(firstTD).removeClass('FullLoadTR');
		$(firstTD).removeClass('BlockedTR');
		$(firstTD).addClass('LoadTR');
	}

	// zakonczone
	if (params['OpenCases'] == 0)
	{
		$(firstTD).removeClass('FullLoadTR');
		$(firstTD).removeClass('LoadTR');		
		$(firstTD).addClass('BlockedTR');
	}
}

// Zdejmuje wszystkich testerów z rozdziałów (z tych, z których sie da)
function ClearAllButton()
{
	var users = $('.UserInFunctionality');

	for (var i = 0; i < users.size(); i++)
	{		
		var elemText = $(users[i]).text();
		var elemID = $(users[i]).prop('id');
		var entity = $('<table id="SelectedUser" class="UserDragEntity Params_'+elemID+'"><tr> <td><td class="SelectedUserName">'+ elemText+'<\/table>');		

		$('body').append(entity);	

		DeleteUserEntity(null);
		MouseUpEvent(null);
	}
	return false;
}

// Zdarzenie powrotne dla update tasków
function UpdateButtonResponse(result)
{
	var error = $('error', result);

	if (error.size())
	{
		alert('Błąd: '+ error.text());
	}		
	else
		alert('Test zaktualizowany.');		
}

// Funkcja przypisuje casy testerom - fizycznie w bazie
function UpdateButton()
{
	var params;
	var chapterLoadData = '';
	var chaps = $('tr.TRChapter');
	
	for (var i = 0; i < chaps.size(); i++)
	{
		if (chapterLoadData != '')
			chapterLoadData += ':';
		
		params = GetParamsFromClass($(chaps[i]));
		
		var user = '';
		var spanUser = $(chaps[i]).find('span.UserInChapter');
		if (spanUser.size() > 0)
		{
			user = GetParamsFromClass($(spanUser))['UserID'];
		}	
		
		chapterLoadData += 'ChapterID-'+params['ChapterID']+'_UserID-'+user;
	}
	
	var taskDesc = $('textarea.TaskDescription');
	var userTaskDesc = '';
	var userTask = '';
	for (var i = 0; i < taskDesc.size(); i++)
	{
		if (userTask != '')
			userTask += ':';
		if (userTaskDesc != '')
			userTaskDesc += '###';

		params = GetParamsFromClass($(taskDesc[i]));

		var dateStop = $('#task_stop_date'+params['UserID']);
		var dateStart = $('#task_start_date'+params['UserID']);

		userTask += 'UserID-'+params['UserID']+'_StartDate-'+dateStart.val() + '_StopDate-'+dateStop.val();
		userTaskDesc += 'Desc:::'+$(taskDesc[i]).val();
	}
	
	var testID = $('#ITestID').val();
	var parameters = {operation: 'update_tasks', chapterLoadData: chapterLoadData, userTask: userTask, userTaskDesc: userTaskDesc, testID: testID};
	$.ajax({type:'POST', url:'tests_test_edit_js.php', data: parameters, success: UpdateButtonResponse, dataType:'xml'});

	return false;
}

// Sumuje czas testu jaki jeszcze pozostał do zakończenia
function TimeLeftSum()
{
	var taskTime = $('td.TaskTime');
	var sumWithWeight = 0;
	var sum = 0;
	
	for(var i = 0; i<taskTime.size();i++)
	{
		var arr = $(taskTime[i]).text();
		if (!arr)
			arr = '0h (0h)';

		var cTime = arr.split(' ');
		cTime[0] = cTime[0].replace('h', '');
		cTime[1] = cTime[1].replace('(', '');
		cTime[1] = cTime[1].replace('h)', '');

		sumWithWeight += parseFloat(cTime[0]);
		sum += parseFloat(cTime[1]);
	}
	
	var sumTime = $('#TasksTimeSummary');
	sumTime.text(sumWithWeight.toFixed(1) + 'h (' + sum.toFixed(1) + 'h)');
}

function init()
{
	var div = $('#TestersList');
	var offset = 825;
	if ($.browser.msie)
		offset = 780;
	div.css('top', offset+'px');		

	
	$('#project_select, #planned_soft_select, #Name, #Description, #test_summary, #chbStartDate, #chbEstimatedStopDate, #chbSoftArriveDate').focusin(FocusTestFields);			
	$('#StartTest').click(StartTestClick);			
	$('#StopTest').click(StopTestClick);			
	$('#SaveTest').click(SaveTestClick);			
	$(window).scroll(ScrollPageEvent);
	$('select#project_select').change(update_planned_soft_list);

	$(document).mousemove(MouseMoveEvent);
	$(document).mouseup(MouseUpEvent);
	
	$('.UserDrag').mousedown(DragUserEntity);
	$('.UserDrop').mouseup(DropUserEntity);
	$('#TrashIcon').mouseup(DeleteUserEntity);
	
	update_planned_soft_list();
	ValidateFunctionalitiesLoad();
	TimeLeftSum();
}

$(init);

