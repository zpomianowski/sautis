<?php
require_once('tests_config.php');

function string_process_case_link($string)
{
	global $g_server_host;
	
	$str = preg_replace("/(%)(\d+)/", '<a style="color:#0000ff; text-decoration:underline;" target="_blank" href="'.$g_server_host.'tests_task.php?redirectToCase=$2">$0</a>', $string);
	
	return $str;
}

?>