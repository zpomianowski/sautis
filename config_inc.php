<?php
	$g_hostname = 'mysqlmaster';
	$g_db_type = 'mysql';
	$g_database_name = 'saut_bugtracker';
	$g_db_username = 'saut';
	$g_db_password = 'papasmerflubijagody?2011';

	date_default_timezone_set('Europe/Warsaw');

	$g_file_upload_method    	= DISK;
	#$g_file_upload_ftp_server	= '193.164.142.251';
	#$g_file_upload_ftp_user		= 'saut';
	#$g_file_upload_ftp_pass		= 'uwep0ew86';
	$g_max_file_size		    = 60000000;
	$g_absolute_path_default_upload_folder = '/var/www/html/saut.polsatc/files/';

	$g_show_user_email_threshold = REPORTER;
	$g_send_reset_password = ON;
	$g_enable_email_notification = ON;
	$g_administrator_email  = 'saut@cyfrowypolsat.pl';
	$g_webmaster_email      = 'saut@cyfrowypolsat.pl';
	$g_from_email           = 'saut@cyfrowypolsat.pl';
	$g_return_path_email    = 'saut@cyfrowypolsat.pl';
	$g_validate_email = OFF;
	$g_email_receive_own    = OFF;

	$g_phpMailer_method = PHPMAILER_METHOD_SMTP;
	$g_smtp_host = 'outlook.corp.plusnet';
	$g_smtp_port = 587;
	$g_smtp_username = 'polkomtel\saut';
	$g_smtp_password = 'KonczaSieHasla12#$';
	$g_smtp_connection_mode = 'tls';
	$g_log_level = LOG_EMAIL | LOG_EMAIL_RECIPIENT;// | LOG_FILTERING | LOG_AJAX;
	$g_log_destination = 'file:duuuuuuuuuuuuuuuuuupa.log';

	$g_limit_email_domain	= OFF;
	$g_phpMailer_method	= PHPMAILER_METHOD_SMTP;
	$g_use_phpMailer = OFF;


	$g_view_filters = ADVANCED_ONLY;
	$g_signup_use_captcha = OFF;
	$g_show_detailed_errors	= ON;

	//enums
	$g_priority_enum_string	            = '10:low,30:medium,60:high,90:critical';
	$g_status_enum_string				= '10:open,90:closed';
	$g_resolution_enum_string			= '10:with vendor,20:with CP, 25:can\'t resolve ,30:fixed by vendor,60:open,70:under testing,80:closed by CP,90:reopen';
	$g_saut_issue_type_enum_string      = '10:Firmware,30:Software,60:FW + SW,90:Other';
	$g_severity_enum_string				= '10:low,30:medium,60:high,90:critical';

	//defaults
	$g_default_limit_view = 500;
	$g_default_language		= 'auto';
	$g_window_title			= 'SAUTis';
	$g_default_bug_priority = SAUT_PRIORITY_HIGH;
	$g_default_bug_resolution = SAUT_RESOLUTION_OPEN;
	$g_default_new_account_access_level	= VIEWER;
	$g_default_bug_view_status = VS_PUBLIC;
	$g_default_bug_reproducibility = REPRODUCIBILITY_ALWAYS;
	$g_due_date_update_threshold = UPDATER;
	$g_due_date_view_threshold = UPDATER;
	$g_create_project_threshold = MANAGER;
	$g_delete_project_threshold = MANAGER;
	$g_set_view_status_threshold = DEVELOPER;
	$g_auto_set_status_to_assigned	= OFF;
	$g_show_user_email_threshold = REPORTER;
	$g_show_user_realname_threshold = REPORTER;
	$g_reminder_receive_threshold = UPDATER;
	$g_handle_bug_threshold			= UPDATER;
	$g_private_project_threshold = DEVELOPER;
	$g_set_view_status_threshold = DEVELOPER;
	$g_update_bug_assign_threshold = UPDATER;
	$g_bug_reminder_threshold		= REPORTER;
	$g_change_view_status_threshold = DEVELOPER;
	$g_update_readonly_bug_threshold = UPDATER;
	$g_manage_global_profile_threshold = DEVELOPER;
	$g_manage_user_threshold = MANAGER;
	$g_logo_image			= 'images/header-logo.png';
	$g_max_dropdown_length = 50;

	$g_excel_columns = array ( 'id', 'project_id', 'reporter_id', 'handler_id', 'priority', 'severity', 'reproducibility', 'version', 'projection', 'category_id', 'date_submitted', 'eta', 'os', 'os_build', 'platform', 'view_state', 'last_updated', 'summary', 'description', 'steps_to_reproduce','additional_information', 'status', 'resolution', 'fixed_in_version' );

	$g_bug_report_page_fields = array(
		'category_id',
		'view_state',
		'priority',
		'reproducibility',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'attachments',
		'saut_issue_type',
	);

	$g_bug_view_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'tags',
		'attachments',
		'due_date',
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_bug_print_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'tags',
		'attachments',
		'due_date',
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_bug_update_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'attachments',
		'due_date',
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_bug_change_status_page_fields = array (
		'id',
		'project',
		'category_id',
		'view_state',
		'date_submitted',
		'last_updated',
		'reporter',
		'priority',
		'reproducibility',
		'status',
		'resolution',
		'projection',
		'eta',
		'platform',
		'handler',
		'os',
		'os_version',
		'product_version',
		'product_build',
		'target_version',
		'fixed_in_version',
		'summary',
		'description',
		'additional_info',
		'steps_to_reproduce',
		'tags',
		'attachments',
		'due_date',
		'saut_issue_type',
		'saut_closed_by_id',
	);

	$g_resolution_enum_workflow = array();

	$g_status_icon_arr = array (
		SAUT_PRIORITY_LOW 		=> 'priority_low_1.gif',
		SAUT_PRIORITY_MEDIUM    => 'priority_normal.gif',
		SAUT_PRIORITY_HIGH      => 'priority_1.gif',
		SAUT_PRIORITY_CRITICAL  => 'priority_2.gif'
	);

	$g_view_issues_page_columns = array ( 'selection', 'edit', 'priority', 'id', 'sponsorship_total', 'bugnotes_count', 'attachment', 'category_id', 'saut_issue_type', 'date_submitted', 'last_updated', 'summary','resolution', 'status' );

	$g_print_issues_page_columns = array ( 'selection', 'priority', 'id', 'sponsorship_total', 'bugnotes_count', 'attachment', 'category_id', 'status', 'last_updated', 'summary' );

	$g_status_colors		= array( 'open'			=> '#9d0000', // red    (scarlet red #ef2929)
									 'closed'		=> '#111111'); // grey  (aluminum    #babdb6)

//$g_resolution_enum_string			= '10:with vendor,20:with CP,30:fixed by vendor,60:open,70:under testing,80:closed by
	$g_resolution_colors	= array( 'with vendor'	   => '#001a55', // red    (scarlet red #ef2929)
									 'with CP'		   => '#006600',
									 'can\'t resolve'  => '#cc3399',
									 'fixed by vendor' => '#003a95',
									 'open'			   => '#9d0000',
									 'under testing'   => '#e8860C',
									 'closed by CP'    => '#111111',
									 'reopen'          => '#ff0000'); // grey  (aluminum    #babdb6)

	$g_language_choices_arr	= array(
		'english',
		'polish',
	);

	$g_custom_group_actions = array(
		array('action' => 'EXT_ADD_MONIT', 'label' => 'actiongroup_menu_add_monit'),
	    array('action' => 'EXT_RMV_MONIT', 'label' => 'actiongroup_menu_rmv_monit')
	);

	$g_my_view_boxes = array (
		'assigned'      => '1',
		'unassigned'    => '0',
		'reported'      => '3',
		'resolved'      => '4',
		'recent_mod'	=> '5',
		'monitored'	=> '6',
		'feedback'	=> '0',
		'verify'	=> '0',
		'my_comments'	=> '0',
		'unresolved'	=> '2'
	);
?>
