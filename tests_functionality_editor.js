// Kodowanie UTF-8, test: Zażółć gęślą jaźń

var functionality_name = null;
var functionality_description = null;
var functionality_length = null;

function set_name_normal()
{
	var header_elem = $('<h1></h1>');
	header_elem.text(functionality_name + ' ');

	var edit_a = $('<a href=\"#\" title=\"Edit\" class=\"edit\"><img src=\"images/edit.png\" alt=\"Edit\"></a>');
	edit_a.click(edit_name_click);
	header_elem.append(edit_a);

	$('#functionality_name').empty().append(header_elem);

	return false;
}

function set_description_normal()
{
	var desc_edit_link = $('div#functionality_description div.header a');
	desc_edit_link.click(edit_description_click);
	desc_edit_link.show();
	
	$('div#functionality_description div.text').text(functionality_description);
	
	return false;
}

function edit_name_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		functionality_name = this.name;
		set_name_normal();
	}
}

function chapter_name_set_normal(chapter_div)
{
	var chapter_name = chapter_div.data('name');
	
	var name_span = $('h2 span.name', chapter_div);
	name_span.text(chapter_name);
	
	$('h2 a.edit', chapter_div).show();
	if (chapter_div.data('active') == 1)
		$('h2 a.delete', chapter_div).show();
}

function chapter_name_edit_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		var request_data = this; 
		var id = request_data.id;
		var new_name = request_data.name;
		var chapter_div = $('div#chapter_' + id);
		chapter_div.data('name', new_name);
		chapter_name_set_normal(chapter_div);
	}
}

function edit_description_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		functionality_description = this.description;
		set_description_normal();
	}
}

function edit_length_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		functionality_length = this.length;
		length_cancel();
	}
}

function request(query_data, success_callback, context)
{
	$.ajax( {
		type: 'POST',
		url: 'tests_functionality_edit.php',
		data: query_data,
		success: success_callback,
		dataType: 'xml',
		context: context } );
}

function edit_name_submit()
{
	var new_name = $('#functionality_name form :text').val();
	if (new_name.length == 0)
		alert('Title cannot be empty.');
	else
	{
		var query_data = { operation:'functionality_set_name', id:functionality_id, name:new_name };
		request(query_data, edit_name_response, query_data);
	}

	return false;
}

function edit_description_submit()
{
	var new_desc = $('div#functionality_description div.text form textarea').val();
	var query_data = { operation:'functionality_set_description', id:functionality_id, description:new_desc };
	request(query_data, edit_description_response, query_data);
	return false;
}

function edit_name_click()
{
	var form = $("<form action=\"#\"></form>");
	form.submit(edit_name_submit);

	var input = $("<input type=\"text\" name=\"name\" class=\"header_font\" />");
	input.val(functionality_name);
	form.append(input);

	input = $("<input type=\"image\" src=\"images/accept.png\" title=\"Save\" />");
	input.click(edit_name_submit);
	form.append(input);

	input = $("<input type=\"image\" src=\"images/delete.png\" title=\"Cancel\" />");
	input.click(set_name_normal);
	form.append(input);

	$('#functionality_name').empty().append(form);

	return false;
}

function edit_description_click()
{
	$('div#functionality_description div.header a').hide();

	var form = $("<form action=\"#\"></form>");
	form.submit(edit_description_submit);

	var input = $("<textarea/>");
	input.val(functionality_description);
	form.append(input);

	input = $("<input type=\"image\" src=\"images/accept.png\" title=\"Save\" />");
	input.click(edit_description_submit);
	form.append(input);

	input = $("<input type=\"image\" src=\"images/delete.png\" title=\"Cancel\" />");
	input.click(set_description_normal);
	form.append(input);

	$('div#functionality_description div.text').empty().append(form);

	return false;
}

function delete_case_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		case_id = this.id;
		
		var case_elem = $('case', data);
		var last_update_user = case_elem.attr('last_update_user');
		var last_update_time = case_elem.attr('last_update_time');

		var case_table = $('table#case_' + case_id);
		case_table.addClass('inactive');
		case_table.data('active', 0);
		case_table.data('last_update_user', last_update_user);
		case_table.data('last_update_time', last_update_time);
		$('td.functions a.delete', case_table).remove();
		$('td.drag_handle span.lp').empty(); // Clear LP number
		if (!is_inactive_visible())
			case_table.hide();
		update_case_last_update(case_table);
		set_case_numbers(case_table.parents('div[id^="chapter_"]'));
	}
}

function edit_case_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		case_id = this.id;
		
		var case_elem = $('case', data);
		var last_update_user = case_elem.attr('last_update_user');
		var last_update_time = case_elem.attr('last_update_time');
		
		var case_table = $('table#case_' + case_id);
		var new_procedure_text = $('td.procedure_text textarea', case_table).val();
		var new_expected_result = $('td.expected_result textarea', case_table).val();
		case_table.data('procedure_text', new_procedure_text);
		case_table.data('expected_result', new_expected_result);
		case_table.data('last_update_user', last_update_user);
		case_table.data('last_update_time', last_update_time);
		set_case_normal(case_table);
		
		set_case_numbers(case_table.parents('div[id^="chapter_"]'));
	}
}

function case_add_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		var case_elem = $('case', data);
		var case_id = case_elem.attr('id');
		var last_update_user = case_elem.attr('last_update_user');
		var last_update_time = case_elem.attr('last_update_time');
		
		var case_table = this;

		var new_procedure_text = $('td.procedure_text textarea', case_table).val();
		var new_expected_result = $('td.expected_result textarea', case_table).val();
		
		case_table.attr('id', 'case_' + case_id);
		case_table.removeClass('new');
		case_table.removeData('new');
		case_table.data('id', case_id);
		case_table.data('procedure_text', new_procedure_text);
		case_table.data('expected_result', new_expected_result);
		case_table.data('last_update_user', last_update_user);
		case_table.data('last_update_time', last_update_time);
		set_case_normal(case_table);
		
		set_case_numbers(case_table.parents('div[id^="chapter_"]'));
	}
}

function edit_case_accept_click()
{
	var case_table = $(this).parents('table[id^="case_"]');
	// Edited existing case
	if (case_table.size())
	{
		var case_id = case_table.data('id');
		var new_procedure_text = $('td.procedure_text textarea', case_table).val();
		var new_expected_result = $('td.expected_result textarea', case_table).val();
		var query_data = {
			operation:'case_edit',
			id:case_id,
			procedure_text:new_procedure_text,
			expected_result:new_expected_result };
		request(query_data, edit_case_response, query_data);
	}
	// Confirming new case
	else
	{
		case_table = $(this).parents('table.new');
		var new_procedure_text = $('td.procedure_text textarea', case_table).val();
		var new_expected_result = $('td.expected_result textarea', case_table).val();
		var chapter_id = case_table.parents('div[id^="chapter_"]').data('id');
		
		var prev_case_tables = case_table.prevAll('table[id^="case_"]');
		
		var query_data = {
			operation:'case_add',
			chapter_id:chapter_id,
			procedure_text:new_procedure_text,
			expected_result:new_expected_result,
			index:prev_case_tables.size() };
		request(query_data, case_add_response, case_table);
	}
	
	return false;
}

function edit_case_cancel_click()
{
	var case_table = $(this).parents('table[id^="case_"]');
	if (case_table.size())
	{
		set_case_normal(case_table);
		set_case_numbers(case_table.parents('div[id^="chapter_"]'));
	}
	else
		$(this).parents('table.new').remove();
	return false;
}

function set_case_edit(case_table)
{
	case_table = $(case_table);

	var procedure_text = case_table.data('procedure_text');
	var expected_result = case_table.data('expected_result');

	var procedure_text_textarea = $("<textarea/>");
	var expected_result_textarea = $("<textarea/>");

	procedure_text_textarea.text(procedure_text);
	expected_result_textarea.text(expected_result);
	
	var accept_link = $("<a href=\"#\" class=\"accept\" title=\"Save\"><img src=\"images/accept.png\" alt=\"Save\"></a>")
	var cancel_link = $("<a href=\"#\" class=\"cancel\" title=\"Cancel\"><img src=\"images/delete.png\" alt=\"Cancel\"></a>")

	accept_link.click(edit_case_accept_click);
	cancel_link.click(edit_case_cancel_click);
	
	var drag_handle_cell = $('td.drag_handle', case_table);
	drag_handle_cell.empty();
	drag_handle_cell.append($('<span class="lp" />'));
	drag_handle_cell.append($("<br />"));
	if (!case_table.data('new'))
		drag_handle_cell.append($("<img src=\"images/drag-handle.png\" alt=\"Drag to move\" title=\"Drag to move\" />"));
	
	$('td.procedure_text', case_table).empty().append(procedure_text_textarea);
	$('td.expected_result', case_table).empty().append(expected_result_textarea);
	$('td.functions', case_table).empty().append(accept_link).append(cancel_link);
}

function edit_case_click()
{
	var case_table = $(this).parents('table[id^="case_"]');
	var chapter_div = $(this).parents('div[id^="chapter_"]');
	
	set_case_edit(case_table);
	set_case_numbers(chapter_div);
	
	return false;
}

function delete_case_click()
{
	if (confirm('Do you really want to delete (deactivate) this case?'))
	{
		var case_table = $(this).parents('table[id^="case_"]');
		var case_id = case_table.data('id');
		var query_data = { operation:'case_delete', id:case_id };
		request(query_data, delete_case_response, query_data);
	}
	return false;
}

function get_last_update_string(case_table)
{
	var last_update_time = case_table.data('last_update_time');
	var last_update_user = case_table.data('last_update_user');
	if (last_update_time && last_update_user)
		return "Updated: " + last_update_time + ", " + last_update_user;
	else if (last_update_time && last_update_user)
		return sprintf("Updated: " + last_update_time + last_update_user);
	else
		return '';
}

function update_case_last_update(case_table)
{
	var last_update_str = get_last_update_string(case_table);
	
	var procedure_text_elem = case_table.find('td.procedure_text');
	var last_update_elem = procedure_text_elem.find('span.last_update');

	if (last_update_elem.size())
	{
		if (last_update_str)
			last_update_elem.text(last_update_str); // span.last_update exists, last update string exists - update text.
		else
			last_update_elem.remove(); // span.last_update exists, last update string is empty - remove span.
	}
	else
	{
		if (last_update_str)
			procedure_text_cell.append($("<span class=\"last_update\" />").text(last_update_str)); // span.last_update doesn't exit, last update string exists - create span.
	}
}

function set_case_normal(case_table)
{
	case_table = $(case_table);
	
	var active = case_table.data('active');
	var procedure_text = case_table.data('procedure_text');
	var expected_result = case_table.data('expected_result');
	var last_update_str = get_last_update_string(case_table);
	
	var drag_handle_cell = $('td.drag_handle', case_table);
	drag_handle_cell.empty();
	drag_handle_cell.append($('<span class="lp" />'));
	drag_handle_cell.append($("<br />"));
	drag_handle_cell.append($("<img src=\"images/drag-handle.png\" alt=\"Drag to move\" title=\"Drag to move\" />"));
	
	var procedure_text_cell = $('td.procedure_text', case_table).text(procedure_text);
	if (last_update_str)
		procedure_text_cell.append($("<span class=\"last_update\" />").text(last_update_str));

	$('td.expected_result', case_table).text(expected_result);

	var functions_cell = $('td.functions', case_table);
	functions_cell.empty();
	
	var add_above_link = $("<a href=\"#\" title=\"Add above\" class=\"add\"><img src=\"images/list-add.png\" alt=\"Add above\"></a>")
		.click(case_add_above_click);
	functions_cell.append(add_above_link);
	
	var edit_link = $('<a href=\"#\" title=\"Edit\" class=\"edit\"><img src=\"images/edit.png\" alt=\"Edit\"></a>');
	edit_link.click(edit_case_click);
	functions_cell.append(edit_link);

	if (active == 1)
	{
		var delete_link = $("<a href=\"#\" title=\"Delete\" class=\"delete\"><img src=\"images/delete.png\" alt=\"Delete\" />");
		delete_link.click(delete_case_click);
		functions_cell.append(delete_link);
	}
}

function create_case_table()
{
	var case_table = $("<table class=\"new\"><tr><td class=\"drag_handle\" /><td class=\"procedure_text\" /><td class=\"expected_result\" /><td class=\"functions\" /></tr></table>");
	set_case_table_widths(case_table);

	case_table.data('new', 1);
	case_table.data('active', 1);
	case_table.data('procedure_text', 'Procedure');
	case_table.data('expected_result', 'Expected result');
	
	return case_table;
}

function case_add_click()
{
	var case_table = create_case_table();
	var cases_div = $(this).parents('div[id^="chapter_"]').children('div.cases');
	cases_div.append(case_table);
	set_case_edit(case_table);
	return false;
}

function case_add_above_click()
{
	var new_case_table = create_case_table();
	var this_case_table = $(this).parents('table[id^="case_"]');
	this_case_table.before(new_case_table);
	set_case_edit(new_case_table);
	return false;
}

function set_case_table_widths(case_table)
{
	case_table = $(case_table);
	var cells = $('th, td', case_table);
	$(cells[0]).attr('width', '32');
	$(cells[1]).attr('width', '50%');
	$(cells[2]).attr('width', '*');
	$(cells[3]).attr('width', '48');
}

function case_order_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	// else ignore, order has been saved.
}

function chapter_order_response(response_data)
{
	var error = $('error', response_data);
	if (error.size())
		alert(error.text());
	// else ignore, order has been saved.
}

function case_order_send(chapter_div)
{
	chapter_div = $(chapter_div);
	var chapter_id = chapter_div.data('id');
	var case_tables = $('div.cases table[id^="case_"]', chapter_div);
	var case_count = case_tables.size();
	if (case_count)
	{
		var case_ids = String($(case_tables[0]).data('id'));
		for (var i = 1; i < case_count; ++i)
		{
			case_ids += ',';
			case_ids += String($(case_tables[i]).data('id'));
		}

		var query_data = { operation:'case_order', chapter_id:chapter_id, case_ids:case_ids };
		request(query_data, case_order_response, null);
	}
}

function case_drag_update(event, ui)
{
	var chapter_div = $(ui.item).parents('div[id^="chapter_"]');
	set_case_numbers(chapter_div);
	case_order_send(chapter_div);
	if (ui.sender)
	{
		chapter_div = $(ui.sender).parents('div[id^="chapter_"]');
		set_case_numbers(chapter_div);
		case_order_send(chapter_div);
	}
}

function chapter_drag_update(event, ui)
{
	chapters_div = $('div#functionality_chapters');
	var chapter_divs = chapters_div.find('> div');
	var chapter_count = chapter_divs.size();
	if (chapter_count > 1)
	{
		var chapter_ids = String($(chapter_divs[0]).data('id'));
		for (var i = 1; i < chapter_count; ++i)
		{
			chapter_ids += ',';
			chapter_ids += String($(chapter_divs[i]).data('id'));
		}

		var query_data = { operation:'chapter_order', functionality_id:functionality_id, chapter_ids:chapter_ids };
		request(query_data, chapter_order_response, null);
	}
	
	set_chapter_numbers();
}

function set_case_numbers(chapter_div)
{
	var case_tables = $('div.cases table[id^="case_"]:not(.inactive)', chapter_div);
	case_tables.each(function(index) {
		$('td.drag_handle span.lp', this).text(index + 1);
	} );
}

function chapter_name_edit_submit()
{
	var new_name = $(this).siblings('input:text').val();
	var chapter_id = $(this).parents('div[id^="chapter_"]').data('id');
	if (new_name.length == 0)
		alert('Chapter name cannot be empty.');
	else
	{
		var query_data = { operation:'chapter_set_name', id:chapter_id, name:new_name };
		request(query_data, chapter_name_edit_response, query_data);
	}

	return false;
}

function chapter_name_edit_cancel()
{
	var chapter_div = $(this).parents('div[id^="chapter_"]');
	chapter_name_set_normal(chapter_div);
	return false;
}

function chapter_name_edit_click()
{
	var chapter_div = $(this).parents('div[id^="chapter_"]');
	var chapter_name = chapter_div.data('name');
	
	var form = $("<form action=\"#\"></form>");
	form.submit(chapter_name_edit_submit);

	var input = $("<input type=\"text\" name=\"name\" class=\"header_font\" />");
	input.val(chapter_name);
	form.append(input);

	input = $("<input type=\"image\" src=\"images/accept.png\" title=\"Save\" />");
	input.click(chapter_name_edit_submit);
	form.append(input);

	input = $("<input type=\"image\" src=\"images/delete.png\" title=\"Cancel\" />");
	input.click(chapter_name_edit_cancel);
	form.append(input);

	$('h2 span.name', chapter_div).empty().append(form);
	$('h2 a.edit, h2 a.delete', chapter_div).hide();

	return false;
}

function chapter_description_set_normal(chapter_div)
{
	var chapter_description = chapter_div.data('description');
	$('div.description div.header a', chapter_div).show();
	$('div.description div.text', chapter_div).text(chapter_description);
}

function chapter_description_set_edit(chapter_div)
{
	var chapter_description = chapter_div.data('description');
	$('div.description div.header a', chapter_div).hide();
	$('div.description div.text', chapter_div).empty().append(
		$("<form action=\"#\"></form>")
			.submit(chapter_description_edit_submit)
			.append($("<textarea />").text(chapter_description))
			.append($("<input type=\"image\" src=\"images/accept.png\" title=\"Save\" />").click(chapter_description_edit_submit))
			.append($("<input type=\"image\" src=\"images/delete.png\" title=\"Cancel\" />").click(chapter_description_edit_cancel))
	);
}

function chapter_description_edit_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		var request_data = this; 
		var id = request_data.id;
		var new_description = request_data.description;
		var chapter_div = $('div#chapter_' + id);
		chapter_div.data('description', new_description);
		chapter_description_set_normal(chapter_div);
	}
}

function chapter_description_edit_submit()
{
	var chapter_div = $(this).parents('div[id^="chapter_"]');
	var new_description = chapter_div.find('div.description div.text textarea').val();
	var chapter_id = chapter_div.data('id');
	
	var query_data = { operation:'chapter_set_description', id:chapter_id, description:new_description };
	request(query_data, chapter_description_edit_response, query_data);

	return false;
}

function chapter_description_edit_cancel()
{
	chapter_description_set_normal($(this).parents('div[id^="chapter_"]'));
	return false;
}

function chapter_description_edit_click()
{
	chapter_description_set_edit($(this).parents('div[id^="chapter_"]'));
	return false;
}

function chapter_delete_response(response_data)
{
	var error = $('error', response_data);
	if (error.size())
		alert(error.text());
	else
	{
		var request_data = this; 
		var chapter_id = request_data.id;
		var chapter_div = $('div#chapter_' + chapter_id);
		
		if ($('deleted', response_data).size()) // <deleted/>
			chapter_div.remove();
		else // <deactivated/>
		{
			chapter_div.data('active', 0);
			chapter_div.addClass('inactive');
		}
		
		set_chapter_numbers();
	}
}

function chapter_delete_click()
{
	if (confirm('Do you really want to delete or deactivate this chapter?'))
	{
		var chapter_div = $(this).parents('div[id^="chapter_"]');
		var chapter_id = chapter_div.data('id');
		
		var query_data = { operation:'chapter_delete', id:chapter_id };
		request(query_data, chapter_delete_response, query_data);
	}
	return false;
}

function chapter_expand_collapse_click()
{
	var expand_collapse_link = $(this);
	var chapter_div = expand_collapse_link.parents('div[id^="chapter_"]');

	expand_collapse_link.toggleClass('icon_collapse');
	expand_collapse_link.toggleClass('icon_expand');

	chapter_div.find('> div.description').toggle();
	chapter_div.find('> div.cases').toggle();
	chapter_div.find('> div.add_case').toggle();
}

function all_chapters_expand_click()
{
	$('div#functionality_chapters > div > h2 a.icon_expand').addClass('icon_collapse').removeClass('icon_expand');
	$('div#functionality_chapters > div > div.description').show();
	$('div#functionality_chapters > div > div.cases').show();
	$('div#functionality_chapters > div > div.add_case').show();
	return false;
}

function all_chapters_collapse_click()
{
	$('div#functionality_chapters > div > h2 a.icon_collapse').addClass('icon_expand').removeClass('icon_collapse');
	$('div#functionality_chapters > div > div.description').hide();
	$('div#functionality_chapters > div > div.cases').hide();
	$('div#functionality_chapters > div > div.add_case').hide();
	return false;
}

function chapter_create(chapters_div, chapter_id, chapter_active, chapter_name, chapter_description)
{
	var chapter_div = $("<div id=\"chapter_" + chapter_id + "\" />");
	chapter_div.data('id', chapter_id);
	chapter_div.data('active', chapter_active);
	chapter_div.data('name', chapter_name);
	chapter_div.data('description', chapter_description);
	
	if (chapter_active != 1)
		chapter_div.addClass('inactive');

	var header = $("<h2 />");
	header.append($("<a href=\"#\" title=\"Expand/collapse\" class=\"icon_collapse\" />").click(chapter_expand_collapse_click));
	header.append($("<img class=\"chapter_drag_handle\" src=\"images/drag-handle.png\" alt=\"Drag to move\" title=\"Drag to move\" />"));
	header.append($("<span class=\"lp\" />"));
	header.append($("<span class=\"name\" />").text(chapter_name));
	
	var name_edit_link = $("<a class=\"edit\" href=\"#\" title=\"Edit\"><img src=\"images/edit.png\" alt=\"Edit\"></a>");
	name_edit_link.click(chapter_name_edit_click);
	header.append(name_edit_link);
	
	var chapter_delete_link = $("<a class=\"delete\" href=\"#\" title=\"Delete\"><img src=\"images/delete.png\" alt=\"Delete\"></a>");
	chapter_delete_link.click(chapter_delete_click);
	header.append(chapter_delete_link);
	if (chapter_active != 1)
		chapter_delete_link.hide();
	
	chapter_div.append(header);
	
	var description_header = $("<div class=\"header\">Description: </div>").append(
		$("<a href=\"#\" title=\"Edit\"><img src=\"images/edit.png\" alt=\"Edit\"></a>")
			.click(chapter_description_edit_click) );
	var description_text = $("<div class=\"text\" />");
	chapter_div.append(
		$("<div class=\"description\" />")
			.append(description_header)
			.append(description_text)
	);
	chapter_description_set_normal(chapter_div);

	var cases_div = $("<div class=\"cases\"><table><tr><th style=\"text-align:center\">LP<th>Procedure<th>Expected result<th>&nbsp;</table></div>");
	set_case_table_widths($('table', cases_div));
	chapter_div.append(cases_div);
	
	var add_case_button = $('<button>Add case</button>');
	add_case_button.click(case_add_click);
	chapter_div.append($('<div class="add_case"/>').append(add_case_button));

	cases_div.sortable( {
		//placeholder: 'placeholder',
		//items: 'table[id^="case_"]:not(.inactive)',
		items: 'table[id^="case_"]',
		distance: 8,
		axys: 'y',
		//opacity: 0.7,
		connectWith: 'div#functionality_chapters div.cases',
		cursor: 's-resize',
		handle: 'td.drag_handle img',
		update: case_drag_update
	} );
	chapters_div.append(chapter_div);
	return chapter_div;
}

function set_chapter_numbers()
{
	var chapter_divs = $('div#functionality_chapters div[id^="chapter_"]');
	var chapter_count = chapter_divs.size();
	var number = 1;
	for (var i = 0; i < chapter_count; ++i)
	{
		var chapter_div = $(chapter_divs[i]);
		var lp_span = chapter_div.find('h2 span.lp');
		if (chapter_div.hasClass('inactive'))
			lp_span.text('');
		else
			lp_span.text(number++ + '. ');
	}
}

function init_chapters(data)
{
	var chapters_div = $('#functionality_chapters');
	chapters_div.empty();
	
	var chapters = $('functionality > chapters > chapter', data);
	
	chapters.each(function(chapter_index) {
		var chapter = chapters[chapter_index];
		
		var chapter_id = $(chapter).attr('id');
		var chapter_active = $(chapter).attr('active');
		var chapter_name = $('name', chapter).text();
		var chapter_description = $('description', chapter).text();
		
		var chapter_div = chapter_create(chapters_div, chapter_id, chapter_active, chapter_name, chapter_description);
		
		var cases_div = chapter_div.find('div.cases');
		var cases = $('case', chapter);
		cases.each(function(case_index) {
			var case_ = cases[case_index];
			
			var case_id = $(case_).attr('id');
			var case_active = $(case_).attr('active');
			var case_procedure_text = $('procedure_text', case_).text();
			var case_expected_result = $('expected_result', case_).text();
			
			var case_table = $("<table><tr><td class=\"drag_handle\"><td class=\"procedure_text\" width=\"50%\" /><td class=\"expected_result\" width=\"*\" /><td class=\"functions\" /></tr></table>");
			set_case_table_widths(case_table);

			case_table.data('id', case_id);
			case_table.data('active', case_active);
			case_table.data('procedure_text', case_procedure_text);
			case_table.data('expected_result', case_expected_result);
			case_table.data('last_update_time', $(case_).attr('last_update_time'));
			case_table.data('last_update_user', $(case_).attr('last_update_user'));

			case_table.attr('id', 'case_' + case_id);
			if (case_active == 0)
				case_table.addClass('inactive');
			cases_div.append(case_table);
			set_case_normal(case_table);
		} );
				
		set_case_numbers(chapter_div);
	} );
	
	set_chapter_numbers();
	
	chapters_div.sortable( {
		//placeholder: '',
		//items: '',
		items: '> div[id^="chapter_"]',
		distance: 8,
		axys: 'y',
		//opacity: 0.7,
		cursor: 's-resize',
		handle: 'h2 > img.chapter_drag_handle',
		update: chapter_drag_update
	} );
}

function is_inactive_visible()
{
	return $('#show_inactive_checkbox').prop('checked');
}

function inactive_checkbox_change()
{
	var show = is_inactive_visible();
	var inactive_chapters = $('div#functionality_chapters > div.inactive');
	var inactive_cases = $('div#functionality_chapters div.cases table.inactive');
	
	if (show)
	{
		inactive_chapters.show();
		inactive_cases.show();
	}
	else
	{
		inactive_chapters.hide();
		inactive_cases.hide();
	}
}

function length_change()
{
	$('form#functionality_length input[type="image"]').show();
}

function fetch_functionality_response(data)
{
	var error = $('error', data);
	if (error.size())
		alert(error.text());
	else
	{
		functionality_name = $('functionality > name', data).text();
		functionality_description = $('functionality > description', data).text();
		functionality_length = $('functionality', data).attr('length');
		
		set_name_normal();
		set_description_normal();
		
		$('form#functionality_length input[type="text"]').val(functionality_length);
		
		init_chapters(data);
		inactive_checkbox_change();
	}
}

function length_submit()
{
	var new_length = $('form#functionality_length input[type="text"]').val();

	var query_data = { operation:'functionality_set_length', id:functionality_id, length:new_length };
	request(query_data, edit_length_response, query_data);

	return false;
}

function length_cancel()
{
	$('form#functionality_length input[type="text"]').val(functionality_length);
	$('form#functionality_length input[type="image"]').hide();
	return false;
}

function chapter_add_response(response_data)
{
	var error = $('error', response_data);
	if (error.size())
		alert(error.text());
	else
	{
		var request_data = this;
		var chapters_div = $('#functionality_chapters');
		var chapter_id = $('id', response_data).text();
		var chapter_active = 1;
		var chapter_name = request_data.name;
		var chapter_description = request_data.description;
		
		var chapter_div = chapter_create(chapters_div, chapter_id, chapter_active, chapter_name, chapter_description);
		set_chapter_numbers();
	}
}

function chapter_add_click()
{
	var query_data = {
		operation:'chapter_add',
		functionality_id:functionality_id,
		name:'Rozdział',
		description:'' };
	request(query_data, chapter_add_response, query_data);
	return false;
}

function init()
{
	$(document).ajaxError(function(event, request, settings, error) {
		alert('AJAX error: ' + error);
	} );
	
	if (functionality_id)
	{
		var query_data = { operation:'functionality_fetch', id:functionality_id };
		request(query_data, fetch_functionality_response, query_data);
	}
	
	$('#show_inactive_checkbox').change(inactive_checkbox_change);
	$('form#functionality_length input[type="image"]').hide();

	$('form#functionality_length input[type="text"]').change(length_change).focusin(length_change);
	$('form#functionality_length').submit(length_submit);
	$('form#functionality_length input[type="image"]:eq(0)').click(length_submit);
	$('form#functionality_length input[type="image"]:eq(1)').click(length_cancel);
	$('div#add_chapter button').click(chapter_add_click);
	
	$('a#expand_all_link').click(all_chapters_expand_click);
	$('a#collapse_all_link').click(all_chapters_collapse_click);
}

$(init);
