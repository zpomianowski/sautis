<?php
// Zażółć gęślą jaźń
// Edycja testu - żądanie AJAX, zwraca XML

require_once('tests_main.php');
header('Content-type: text/xml; charset=utf-8');
send_header_no_cache();
send_xml_header();
connect_to_database();
login();

function SaveTest()
{
	$testID = $_POST['testID'];
	$projectID = $_POST['projectID'];
	
	$softID = $_POST['softID'];
	$name = StripInput($_POST['name']);
	$description = StripInput($_POST['description']);
	$summary = StripInput($_POST['summary']);

	$start_date = $_POST['startDate'];
	$arr = explode("/", $start_date);
	$startDate = $arr[2]."-".$arr[1]."-".$arr[0];		
	
	$estimated_stop_date = $_POST['estimatedStopDate'];
	$arr = explode("/", $estimated_stop_date);
	$estimatedStopDate = $arr[2]."-".$arr[1]."-".$arr[0];		
	
	$soft_arrive_date = $_POST['softArriveDate'];
	$arr = explode("/", $soft_arrive_date);
	$softArriveDate = $arr[2]."-".$arr[1]."-".$arr[0];		
	
	$query = 'UPDATE tests_tests SET id_project="'.$projectID.'", name="'.mysql_real_escape_string($name).'",
			 description="'.mysql_real_escape_string($description).'"';
	  
	if ($softID != '')	
		$query .= ', id_planned_soft="'.$softID.'"';
	else 
		$query .= ', id_planned_soft=NULL';
	
	if ($start_date)
		$query .= ', start_date="'.$startDate.'"';
	if ($estimated_stop_date)
		$query .= ', estimated_stop_date="'.$estimatedStopDate.'"';
	if ($soft_arrive_date)
		$query .= ', software_arrive_date="'.$softArriveDate.'"';
	if ($summary != '')
		$query .= ', summary="'.$summary.'"';
	else 				
		$query .= ', summary=NULL';
	$query .= ' WHERE id_test='.$testID.';';
		
	$result = mysql_query($query);
	if ($result)
		print("<ok/>");
	else
		print_error(mysql_error());
}

function GetOpenTasks()
{
	$testID = $_POST['testID'];

	$query = 'SELECT count(*) count FROM tests_tasks WHERE id_test='.$testID.' AND closed=0 GROUP BY closed;';
	$result = mysql_query($query);
	$row = mysql_fetch_assoc($result);
	print("<openTasks>".$row['count']."</openTasks>");
}

function StartTest()
{
	$testID = $_POST['testID'];
	
	if ($testID == "")
		print_error("Invalid TestID");
	
	$query = 'SELECT * FROM tests_tasks WHERE id_test='.$testID.';';
	$result = mysql_query($query);
	if (!mysql_fetch_assoc($result))
	{
		print("<noTasks>1</noTasks>");
		return;
	}		
		
	$query = 'UPDATE tests_tests SET start_date=NOW() WHERE id_test='.$testID.' LIMIT 1;';
		
	$r = mysql_query($query);
	if ($r === false)
		print_error(mysql_error());
	else
	{
		$err = ReportStartTest($testID);
		if ($err)
		{
			print("<error>".$err."</error>");
			return;
		}
		print("<TestID>".$row['count']."</TestID>");
	}
}

function StopTest()
{
	$testID = $_POST['testID'];
	$softStatus = $_POST['softStatus'];
	
	$query = 'SET AUTOCOMMIT=0';
	$r = mysql_query($query);
	$query = 'START TRANSACTION';
	$r = mysql_query($query);
		
	$query = 'UPDATE tests_tests SET stop_date=NOW(), software_status='.$softStatus.' WHERE id_test='.$testID.' LIMIT 1;';
	echo $query;
	$r = mysql_query($query);
	$query = 'UPDATE tests_tasks SET closed=1, summary=concat(summary, "\n\nClosed automatically.") WHERE closed=0 AND id_test='.$testID.';';
	$r2 = mysql_query($query);
	
	if ($r && $r2)
	{
		$query = 'COMMIT';
		$r = mysql_query($query);
		$query = 'SET AUTOCOMMIT=1';
		$r = mysql_query($query);
		
		ReportEndTest($testID);
		$t_redirect = "tests_test.php?id=".$testID;
		redirect($t_redirect, 0);		
	}
	else
	{
		$query = 'ROLLBACK';
		$r = mysql_query($query);		
		$query = 'SET AUTOCOMMIT=0';
		$r = mysql_query($query);
		
		send_html_header();
		print_error_paragraph(mysql_error());			
		echo '<br/>';	
		print_back_link('tests_test.php?id='.$testID);
		echo '<br/>';	
		print_page_end();		
	}
}

function UpdateTasks()
{
	$testID = $_POST['testID'];
	$chapStr = $_POST['chapterLoadData'];
	$taskStr = $_POST['userTask'];
	$taskDescStr = $_POST['userTaskDesc'];
	
	// Parsowanie parametrow rozdzialow = chapterID => userID
	$chaptersData = array();
	$params = explode(':', $chapStr);
	foreach($params as $param)
	{
		$arr = split('_', $param);
		$arr[0] = split('-', $arr[0]);
		$arr[1] = split('-', $arr[1]);
		
		$keyValuePair[0] = $arr[0][1]; 
		$keyValuePair[1] = $arr[1][1]; 
		
		$chaptersData[$keyValuePair[0]] = $keyValuePair[1];
	}
	// Parsowanie parametrow testerów = userID => description
	$tasksData = array('desc' => '', 'taskID' => 0, 'startDate' => 0, 'stopDate' => 0);
	$params = explode(':', $taskStr);
	$paramsDesc = explode('###', $taskDescStr);
	$idx = 0;
	foreach($params as $param)
	{
		$descs = explode(':::', $paramsDesc[$idx]);
		
		$arr = explode('_', $param);		
		$arr[0] = explode('-', $arr[0]);
		$arr[1] = explode('-', $arr[1]);
		$arr[2] = explode('-', $arr[2]);
		
		$keyValuePair[0] = $arr[0][1]; 
		$keyValuePair[1] = $arr[1][1]; 
		$keyValuePair[2] = $arr[2][1]; 
		
		$tasksData[$keyValuePair[0]]['desc'] = $descs[1];
		$tasksData[$keyValuePair[0]]['startDate'] = $keyValuePair[1];
		$tasksData[$keyValuePair[0]]['stopDate'] = $keyValuePair[2];
		
		$idx++;
	}	
	
	// Zablokowanie tabel na wyłączność
	$query = 'LOCK TABLES tests_tasks WRITE, tests_test_cases WRITE, tests_cases READ, tests_cases AS tc READ;';
	$result = mysql_query($query);
	
	// przejscie po kazdym rozdziale
	foreach($chaptersData as $key => $value)
	{
		// pobiera id_tests_test_cases nie zamknietych i przypisanych juz do kogos dla danego rozdzialu
		// nastepnie oddziela idki przecinkami i trzyma w zmiennej "$openAssignedCasesStrList"
		$openAssignedCasesStrList = '';
		$query = 'SELECT tests_test_cases.* FROM tests_test_cases 
				JOIN tests_tasks ON tests_test_cases.id_tests_task=tests_tasks.id_tests_task 
				JOIN tests_cases ON tests_test_cases.id_tests_cases=tests_cases.id 
				WHERE result = 0 AND id_test='.$testID.' AND chapter_id='.$key.';';
		$result = mysql_query($query);
		if (!result)
		{
			print_update_tasks_error();
			return;
		}
		while($row = mysql_fetch_assoc($result))
			$openAssignedCasesStrList .= $row['id_tests_test_cases'].',';
		if ($openAssignedCasesStrList != '')
			$openAssignedCasesStrList = substr($openAssignedCasesStrList, 0, strlen($openAssignedCasesStrList)-1);

			
		// jezeli rozdzial ma wykonawce
		if ($value)
		{
			// Pobranie dla id_user odowiedniego id_tests_task, jak nie ma to utworzyc wpis taska
			$query = 'SELECT id_tests_task FROM tests_tasks WHERE id_test='.$testID.' AND id_user='.$value.';';
			$result = mysql_query($query);
			if (!result)
			{
				print_update_tasks_error();
				return;
			}
			
			if ($row = mysql_fetch_assoc($result))
				$tasksData[$value]['taskID'] = $row['id_tests_task'];
			else 
			{
				$query = 'INSERT INTO tests_tasks (id_test, id_user) VALUES('.$testID.', '.$value.');';
				$result = mysql_query($query);
				if (!$result)
				{
					print_update_tasks_error();
					return;
				}
				$tasksData[$value]['taskID'] = mysql_insert_id();
			}
			
			// wybieramy wszystkie przydzielone casy i nie zamkniete z tego rozdzialu i updejtujemy im idTaska
			if ($openAssignedCasesStrList != '')
			{
				$query = 'UPDATE tests_test_cases SET id_tests_task='.$tasksData[$value]['taskID'].', admin_change_time=NOW()  
							WHERE id_tests_task <> '.$tasksData[$value]['taskID'].' AND id_tests_test_cases IN ('.$openAssignedCasesStrList.')';				
				$result = mysql_query($query);
				if (!$result)
				{
					print_update_tasks_error();
					return;
				}
			}				

			// wybieramy nie przydzielone casy z tego rozdziału
			$query = 'SELECT tests_cases.id FROM tests_cases WHERE tests_cases.chapter_id='.$key.' AND tests_cases.active=1 AND tests_cases.id NOT IN
					(
					    SELECT tests_test_cases.id_tests_cases FROM tests_test_cases 
					    JOIN tests_tasks ON tests_test_cases.id_tests_task=tests_tasks.id_tests_task
					    JOIN tests_cases as tc ON tests_test_cases.id_tests_cases=tc.id
					    WHERE tests_tasks.id_test='.$testID.'    
					);';
			$notAssignedCases_result = mysql_query($query);
			if (!$notAssignedCases_result)
			{
				print_update_tasks_error();
				return;
			}
			
			// wszystkie nie przydzielone casy z tego rozdziału, przydzielamy dla idTaska(usera)
			while($notAssignedRow = mysql_fetch_assoc($notAssignedCases_result))
			{
				$query = 'INSERT INTO tests_test_cases (id_tests_task, id_tests_cases, admin_change_time) VALUES('.$tasksData[$value]['taskID'].','.$notAssignedRow['id'].', NOW());';	
				$result = mysql_query($query);
				if (!result)
				{
					print_update_tasks_error();
					return;
				}
			}			
		}
		// jezeli rozdzial nie ma wykonawcy to usunac dotychczasowe nie zamkniete casy
		else
		{
			// pobieramy casy nie zamkniete z tego rozdziału i kasujemy
			if ($openAssignedCasesStrList != '')
			{
				$query = 'DELETE FROM tests_test_cases WHERE tests_test_cases.id_tests_test_cases IN ('.$openAssignedCasesStrList.')';
				$result = mysql_query($query);

				if (!$result)
				{
					print_update_tasks_error();
					return;
				}
			}			
		}		
	}

	// pobranie wszystkich testerow przydzielonych do danego testu
	$query = 'SELECT id_tests_task, id_user FROM tests_tasks WHERE id_test='.$testID.';';
	$result = mysql_query($query);
	if (!$result)
	{
		print_update_tasks_error();
		return;
	}
	
	// Sprawdzenie czy testerzy maja przydzielone casy
	while($row = mysql_fetch_assoc($result))
	{
		$query_cases = 'SELECT * FROM tests_test_cases WHERE id_tests_task='.$row['id_tests_task'].';';
		$cases_result = mysql_query($query_cases);
		if (!$cases_result)
		{
			print_update_tasks_error();
			return;
		}
		
		// Jezeli tester ma przydzielone casy to update "description" 
		if (mysql_fetch_assoc($cases_result))
		{
			$start_date = $tasksData[$row['id_user']]['startDate'];
			$arr = explode("/", $start_date);
			$startDate = $arr[2]."-".$arr[1]."-".$arr[0];
			
			$stop_date = $tasksData[$row['id_user']]['stopDate'];
			$arr = explode("/", $stop_date);
			$stopDate = $arr[2]."-".$arr[1]."-".$arr[0];
			
			$desc = StripInput($tasksData[$row['id_user']]['desc']);
			$query = 'UPDATE tests_tasks SET description="'.mysql_real_escape_string($desc).'",
					start_date="'.$startDate.'", stop_date="'.$stopDate.'" WHERE id_tests_task='.$row['id_tests_task'].' LIMIT 1;';
			$update_result = mysql_query($query);
			
			if (!$update_result)
			{
				print_update_tasks_error();
				return;
			}
		}
		// Jezeli tester nie ma przydzielonych casów to nalezy skasowac taki wpis taska
		else
		{
			$query = 'DELETE FROM tests_tasks WHERE id_tests_task='.$row['id_tests_task'].' LIMIT 1;';
			$delete_result = mysql_query($query);

			if (!$delete_result)
			{
				print_update_tasks_error();
				return;
			}
		}
	}

	// Odblokowanie tabel
	$query = 'UNLOCK TABLES;';
	$result = mysql_query($query);
	
	print("<ok/>");	
}

function print_update_tasks_error()
{
	print_error(mysql_error());
	$query = 'UNLOCK TABLES;';
	$result = mysql_query($query);
}

function print_error($msg)
{
	printf("<error>%s</error>", htmlspecialchars($msg));
}

switch ($_POST['operation'])
{
	case 'save_test':
		if ($g_login['access_level'] == ADMIN)
			SaveTest();
		else 
			print_error('Access Denied');		
		break;
				
	case 'check_open_tasks':
		if ($g_login['access_level'] == ADMIN)
			GetOpenTasks();
		else 
			print_error('Access Denied');		
		break;
		
	case 'start_test':
		if ($g_login['access_level'] == ADMIN)
			StartTest();
		else 
			print_error('Access Denied');		
		break;
		
	case 'stop_test':
		if ($g_login['access_level'] == ADMIN)
			StopTest();
		else 
			print_error('Access Denied');		
		break;
		
	case 'update_tasks':
		if ($g_login['access_level'] == ADMIN)
			UpdateTasks();
		break;
	default:
		print_error('Wrong Operation.');
}
?>