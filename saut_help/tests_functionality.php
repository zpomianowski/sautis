<?php
// Kodowanie UTF-8, test: Zażółć gęślą jaźń
// Lista funkcjonalności

require_once('tests_main.php');
send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");

print_page_begin('Functionality');

$id = $_GET['id'];
if (is_numeric($id))
{
	if (current_user_can_edit_functionalities())
		$ok = true;
	else
		print_error_paragraph('Access denied.');
}
else
	print_error_paragraph('Invalid identifier.');
	
if ($ok)
{
	printf("<script type=\"text/javascript\">var functionality_id = %d;</script>\n", $id);
?>

<script type="text/javascript" src="tests_functionality_editor.js" charset="UTF-8"></script>

<div id="functionality_name"></div>

<form id="functionality_length" action="#" method="get">
	<label for="functionality_length_input">Duration:</label>
	<input type="text" name="length" id="functionality_length_input">
	hours
	<input type="image" src="images/accept.png" title="Save">
	<input type="image" src="images/delete.png" title="Cancel">
</form>

<div id="functionality_description">
	<div class="header">Description:
		<a href="#" title="Edit"><img src="images/edit.png" alt="Edit"></a>
	</div>
	<div class="text"></div>
</div>

<div style="border-bottom:1px solid #d9d9d9; padding-bottom:4px">
	<input type="checkbox" id="show_inactive_checkbox"> <label for="show_inactive_checkbox">Show inactive</label>
	| <a href="#" title="Expand all" id="expand_all_link">
		<span style="display:inline-block; width:16px; height:16px; background-image:url(images/expand_collapse.png); background-position:0px 0px;"></span>
		Expand all
	</a> 
	| <a href="#" title="Collapse all" id="collapse_all_link">
		<span style="display:inline-block; width:16px; height:16px; background-image:url(images/expand_collapse.png); background-position:0px -16px;"></span>
		Collapse all
	</a>
</div>

<div id="functionality_chapters">
	<div style="text-align:center">
		<img src="images/ajax-loader_indicator_big.gif" alt="Loading...">
	</div>
</div>

<div id="add_chapter">
	<button>Add chapter</button>
</div>

<?php
} // if ($ok)

print_page_end();
?>