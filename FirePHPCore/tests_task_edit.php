<?php
// Operacje na taskach
// Zażółć gęślą jaźń
require_once('tests_main.php');

send_html_header();
connect_to_database();
if (!login())
	die("Access Denied");


function OpenTask()
{
	$query = 'UPDATE tests_tasks SET closed=0 WHERE id_tests_task='.$_POST['taskID'].' LIMIT 1;';
	mysql_query($query);
	
	ReportOpenTask($_POST['taskID']);
	redirect('tests_task.php?testID='.$_POST['testID'].'&amp;taskID='.$_POST['taskID'], 0);
}

function CloseTask()
{
	$query = 'UPDATE tests_tasks SET closed=1 WHERE id_tests_task='.$_POST['taskID'].' LIMIT 1;';
	mysql_query($query);
	
	ReportCloseTask($_POST['taskID']);
	redirect('tests_task.php?testID='.$_POST['testID'].'&amp;taskID='.$_POST['taskID'], 0);
}

switch ($_POST['Action'])
{
	case 'Open':
		OpenTask();
		break;
	case 'Close':
		CloseTask();
		break;
	
	default:
		print_error_paragraph('Wrong Operation.');
}

print_page_end();
?>